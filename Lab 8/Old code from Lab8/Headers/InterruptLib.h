#ifndef InterruptLib
#define InterruptLib

#include "ES_Types.h"

void InitPeriodicInt(void);
uint8_t PeriodicIntQuery(void);

#endif /* InterruptLib */
