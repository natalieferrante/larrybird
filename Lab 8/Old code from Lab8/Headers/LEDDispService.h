/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServLEDDisp_H
#define ServLEDDisp_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitLEDDispService(uint8_t Priority);
bool PostLEDDispService(ES_Event_t ThisEvent);
ES_Event_t RunLEDDispService(ES_Event_t ThisEvent);

#endif /* ServLEDDisp_H */
