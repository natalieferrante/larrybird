/****************************************************************************
 Module
   EventCheckers.c

 Revision
   1.0.1

 Description
   This is the sample for writing event checkers along with the event
   checkers used in the basic framework test harness.

 Notes
   Note the use of static variables in sample event checker to detect
   ONLY transitions.

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 13:36 jec     initial version
****************************************************************************/

// this will pull in the symbolic definitions for events, which we will want
// to post in response to detecting events
#include "ES_Configure.h"
// This gets us the prototype for ES_PostAll
#include "ES_Framework.h"
// this will get us the structure definition for events, which we will need
// in order to post events in response to detecting events
#include "ES_Events.h"
// if you want to use distribution lists then you need those function
// definitions too.
#include "ES_PostList.h"
// This include will pull in all of the headers from the service modules
// providing the prototypes for all of the post functions
#include "ES_ServiceHeaders.h"
// this test harness for the framework references the serial routines that
// are defined in ES_Port.c
#include "ES_Port.h"
// include our own prototypes to insure consistency between header &
// actual functionsdefinition
#include "EventCheckers.h"

//Might be able to delete soon
#include "BeaconDetectModule.h"
#include "DriveFSM.h"

#define LEFT 1
#define RIGHT 2
#define HALF 1
#define FULL 2

// This is the event checking function sample. It is not intended to be
// included in the module. It is only here as a sample to guide you in writing
// your own event checkers
//#if 0
/****************************************************************************
 Function
   CheckMorseEvents
 Parameters
   None
 Returns
   bool: returns True if an event was posted
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
//bool CheckMorseEvents(void)
//{
//  static uint8_t  LastInputState = 0;
//  uint8_t         CurrentInputState;
//  bool            ReturnVal = false;

////  Get the CurrentInputState from the input line
//  CurrentInputState = HWREG(GPIO_PORTB_BASE +(GPIO_O_DATA + ALL_BITS)) & BIT3HI;
//
////If the state of the Morse input line has changed
//  if (CurrentInputState != LastInputState) // event detected, so post detected event
//  {
//    ES_Event ThisEvent;
//    ThisEvent.EventType = RisingEdge;
//    ThisEvent.EventParam  = ES_Timer_GetTime(); //check this is right timer
//    ES_PostAll(ThisEvent);
//    ReturnVal = true;
//  } else{
//    ES_Event ThisEvent;
//    ThisEvent.EventType = FallingEdge;
//    ThisEvent.EventParam = ES_Timer_GetTime(); //check this is right timer
//    ES_PostAll(ThisEvent);
//    ReturnVal = true;
//  }
//
//  LastInputState = CurrentInputState; // update the state for next time

//  return ReturnVal;
//}

//#endif

/****************************************************************************
 Function
   Check4Keystroke
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 Notes
   The functions that actually check the serial hardware for characters
   and retrieve them are assumed to be in ES_Port.c
   Since we always retrieve the keystroke when we detect it, thus clearing the
   hardware flag that indicates that a new key is ready this event checker
   will only generate events on the arrival of new characters, even though we
   do not internally keep track of the last keystroke that we retrieved.
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Keystroke(void)
{
  if (IsNewKeyReady())
  {
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_NEW_KEY;
    ThisEvent.EventParam = GetNewKey();
    if (ThisEvent.EventParam == '1')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = STOP;
      PostEvent.EventParam = FULL;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '2')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = TURN;
      PostEvent.EventParam = LEFT;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '3')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = TURN;
      PostEvent.EventParam = RIGHT;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '4')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = FORWARD;
      PostEvent.EventParam = HALF;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '5')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = FORWARD;
      PostEvent.EventParam = FULL;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '6')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = BACKWARD;
      PostEvent.EventParam = HALF;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '7')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = BACKWARD;
      PostEvent.EventParam = FULL;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '8')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = ALIGN;
      PostEvent.EventParam = FULL;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '9')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = DETECT;
      PostEvent.EventParam = FULL;
      PostDriveFSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'd')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = TAPE_FOUND;
      PostEvent.EventParam = FULL;
      PostDriveFSM(PostEvent);
    }
    return true;
  }

  return false;
}

/****************************************************************************
 Function
   Check4Button
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 Notes
   The functions that actually check the serial hardware for characters
   and retrieve them are assumed to be in ES_Port.c
   Since we always retrieve the keystroke when we detect it, thus clearing the
   hardware flag that indicates that a new key is ready this event checker
   will only generate events on the arrival of new characters, even though we
   do not internally keep track of the last keystroke that we retrieved.
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
//bool Check4Button(void)
//{
//  //if button pressed
//  //~do something~
//
//  if (IsNewKeyReady())   // new key waiting?
//  {
//    ES_Event_t ThisEvent;
//    ThisEvent.EventType   = ES_NEW_KEY;
//    ThisEvent.EventParam  = GetNewKey();
//    ES_PostAll(ThisEvent);
//    return true;
//  }
//  return false;
//}
