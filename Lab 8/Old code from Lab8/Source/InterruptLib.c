/****************************************************************************
 Module
   InterruptLib.c

 Revision
   1.0.1

 Description
   This module acts as the holder for all interrupt files.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "PWMLib.h"
#include "InterruptLib.h"
#include "CommGenService.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_pwm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_Timer.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"

#include "ES_Configure.h"
#include "ES_Framework.h"

//From Ed's PWM lib
#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable

static uint32_t TicksPerMS = 1250; //value used for periodic init
static uint32_t TimeoutCount = 0;
static uint16_t command;
//static uint8_t lastcommand;

/*-------------------------- Modular Functions --------------------------*/

/*-------------------------- Interrupt Functions --------------------------*/

/****************************************************************************
 Function
    EOTISR

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   Natalie Ferrante 2/06/2020
****************************************************************************/
void EOTISR(void)
{
  //Mask interrupts while dealing with this one
  //HWREG(SSI0_BASE + SSI_O_IM) &= ~SSI_IM_TXIM;

  //read command in on Rx line (MISO)

  //HWREG(SSI0_BASE + SSI_O_DR); // trash
  //HWREG(SSI0_BASE + SSI_O_DR); // trash
  command = HWREG(SSI0_BASE + SSI_O_DR); // actually the real one

  ES_Event_t ThisEvent;
  ThisEvent.EventType = EOT;
  ThisEvent.EventParam = command;
  PostCommGenService(ThisEvent);

  //HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
}
