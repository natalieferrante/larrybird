/****************************************************************************
 Module
   CommGenService.c

 Revision
   1.0.1

 Description
   This is a Command Generator service file for reading the commands from the
   Command Generator under the Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 02/04/2020 8:43 nat     built service
 02/07/2020      jul     updated. working.
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ssi.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"

#include "DriveFSM.h"
#include "InterruptLib.h"

#include "ES_Configure.h"
#include "ES_Framework.h"

//#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/

#define NO_COMMAND 0xFF
#define QUERY_MISO 0xAA
#define VOID_BYTES 0x00
#define L 1
#define R 2
#define HALF 1
#define FULL 2
#define HALF_TIME 1500        //1.5s
#define QUARTER_TIME 1000     //1s
#define COMM_TIME 100

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t  MyPriority;
uint32_t        BitsPerNibble = 4;
static uint16_t command;
static uint16_t lastcommand = NO_COMMAND;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitCommGenService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     Natalie Ferrante, 1/30/2020
****************************************************************************/
bool InitCommGenService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/

//1. Enable the clock to the GPIO port
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;

//2. Enable the clock to SSI module
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;

//3. Wait for the GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
  {}

//4. Program the GPIO to use the alternate functions on the SSI pins
  /*  "The digital alternate hardware functions are enabled by setting the
      appropriate bit in the GPIO Alternate Function Select (GPIOAFSEL) (Step 4)
      and GPIODEN registers (Step 7.) and configuring the PMCx bit field in the GPIO
      Port Control (GPIOPCTL) (Step 5.) register to the numeric encoding shown in
      the table below" pg 650-651 (also see pg 953-4)
  */
  //SSI0Fss   IO:PA2   Pin:19 on Tiva
  //SSI0Clk   IO:PA3   Pin:20 on Tiva
  //SSI0Rx    IO:PA4   Pin:21 on Tiva (MISO)
  //SSI0Tx    IO:PA5   Pin:22 on Tiva (MOSI)
  //Set these pins high on portA in GPIOPCTL to use these alternate functions
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

//5. Set mux position in GPIOPCTL to select the SSI use of the pins
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 2 for SSI0Fss, SSI0Clk, SSI0Rx, SSI0Tx
  // Clear bits 2, 3, 4, 5 with 0xff0000ff
  // Copy syntax from PWM library function doing the same thing
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) + //clear 2, 3, 4, 5
      (2 << (2 * BitsPerNibble)) +                          //SSI0Fss
      (2 << (3 * BitsPerNibble)) +                          //SSI0Clk
      (2 << (4 * BitsPerNibble)) +                          //SSI0Rx
      (2 << (5 * BitsPerNibble));                           //SSI0Tx

//6. Program the port lines for digital I/O (use GPIO_O_DEN)
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

//7. Program the required data directions on the port lines
  // SSI0Fss - out, SSI0Clk - out, SSI0Rx - in, SSI0Tx - out
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= BIT4LO; //MISO line

  HWREG(GPIO_PORTA_BASE + (GPIO_O_DATA + ALL_BITS)) |= (BIT2HI | BIT3HI | BIT5HI); // testing

//8. If using SPI mode 3, program the pull-up on the clock line
  // Since we are using Mode 3, we need a pull-up on the clock line (SSI0Fss)
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= BIT2HI;

//9. Wait for the SSI0 to be ready (see above, syntax)
  while ((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R0) != SYSCTL_PRSSI_R0)
  {}
//10. Make sure that the SSI is disabled before programming mode bits
  //SSI Synchronous Serial Port Enable
  // Name: SSE (bit 3)
  // Value Description
  //  0 SSI operation is disabled.
  //  1 SSI operation is enabled.
  //    Note: This bit must be cleared before any control registers
  //    are reprogrammed. (pg 972)
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_SSE);  //name is HI setting, ~ for LO set

//11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
//SSI Master/Slave Select
// Name: MS (bit 2)
//    This bit selects Master or Slave mode and can be modified only when
//    the SSI is disabled (SSE=0).
//  Value Description
//    0 The SSI is configured as a master.
//    1 The SSI is configured as a slave. (pg 972)
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_MS);

  // End of Transmission
  //  This bit is only valid for Master mode devices and operations (MS = 0x0).
  //   Value    Description
  //   1         The TXRIS interrupt indicates that the transmit FIFO is half full
  //              or less.
  //   0         The End of Transmit interrupt mode for the TXRIS interrupt is
  //              enabled.
  HWREG(SSI0_BASE + SSI_O_CR1) |= (SSI_CR1_EOT);

//12. Configure the SSI clock source to the system clock
  //SSI Baud Clock Source
  //The following table specifies the source that generates for the SSI baud
  //clock:
  //  Value     Description
  //  0x0       System clock (based on clock source and divisor factor)
  //  0x1-0x4   Reserved
  //  0x5       PIOSC
  //  0x6-0xF   Reserved  (pg 984)
  HWREG(SSI0_BASE + SSI_O_CC) |= (SSI_CC_CS_SYSPLL);

//13. Configure the clock pre-scaler
// In class found max frequency 917 kHz
// Set bitrate to 50kHz
// bitrate = SYSCLCK / (CPSR * (1+SCR) = 40MHz / 800 = 40 / (800 * (1+0))
// Set CPSR = 100, set SCR = 7
  HWREG(SSI0_BASE + SSI_O_CPSR) = 2;     // testing

//14. Configure clock rate (SCR), phase & polarity (SPH, SPO), mode (FRF),
//data size (DSS)
//Set SCR = 7
  //HWREG(SSI0_BASE + SSI_O_CR0) = 0x00;
  // configure SPH = 1, because data is captured at the second edge
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH;
  // configure SPO = 1, because idle state is high
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO;
  // set freescale SPI Mode
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & ~SSI_CR0_SCR_M) + (21 << SSI_CR0_SCR_S); // testing SCR = 21

  //Set data size to 8 bits
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8;

//15. Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

//16. Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;

//17. Enable the NVIC interrupt for the SSI when starting to transmit
  //enable bit 7 in EN0
  HWREG(NVIC_EN0) |= BIT7HI;     //when disable EN = DIS

  //globally enable interrupts (done elsewhere, comment out)
  __enable_irq();

  //Initialize the timer at 100 ms
  ES_Timer_InitTimer(CommGenTimer, COMM_TIME);

  HWREG(SSI0_BASE + SSI_O_DR) = QUERY_MISO;

  printf("\n Completed Initialization CommGenService \r\n");

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostCommGenService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Natalie Ferrante, 1/30/2020
****************************************************************************/
bool PostCommGenService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunCommGenService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Runs the Communication Generator service. Posts to DriveFSM.
 Notes

 Author
   Natalie Ferrante, 1/30/2020
****************************************************************************/
ES_Event_t RunCommGenService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  //happens only if we timout, send query
  if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == CommGenTimer))
  {
    //read the command
    HWREG(SSI0_BASE + SSI_O_DR) = QUERY_MISO;

    //Restart timer
    ES_Timer_InitTimer(CommGenTimer, COMM_TIME);
  }

  //Do if statement, only happens if we get EOT interrupt
  if (ThisEvent.EventType == EOT)
  {
    //read the command from the MISO line
    command = ThisEvent.EventParam;

    //if(command != 0 )
    //Only respond if one of the viable commands
    if ((command != NO_COMMAND) && (command != lastcommand))
    {
      //printf("\n\r Commmand: %x\r\n", command);

      //if 0x00
      if (command == 0x00)
      {
        //Post STOP to Drive FSM
        ES_Event_t ThisEvent;
        ThisEvent.EventType = STOP;
        PostDriveFSM(ThisEvent);

        //else if 0x02
      }
      else if (command == 0x02)
      {
        //Post TURN to FSM with parameter = R set timer for 90 deg
        ES_Event_t ThisEvent;
        ThisEvent.EventType = TURN;
        ThisEvent.EventParam = R;
        //set timer for 90 degrees
        ES_Timer_InitTimer(TURN_TIMER, HALF_TIME);
        PostDriveFSM(ThisEvent);

        //else if 0x03
      }
      else if (command == 0x03)
      {
        //post TURN to FSM with parameter = R, set timer for 45 deg
        ES_Event_t ThisEvent;
        ThisEvent.EventType = TURN;
        ThisEvent.EventParam = R;
        //set timer for 45 degrees
        ES_Timer_InitTimer(TURN_TIMER, QUARTER_TIME);
        PostDriveFSM(ThisEvent);

        //else if 0x04
      }
      else if (command == 0x04)
      {
        //Post TURN to FSM with parameter = L set timer for 90 deg
        ES_Event_t ThisEvent;
        ThisEvent.EventType = TURN;
        ThisEvent.EventParam = L;
        //set timer for 90 degrees
        ES_Timer_InitTimer(TURN_TIMER, HALF_TIME);
        PostDriveFSM(ThisEvent);

        //else if 0x05
      }
      else if (command == 0x05)
      {
        //Post TURN to FSM with parameter = L set timer for 45 deg
        ES_Event_t ThisEvent;
        ThisEvent.EventType = TURN;
        ThisEvent.EventParam = L;
        //set timer for 45 degrees
        ES_Timer_InitTimer(TURN_TIMER, QUARTER_TIME);
        PostDriveFSM(ThisEvent);

        //else if 0x08
      }
      else if (command == 0x08)
      {
        //Post FORWARD to FSM with param = HALF
        ES_Event_t ThisEvent;
        ThisEvent.EventType = FORWARD;
        ThisEvent.EventParam = HALF;
        PostDriveFSM(ThisEvent);

        //else if 0x09
      }
      else if (command == 0x09)
      {
        //post FORWARD to FSM with param = FULL
        ES_Event_t ThisEvent;
        ThisEvent.EventType = FORWARD;
        ThisEvent.EventParam = FULL;
        PostDriveFSM(ThisEvent);

        //else if 0x10
      }
      else if (command == 0x10)
      {
        //Post BACKWARD to FSM with param = HALF
        ES_Event_t ThisEvent;
        ThisEvent.EventType = BACKWARD;
        ThisEvent.EventParam = HALF;
        PostDriveFSM(ThisEvent);

        //else if 0x11
      }
      else if (command == 0x11)
      {
        //Post BACKWARD to FSM with param = FULL
        ES_Event_t ThisEvent;
        ThisEvent.EventType = BACKWARD;
        ThisEvent.EventParam = FULL;
        PostDriveFSM(ThisEvent);

        //else if 0x20
      }
      else if (command == 0x20)
      {
        //Post ALIGN to FSM with no param
        ES_Event_t ThisEvent;
        ThisEvent.EventType = ALIGN;
        PostDriveFSM(ThisEvent);

        //else if 0x40
      }
      else if (command == 0x40)
      {
        //Post DETECT to FSM with no param
        ES_Event_t ThisEvent;
        ThisEvent.EventType = DETECT;
        PostDriveFSM(ThisEvent);
      }
      // Store the value of the completed command
      lastcommand = command;
    }
  }

  // Reinit timer
  ES_Timer_InitTimer(CommGenTimer, COMM_TIME);

  return ReturnEvent;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
