/****************************************************************************
 Module
   DriveService.c

 Description
   This is a hierarchal state machine (level 2) that handles MINER acquisition.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/19/20       AS      Added Drive functionality for check off
 02/26/20       NF      Updated with encoder functionality
 02/28/20       NF      Added PI control
 03/02/20     AS + JC   Fixed PI control. Found potential hardware issue
 03/03/20       JC      Rewrote code to reduce posting; added query.

****************************************************************************/

/****************************************************************************
 Module
   DriveService.c

 Description
   This service responds to requests to control the motors.

Natalie Ferrante & Amanda Spyropoulos
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#include "DriveService.h"
#include "ES_Events.h"     /* gets ES_Events */

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "bitdefs.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "GameplayHSM.h"
#include "AcquireMinerSM.h"

/* include header files for the other modules in Lab8 that are referenced */

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/*----------------------------- Module Defines ----------------------------*/

#define TicksPerMS 40000
// 40,000 ticks per mS assumes a 40Mhz clock, we will use SysClk/32 for PWM
#define PWMTicksPerUS 10      //40000 / 4
#define PeriodInUS 200        //us
// program generator A to go to 1 at rising comare A, 0 on falling compare A
#define GenA_Normal_0 (PWM_0_GENA_ACTCMPAU_ONE | PWM_0_GENA_ACTCMPAD_ZERO)
#define GenA_Normal_1 (PWM_1_GENA_ACTCMPAU_ONE | PWM_1_GENA_ACTCMPAD_ZERO)
// program generator B to go to 1 at rising comare B, 0 on falling compare B
#define GenB_Normal_0 (PWM_0_GENB_ACTCMPBU_ONE | PWM_0_GENB_ACTCMPBD_ZERO)
#define GenB_Normal_1 (PWM_1_GENB_ACTCMPBU_ONE | PWM_1_GENB_ACTCMPBD_ZERO)
#define BitsPerNibble 4
#define msInSec 1000            // constant - 1000 ms per second
#define secInMin 60             // constant - 60 sec per min
#define GR 50                   // Encoder Gear Ratio
#define Encoder_Resolution 6    //12 for 2 sides, 6 for only one
#define Clock_Freq 40 * 1000000 //40MHz
#define PERIODIC_INT_TIME 2
#define MOTOR_UPDATE_TIME 10

// Duty Cycle Values
#define MAX_DUTY_CYCLE 99       // 100% is max DC
#define MIN_DUTY_CYCLE 1        // min duty cycle to turn motor

// RPM for turning
#define TURN_RPM 40

// Pins associated with each motor
#define LEFT_MOTOR_A 6
#define LEFT_MOTOR_B 7
#define RIGHT_MOTOR_A 4
#define RIGHT_MOTOR_B 5

//Parameters that can be passed to our states
#define L 1
#define R 2

//Current Movement, for keeping track of motion
#define STOPPING 0
#define MOVING_FORWARD 1
#define TURNING_RIGHT 2
#define TURNING_LEFT 3
#define REVERSING 4
/*---------------------------- Module Variables ---------------------------*/

static uint8_t  MyPriority;           // priority of service
static uint8_t  Target_RPMLEFT;
static uint8_t  Target_RPMRIGHT;
static uint8_t  RPMLEFT;
static uint8_t  RPMRIGHT;
static uint32_t InputCaptureCountLEFT = 0;
static uint32_t InputCaptureCountRIGHT = 0;
static uint32_t EncoderPeriodLEFT;
static uint32_t EncoderPeriodRIGHT;
static uint32_t LastCaptureLEFT;
static uint32_t LastCaptureRIGHT;
static uint32_t PeriodicTimeoutCount = 0;
static float    pGain = 1.26;           // proportional gain
static float    iGain = 0.01;           // integral gain
static int8_t   RequestedDutyLEFT = 50;
static int8_t   RequestedDutyRIGHT = 50;
static uint32_t ThisCaptureLEFT;
static uint32_t ThisCaptureRIGHT;
static int8_t   CurrentMovement;        // keeps track of current motion
// used to calculate threshold of encoder ticks for turning a specific angle
static float    ThresholdMultiplier = 1.20;   

/*---------------------------- Modular Functions ---------------------------*/

static void initPins(void);
static void stop(void);
static void forward(void);
static void reverse(void);
static void turn_right(void);
static void turn_left(void);
;
static void RestoreDC(void);
static void EncoderInitInputCaptureLEFT(void);
static void EncoderInitInputCaptureRIGHT(void);
static void InitPeriodicInt(void);
static float clamp(float val, float clampL, float clampH);
static void TestingFunc(void);

/*---------------------------- Service Functions ---------------------------*/

/****************************************************************************
 * InitDriveService
 * Takes a priority number, returns true
****************************************************************************/
bool InitDriveService(uint8_t Priority)
{
  MyPriority = Priority;
  initPins();                 // initialize the PWM pins
  stop();                     // begin in a stopped state
  CurrentMovement = STOPPING; // record that we are stopping

  // Initialize all pins and interrupts
  InitPeriodicInt();                // Interrupt initialization
  printf("\n\rInitialized Periodic Interrupt");
  EncoderInitInputCaptureLEFT();    // Encoder initialization
  EncoderInitInputCaptureRIGHT();   // Encoder initialization
  printf("\n\rInitialized Input Capture");

  // Initialize timing loop
  ES_Timer_InitTimer(MOTOR_UPDATE_TIMER, MOTOR_UPDATE_TIME);

  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
  printf("\n\rInitialized Drive Service");

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  { // Return true for successful initialization
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 * RunDriveService
 * Receives an event from DriveFSM
 * Moves the robot forward/backward/turn at full or half speed, as needed
****************************************************************************/
ES_Event_t RunDriveService(ES_Event_t ThisEvent)
{
  static bool     SPECIFIC_TURN_FLAG = false;
  static uint16_t CAPTURE_THRESHOLD = 0;

  ES_Event_t      ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  TestingFunc();  // testing function 

  switch (ThisEvent.EventType)
  {
    // Update motion using updated duty cycle values from PI control
    case ES_TIMEOUT:
    {
      // Re-initialize motor timer
      ES_Timer_InitTimer(MOTOR_UPDATE_TIMER, MOTOR_UPDATE_TIME);

      // Set motors to motion required by the stored current movement
      if (CurrentMovement == STOPPING)
      {
        stop();
      }
      else if (CurrentMovement == MOVING_FORWARD)
      {
        forward();
      }
      else if (CurrentMovement == REVERSING)
      {
        reverse();
      }
      else if (CurrentMovement == TURNING_LEFT)
      {
        // Check if you were supposed to turn 90 degrees, and if you've met that goal
        if ((SPECIFIC_TURN_FLAG == true) && (InputCaptureCountLEFT >= CAPTURE_THRESHOLD))
        {
          // Stop the motors once 90 degrees is reached
          ES_Event_t ThisEvent;
          ThisEvent.EventType = EU_STOP;
          PostDriveService(ThisEvent);
          CurrentMovement = STOPPING;
          SPECIFIC_TURN_FLAG = false;    // Finished turning 90
          // Post to Gameplay that 90 degree turn has been completed
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_TURN_COMPLETED;
          PostGameplayHSM(PostEvent);
          printf("\r\nFinished turn, STOP!");
        }
        else
        {
          turn_left();  // keep turning left
        }
      }
      else if (CurrentMovement == TURNING_RIGHT)
      {
        // Check if you were supposed to turn 90 degrees, and if you've met that goal
        if ((SPECIFIC_TURN_FLAG == true) && (InputCaptureCountRIGHT >= CAPTURE_THRESHOLD))
        {
          // Stop the motors once 90 degrees is reached
          ES_Event_t ThisEvent;
          ThisEvent.EventType = EU_STOP;
          PostDriveService(ThisEvent);
          CurrentMovement = STOPPING;
          SPECIFIC_TURN_FLAG = false;    // Finished turning 90
          // Post to Gameplay that 90 degree turn has been completed
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_TURN_COMPLETED;
          PostGameplayHSM(PostEvent);
          printf("\r\nFinished turn, STOP!");
        }
        else
        {
          turn_right(); // keep turning right
        }
      }
    }
    break;

    // Sort which type of motion and set target RPM based on the event type
    case EU_STOP:
    {
      Target_RPMLEFT = 0;   // Set RPM goal to 0
      Target_RPMRIGHT = 0;
      SPECIFIC_TURN_FLAG = false;   // Set turn flag to false
      CurrentMovement = STOPPING;   // Set current motion to stop
    }
    break;
    case EU_FORWARD:
    {
      Target_RPMLEFT = ThisEvent.EventParam;  // Set RPM goal to event parameter
      Target_RPMRIGHT = Target_RPMLEFT;
      CurrentMovement = MOVING_FORWARD;       // Set current motion to forward
    }
    break;
    case EU_REVERSE:
    {
      Target_RPMLEFT = ThisEvent.EventParam;  // Set RPM goal to event parameter
      Target_RPMRIGHT = Target_RPMLEFT;
      CurrentMovement = REVERSING;            // Set current motion to reverse
    }
    break;
    case EU_TURN_LEFT:
    {
      Target_RPMLEFT = TURN_RPM;              // Set RPM goal to the turn RPM
      Target_RPMRIGHT = Target_RPMLEFT;
      CurrentMovement = TURNING_LEFT;         // Set current motion to turn left
      // If the turn is set to a specific angle 
      if (ThisEvent.EventParam != UNKNOWN_TURN)
      {
        SPECIFIC_TURN_FLAG = true;    // Set the specific turn flag to true
        InputCaptureCountRIGHT = 0;   // Reset the input capture counters
        InputCaptureCountLEFT = 0;
        // Calculate the capture threshold
        CAPTURE_THRESHOLD = ThisEvent.EventParam * ThresholdMultiplier; 
      }
      else
      {
        SPECIFIC_TURN_FLAG = false;   // Otherwise, set turn flag to false
      }
    }
    break;
    case EU_TURN_RIGHT:
    {     
      Target_RPMLEFT = TURN_RPM;            // Set RPM goal to the turn RPM
      Target_RPMRIGHT = Target_RPMLEFT;     
      CurrentMovement = TURNING_RIGHT;      // Set current motion to turn right
      if (ThisEvent.EventParam != UNKNOWN_TURN)
      {
        SPECIFIC_TURN_FLAG = true;    // Set the specific turn flag to true
        InputCaptureCountRIGHT = 0;   // Reset the input capture counters
        InputCaptureCountLEFT = 0;
        // Calculate the capture threshold
        CAPTURE_THRESHOLD = ThisEvent.EventParam * ThresholdMultiplier;
      }
      else
      {
        SPECIFIC_TURN_FLAG = false;   // Otherwise, set turn flag to false
      }
    }
    break;
    default:
    {
      ;
    }
    break;
  }
  return ReturnEvent; // Return the event (ES_NO_EVENT)
}

/****************************************************************************
 * PostDriveService
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
bool PostDriveService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/*---------------------------- Module Functions ---------------------------*/

/****************************************************************************
 * RestoreDC
 * Call this function to revert to DC between 1-99
****************************************************************************/
static void RestoreDC(void)
{
  // To restore the previous DC, simply set the action back to the normal actions
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal_0;
  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal_0;
  HWREG(PWM0_BASE + PWM_O_1_GENA) = GenA_Normal_1;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = GenB_Normal_1;
}

/****************************************************************************
 turn_left
 The robot will turn left at the given duty cycle
****************************************************************************/
static void turn_left()
{
  RestoreDC();  // restores any pins

  //Left motor set duty
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - RequestedDutyLEFT));  
  //Right Motor set duty
  HWREG(PWM0_BASE + PWM_O_1_CMPB) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - RequestedDutyRIGHT)); 

  //Set GEN A & B
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENA_ACTZERO_ZERO;    //set pin to LOW
  HWREG(PWM0_BASE + PWM_O_1_GENA) = PWM_1_GENB_ACTZERO_ZERO;    //set pin to LOW
}

/****************************************************************************
 turn right
 The robot will turn right at the given duty cycle
****************************************************************************/
static void turn_right()
{
  RestoreDC();  // restores any pins

  //Left motor set duty
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - RequestedDutyLEFT)); 
  //Right motor set duty
  HWREG(PWM0_BASE + PWM_O_1_CMPA) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - RequestedDutyRIGHT));

  //Set GEN A & B
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;    // set pin to LOW
  HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;    // set pin to LOW
}

/****************************************************************************
 forward
 The robot will go forward at the given duty cycle
****************************************************************************/
static void forward()
{
  RestoreDC();  // restore pins

  //Left motor set duty
  HWREG(PWM0_BASE + PWM_O_0_CMPB) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - RequestedDutyLEFT)); 
  //Right motor set duty
  HWREG(PWM0_BASE + PWM_O_1_CMPB) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - RequestedDutyRIGHT)); 

  //Set GEN A to low
  HWREG(PWM0_BASE + PWM_O_0_GENA) = PWM_0_GENA_ACTZERO_ZERO;    // set pin to LOW
  HWREG(PWM0_BASE + PWM_O_1_GENA) = PWM_1_GENA_ACTZERO_ZERO;    // set pin to LOW
}

/****************************************************************************
  reverse
  The robot will drive forward at the given duty cycle
****************************************************************************/
static void reverse()
{
  RestoreDC();  // restore pins

  //Left motor set duty
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - RequestedDutyLEFT)); 
  //Right motor set duty
  HWREG(PWM0_BASE + PWM_O_1_CMPA) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - RequestedDutyRIGHT)); 

  //Set GEN B to low
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;    // set pin to LOW
  HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;    // set pin to LOW
}

/****************************************************************************
 stop
 Sets the duty cycle on all four pins to 0. Stops the robot.
****************************************************************************/
static void stop(void)
{
  RestoreDC();  // restores any pins

  //Left motor set duty
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = (HWREG(PWM0_BASE + PWM_O_0_LOAD) / 100 * (100 - MIN_DUTY_CYCLE));  
  //Right motor set duty
  HWREG(PWM0_BASE + PWM_O_1_CMPA) = (HWREG(PWM0_BASE + PWM_O_1_LOAD) / 100 * (100 - MIN_DUTY_CYCLE)); 
  //Set GEN A & B
  HWREG(PWM0_BASE + PWM_O_0_GENB) = PWM_0_GENB_ACTZERO_ZERO;    // set pin to LOW
  HWREG(PWM0_BASE + PWM_O_1_GENB) = PWM_1_GENB_ACTZERO_ZERO;    // set pin to LOW
}

/****************************************************************************
 * initPins
 * Initialize all four PWM pins for the robot.
 * PB 4 = Left Motor
 * PB 5 = Left Motor
 * PB 6 = Right Motor
 * PB 7 = Right Motor
*****************************************************************************/
static void initPins(void)
{
  // start by enabling the clock to the PWM Module (PWM0) for PB 4-7
  HWREG(SYSCTL_RCGCPWM) |= SYSCTL_RCGCPWM_R0;
  // enable the clock to Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  // Select the PWM clock as System Clock/32
  HWREG(SYSCTL_RCC) = (HWREG(SYSCTL_RCC) & ~SYSCTL_RCC_PWMDIV_M) |
      (SYSCTL_RCC_USEPWMDIV | SYSCTL_RCC_PWMDIV_32);

  // make sure that the PWM module clock has gotten going
  while ((HWREG(SYSCTL_PRPWM) & SYSCTL_PRPWM_R0) != SYSCTL_PRPWM_R0)
  {
    ;
  }
  // disable the PWM while initializing for all four pins
  HWREG(PWM0_BASE + PWM_O_0_CTL) = 0;
  HWREG(PWM0_BASE + PWM_O_1_CTL) = 0;

  // program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
  // for all four pins
  HWREG(PWM0_BASE + PWM_O_0_GENA) = GenA_Normal_0;
  HWREG(PWM0_BASE + PWM_O_1_GENA) = GenA_Normal_1;

  HWREG(PWM0_BASE + PWM_O_0_GENB) = GenB_Normal_0;
  HWREG(PWM0_BASE + PWM_O_1_GENB) = GenB_Normal_1;

  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  HWREG(PWM0_BASE + PWM_O_0_LOAD) = ((PeriodInUS * PWMTicksPerUS)) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_LOAD) = ((PeriodInUS * PWMTicksPerUS)) >> 1;

  // Set the initial Duty cycle on all four pins to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  HWREG(PWM0_BASE + PWM_O_0_CMPA) = HWREG(PWM0_BASE + PWM_O_0_LOAD) >> 1;
  HWREG(PWM0_BASE + PWM_O_1_CMPA) = HWREG(PWM0_BASE + PWM_O_1_LOAD) >> 1;

  // enable the PWM outputs for all four pins
  HWREG(PWM0_BASE + PWM_O_ENABLE) |= (PWM_ENABLE_PWM0EN | PWM_ENABLE_PWM1EN | PWM_ENABLE_PWM2EN | PWM_ENABLE_PWM3EN);

  // now configure the Port B pins to be PWM outputs
  // start by selecting the alternate function for PB4-7
  HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= BIT4HI | BIT5HI | BIT6HI | BIT7HI;

  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 4, 5, 6, 7
  HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0x00ffffff)
      + (4 << (4 * BitsPerNibble)) + (4 << (5 * BitsPerNibble)) + (4 << (6 * BitsPerNibble)) + (4 << (7 * BitsPerNibble));

  // Enable pins 4, 5, 6, 7 on Port B for digital I/O
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT7HI | BIT6HI | BIT5HI | BIT4HI;

  // make pins 4-7 on Port B into outputs
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT7HI | BIT6HI | BIT5HI | BIT4HI);

  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count
  HWREG(PWM0_BASE + PWM_O_0_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
      PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
  HWREG(PWM0_BASE + PWM_O_1_CTL) = (PWM_0_CTL_MODE | PWM_0_CTL_ENABLE |
      PWM_0_CTL_GENAUPD_LS | PWM_0_CTL_GENBUPD_LS);
}

/*---------------------------- Encoder Functions ---------------------------*/

/****************************************************************************
 EncoderInitInputCaptureLEFT
 Initializes Interrupts and pins for input capture
 Uses Wide Timer 1, subtimer A (Left) and subtimer B (Right)
 Uses Pins PC6 for Left Encoder, and PC7 for Right Encoder
 Natalie Ferrante
****************************************************************************/
static void EncoderInitInputCaptureLEFT(void)
{
  // start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;

  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;

  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A & B) is disabled before configuring
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value for both A & B :-)
  HWREG(WTIMER1_BASE + TIMER_O_TAILR) = 0xffffffff;

  // set up timers A and B in capture mode (TAMR=3 (both), TAAMS = 0, TBAMS = 0),
  // for edge time (TACMR = 1, TBCMR = 1) and up-counting (TACDIR = 1, TBCDIR = 1)
  HWREG(WTIMER1_BASE + TIMER_O_TAMR) =
      (HWREG(WTIMER1_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
      (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);

  // To set the event to rising edge, we need to modify the TAEVENT &
  // TBEVENT bits in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;

  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bits 6 & 7 (WT1CCP0 & WT1CCP1)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= (BIT6HI);

  // Then, map bit 6 & 7's alternate functions to WT1CCP0 & WT1CCP1
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 7 for WT1CCP0, WT1CCP1
  // Clear bits 6 & 7 with 0xff0000ff
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0xf0ffffff) + //clear 6, 7
      (7 << (6 * BitsPerNibble));                           //WT1CCP0

  // Enable pins 6 & 7 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT6HI);

  // make pin 6 & 7 on Port C into inputs
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= (BIT6LO);

  // back to the timer to enable a local capture interrupt (A & B)
  HWREG(WTIMER1_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;

  // enable Timer A & B in Wide Timer 1 interrupt in the NVIC
  // Timer 1A is interrupt 96, Timer 1B is interrupt 97 (pg 104)
  // int 96 & 97 are on EN3 on NVIC (Int 96-127) as the first two
  // so set Bit 0 and Bit 1 high
  HWREG(NVIC_EN3) = (BIT0HI);

  // make sure interrupts are enabled globally
  __enable_irq();

  // now kick the timers off by enabling A & B and enabling the timers to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 EncoderInitInputCaptureRIGHT
 Initializes Interrupts and pins for input capture
 Uses Wide Timer 1, subtimer A (Left) and subtimer B (Right)
 Uses Pins PC6 for Left Encoder, and PC7 for Right Encoder
 Natalie Ferrante
****************************************************************************/
static void EncoderInitInputCaptureRIGHT(void)
{
  // start by enabling the clock to the timer (Wide Timer 1)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R1;

  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;

  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A & B) is disabled before configuring
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER1_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value for both A & B :-)
  HWREG(WTIMER1_BASE + TIMER_O_TBILR) = 0xffffffff;

  // set up timers A and B in capture mode (TAMR=3 (both), TAAMS = 0, TBAMS = 0),
  // for edge time (TACMR = 1, TBCMR = 1) and up-counting (TACDIR = 1, TBCDIR = 1)
  HWREG(WTIMER1_BASE + TIMER_O_TBMR) =
      (HWREG(WTIMER1_BASE + TIMER_O_TBMR) & ~TIMER_TBMR_TBAMS) |
      (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);

  // To set the event to rising edge, we need to modify the TAEVENT &
  // TBEVENT bits in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER1_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEVENT_M;

  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bits 6 & 7 (WT1CCP0 & WT1CCP1)
  HWREG(GPIO_PORTC_BASE + GPIO_O_AFSEL) |= (BIT7HI);

  // Then, map bit 6 & 7's alternate functions to WT1CCP0 & WT1CCP1
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 7 for WT1CCP0, WT1CCP1
  // Clear bits 6 & 7 with 0xff0000ff
  HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTC_BASE + GPIO_O_PCTL) & 0x0fffffff) + //clear 6, 7
      (7 << (7 * BitsPerNibble));                           //WT1CCP1

  // Enable pins 6 & 7 on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE + GPIO_O_DEN) |= (BIT7HI);

  // make pin 6 & 7 on Port C into inputs
  HWREG(GPIO_PORTC_BASE + GPIO_O_DIR) &= (BIT7LO);

  // back to the timer to enable a local capture interrupt (A & B)
  HWREG(WTIMER1_BASE + TIMER_O_IMR) |= TIMER_IMR_CBEIM;

  // enable Timer A & B in Wide Timer 1 interrupt in the NVIC
  // Timer 1A is interrupt 96, Timer 1B is interrupt 97 (pg 104)
  // int 96 & 97 are on EN3 on NVIC (Int 96-127) as the first two
  // so set Bit 0 and Bit 1 high
  HWREG(NVIC_EN3) = (BIT1HI);

  // make sure interrupts are enabled globally
  __enable_irq();

  // now kick the timers off by enabling A & B and enabling the timers to
  // stall while stopped by the debugger
  HWREG(WTIMER1_BASE + TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

/****************************************************************************
 EncoderLEFTInputCaptureResponse
 Response to input at Wide Timer 1, subtimer A
 Captures Encoder edges at PC6 (Left motor), calculates the period
 Natalie Ferrante
****************************************************************************/
void EncoderLEFTInputCaptureResponse(void)
{
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER1_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;

  // now grab the captured value on left motor and calculate the period
  ThisCaptureLEFT = HWREG(WTIMER1_BASE + TIMER_O_TAR);
  EncoderPeriodLEFT = ThisCaptureLEFT - LastCaptureLEFT;

  // update LastCaptureLEFT to prepare for the next edge
  LastCaptureLEFT = ThisCaptureLEFT;

  //Increment Counter
  InputCaptureCountLEFT++;
}

/****************************************************************************
 EncoderRIGHTInputCaptureResponse
 Response to input at Wide Timer 1, subtimer B
 Captures Encoder edges at PC7 (Right motor), calculates the period
 Natalie Ferrante
****************************************************************************/
void EncoderRIGHTInputCaptureResponse(void)
{
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER1_BASE + TIMER_O_ICR) = TIMER_ICR_CBECINT;

  // now grab the captured value on left motor and calculate the period
  ThisCaptureRIGHT = HWREG(WTIMER1_BASE + TIMER_O_TBR);
  EncoderPeriodRIGHT = ThisCaptureRIGHT - LastCaptureRIGHT;

  // update LastCaptureRIGHT to prepare for the next edge
  LastCaptureRIGHT = ThisCaptureRIGHT;

  //Increment Counter
  InputCaptureCountRIGHT++;
}

/****************************************************************************
 InitPeriodicInt
 Implements PI Control function on motors based on encoder values & desired RPM
 Periodic Interrupt set on Wide Timer 0 subtimer B

 Natalie Ferrante
****************************************************************************/
static void InitPeriodicInt(void)
{
  //start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;

  // kill a few cycles to let the clock get going
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0)
  {}

  // make sure that timer (Timer B) is disabled before configuring
  HWREG(WTIMER0_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TBEN;

  // set it up in 32bit wide (individual, not concatenated) mode
  HWREG(WTIMER0_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;

  // set up timer B in periodic mode so that it repeats the time-outs
  HWREG(WTIMER0_BASE + TIMER_O_TBMR) =
      (HWREG(WTIMER0_BASE + TIMER_O_TBMR) & ~TIMER_TBMR_TBMR_M) |
      TIMER_TBMR_TBMR_PERIOD;

  // set timeout DEFINED ABOVE
  HWREG(WTIMER0_BASE + TIMER_O_TBILR) = TicksPerMS * PERIODIC_INT_TIME;

  // enable a local timeout interrupt
  HWREG(WTIMER0_BASE + TIMER_O_IMR) |= TIMER_IMR_TBTOIM;

  // enable the Timer B in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 95 so appears in EN2 at bit 31
  HWREG(NVIC_EN2) |= BIT31HI;

  // make sure interrupts are enabled globally
  __enable_irq();

  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER0_BASE + TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

/****************************************************************************
 PeriodicIntResponse
 Implements PI Control function on motors based on encoder values & desired RPM
 Periodic Interrupt set on Wide Timer 0 subtimer B
 Natalie Ferrante
****************************************************************************/
void PeriodicIntResponse(void)
{
  // start by clearing the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_TBTOCINT;

  //run the closed loop control
  ControlLaw();

  // increment our counter so that we can tell this is running
  ++PeriodicTimeoutCount;
}

/****************************************************************************
 ControlLaw
 Implements PI Control function on motors based on encoder values & desired RPM
 Calculated for both motors
 Natalie Ferrante
****************************************************************************/
void ControlLaw(void)
{
  static float    IntegralTermLEFT = 0.0; /* integrator control effort */
  static float    IntegralTermRIGHT = 0.0;
  static float    RPMErrorLEFT;        /* make static for speed */
  static float    RPMErrorRIGHT;
  static uint32_t ThisPeriodLEFT;       /* make static for speed */
  static uint32_t ThisPeriodRIGHT;      /* make static for speed */

  // Update this period 
  ThisPeriodLEFT = EncoderPeriodLEFT;
  ThisPeriodRIGHT = EncoderPeriodRIGHT;

  // Calculate the RPM based on this period
  //12 counts per revolution of the motor when counting both edges of both channels
  // for encoder resolution (so 6 counts per rev for only one), with motor GR = 50
  //Calculate: SYSCLCK[Hz] * 1000[ms/s] * 60[s/min] / Period [ms] / (resolution*GR)
  RPMLEFT = Clock_Freq * 1000 * 60 / ThisPeriodLEFT / (Encoder_Resolution * GR);
  RPMRIGHT = Clock_Freq * 1000 * 60 / ThisPeriodRIGHT / (Encoder_Resolution * GR);

  //Calculate both motors' error
  RPMErrorLEFT = Target_RPMLEFT - RPMLEFT;
  RPMErrorRIGHT = Target_RPMRIGHT - RPMRIGHT;

  // get integral terms for both motors
  IntegralTermLEFT += iGain * RPMErrorLEFT;
  IntegralTermLEFT = clamp(IntegralTermLEFT, 0, 100 / pGain);  /* anti-windup */

  IntegralTermRIGHT += iGain * RPMErrorRIGHT;
  IntegralTermRIGHT = clamp(IntegralTermRIGHT, 0, 100 / pGain);  /* anti-windup */

  //Calculate updated duty cycle to send to motors; updates modular variable
  RequestedDutyLEFT = (pGain * ((RPMErrorLEFT) + IntegralTermLEFT));
  RequestedDutyLEFT = clamp(RequestedDutyLEFT, MIN_DUTY_CYCLE, MAX_DUTY_CYCLE);

  RequestedDutyRIGHT = (pGain * ((RPMErrorRIGHT) + IntegralTermRIGHT));
  RequestedDutyRIGHT = clamp(RequestedDutyRIGHT, MIN_DUTY_CYCLE, MAX_DUTY_CYCLE);
}

/****************************************************************************
 Clamp
 Ensures that Duty cycle will not go above or below the max & min values
constrain val to be in range clampL to clampH
 Natalie Ferrante
****************************************************************************/
static float clamp(float val, float clampL, float clampH)
{
  if (val > clampH)   // if too high
  {
    return clampH;    // Return upper bound
  }
  if (val < clampL)   // if too low
  {
    return clampL;    // Return lower bound
  }
  return val;         // if OK as-is
}

/****************************************************************************
 QueryCurrentMovement
 Allows other modules (i.e. CollisionSM) to query the robot's live motion
 Julea Chin
****************************************************************************/
uint8_t QueryCurrentMovement(void)
{
  return CurrentMovement;
}

/****************************************************************************
 TestingFunc
 Allows for printouts of variables  of interest
 Natalie Ferrante
****************************************************************************/
static void TestingFunc(void)
{
  //printf("\n\r Target RPM LEFT, Right = %u , %u", Target_RPMLEFT, Target_RPMRIGHT);
  //printf("\n\r RPM Left = %d", RPMLEFT);
  //printf("\n\r RPM Right = %d", RPMRIGHT);
  //printf("\n\r Req Duty = %u", RequestedDutyLEFT);
  //printf("\n\r Req Duty = %u", RequestedDutyRIGHT);
  //printf("\n\r Input Capture Count %x", InputCaptureCountRIGHT);
  //printf("\n\r Input Capture Count = %x", InputCaptureCountLEFT);
  //printf("\n\r Periodic = %x", PeriodicTimeoutCount);
}
