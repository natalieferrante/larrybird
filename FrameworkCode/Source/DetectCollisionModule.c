/****************************************************************************
 Module
   DetectCollisionModule.c

 Revision
   1.0.1

 Description
   This module contains an event checker to detect if the bot has run into a wall or an opponent.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 03/04/20 17:20 EGG     Created file
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "DetectCollisionModule.h"
#include "AcquireMinerSM.h"
#include "GameplayHSM.h"
#include "DriveService.h"

// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define FRONT_SWITCHES BIT3HI         // Front switches PB2
#define BACK_SWITCHES BIT2HI          // Back switches PE3

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     CheckSwitch

 Parameters
    None

 Returns
     Nothing

 Description
     Event checker that checks to see if beam has been broken.
****************************************************************************/

bool CheckSwitch(void)
{
  static uint8_t  LastFrontValue = true;
  static uint8_t  LastBackValue = true;
  bool            ReturnVal = false;
  if (QueryCurrentMovement() != STOPPING)  // Check current state of AcquireMiner SM
  {
    uint8_t CurrentFrontValue = HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) & FRONT_SWITCHES;
    uint8_t CurrentBackValue = HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) & BACK_SWITCHES;

    if (((CurrentFrontValue == 0) && (LastFrontValue != 0)) || ((CurrentBackValue == 0) && (LastBackValue != 0)))
    {
      ReturnVal = true;     // Set ReturnVal = True
      // If the MINER has entered the plow
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_COLLISION;
      PostGameplayHSM(PostEvent);
      printf("\r\n Detected Collision");
    }
    LastBackValue = CurrentBackValue;
    LastFrontValue = CurrentFrontValue;
  }
  return ReturnVal; // Return ReturnVal
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
