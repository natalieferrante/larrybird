/****************************************************************************
 Module
   AccelerometerService.c

 Revision
   1.0.1

 Description
   This is an Accelerometer service file for reading the commands from the
   Accelerometer under the Gen2 Events and Services Framework.

 Notes

 History
 When             Who     What/Why
 --------------   ---     --------
 01/16/12 09:58   jec     began conversion from TemplateFSM.c
 02/04/2020 8:43  nat     built CommGenService
 02/07/2020       jul     updated. working for Lab 8.
 02/18/2020       jul     repurposed and updated for SPUD communication
 02/20/2020       ama     repurposed and updated for Accelerometer communication
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "AccelerometerService.h"
#include "SPUDService.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ssi.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "GamePlayHSM.h"
#include "AcquireMinerSM.h"
#include "DriveService.h"

/*----------------------------- Module Defines ----------------------------*/
#define QUERY_ACC_TIMER 1
#define QUERY_TIME_LIMIT 50           // 100 ms
#define SSI_PRESCALER 0x04            // CPSR, 8
#define SCR_VALUE 0x18                // SCR, 9
#define TRASH_BYTE 0x00
// Accelerometer Command Addresses
#define DEVICE_ID 0x00
#define THRESH_TAP 0x1D
#define OFSX 0x1E
#define OFSY 0x1F
#define OFSZ 0x20
#define DUR 0x21
#define TAP_AXES 0x2A
#define ACT_TAP_STATUS 0x2B
#define BW_RATE 0x2C
#define POWER_CTL 0x2D
#define INT_ENABLE 0x2E
#define INT_MAP 0X2F
#define INT_SOURCE 0X30
#define DATA_FORMAT 0X31
#define DATAX0 0X32
#define DATAX1 0X33
#define DATAY0 0X34
#define DATAY1 0X35
#define DATAZ0 0X36
#define DATAZ1 0X37

#define NUM_TRASH_INIT 6
// Accelerometer Commands
#define BW_RATE_PARAMETER 12      //TESTING let's pick 25 Hz to be safe. This has code 1001, which is 1 + 8 = 9. Look at ADXL343 datasheet for codes
#define DATA_FORMAT_PARAMETER 0   // set self test to 0, SPI mode to 4 wire, interrupts to active high, full res to 0, justify to right justified mode, range to 2g
#define ENABLE_MEASUREMENT 1 << 3 // set link mode to off, auto sleep mode to off, measure to ON,  sleep to OFF,  wakeup to 8 hz (although this is not needed)
#define ENABLE_SLEEP 1 << 2       // // set link mode to off, auto sleep mode to off, measure to ON,  sleep to OFF,  wakeup to 8 hz (although this is not needed)
#define READ 1 << 7
#define WRITE 0
#define MULTIWRITE 1 << 6
#define MASK_SIGNED_BIT 1 << 9
#define QUERY_ACC READ | MULTIWRITE | DATAX0 | MB_HI
#define MB_HI 0x40        // Pointer to most signficant bit
#define ACC_MAX_MASK 1023 // this is 10 ones
#define ONE_G 255         // this is one g force
#define ACC_MAX 300
#define ACC_MIN -300
#define MOVING_AVERAGE_WINDOW 16
#define ROLL_ORTHO_THRESHOLD_LOW -12
#define ROLL_ORTHO_THRESHOLD_HIGH -4
#define PITCH_ORTHO_THRESHOLD_LOW -15
#define PITCH_ORTHO_THRESHOLD_HIGH -7
#define BASELINE_ROLL -15
#define BASELINE_PITCH -25
#define BASELINE_YAW -280
#define COLLISION_THRESHOLD_ROLL 300
#define COLLISION_THRESHOLD_YAW 30
#define COUNTORTHOMAX 20

// for a int16_t, the max is 2^ - 1
static uint32_t BitsPerNibble = 4;
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void InitPins(void);
/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority;

// SPI communication data
static int16_t ORTHO_PITCH_THRESHOLD = -20;

//static int16_t BASELINE_ROLL = -15;
//static int16_t BASELINE_PITCH = -25;
//static int16_t BASELINE_YAW = -280;
//static int16_t COLLISION_THRESHOLD_ROLL = 300;
//static int16_t COLLISION_THRESHOLD_YAW = 30;

static uint8_t  RecentQuery;
static uint8_t  NextQuery = QUERY_ACC;
static uint8_t  ReceivedTrash;
static uint8_t  ReceivedByte;
static int32_t  PitchAcceleration = 0;
static int32_t  RollAcceleration = 18;
static int32_t  YawAcceleration = -260;
static uint16_t AccelerometerContent = 0;
static uint16_t x0 = 0;
static uint16_t x1 = 0;
static uint16_t y0 = 0;
static uint16_t y1 = 0;
static uint16_t z0 = 0;
static uint16_t z1 = 0;
static int16_t  pitch_int = 0;
static int16_t  roll_int = 0;
static int16_t  yaw_int = 0;
static bool     initACC = false;
static uint8_t  countOrtho = 0;

static int16_t  movingAverageACC_PITCH[MOVING_AVERAGE_WINDOW];
static int16_t  movingAverageACC_ROLL[MOVING_AVERAGE_WINDOW];
static int16_t  movingAverageACC_YAW[MOVING_AVERAGE_WINDOW];

static bool     Became_Orthogonal_Recently;

//

///*------------------------------ Module Code ------------------------------*/
static void configAccelerometer(void);
static void updateAccelerationData(void);
/****************************************************************************
 * PostAccelerometerService
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
bool PostAccelerometerService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

//End of PostAccelerometerService

bool QueryOrthogonal(void)
{
  return Became_Orthogonal_Recently;
}

/****************************************************************************
 * InitPins
 * Receives nothing, returns nothing.
 * Description
     Sets up the pins for the SS line (assume that the MOSI, MISO, and SLCK lines were already initialized
     SS line is PF3 !
* Author: Natalie Ferrante & Julea Chin & Amanda Spyropoulos
****************************************************************************/
static void InitPins(void)
{
//1. Enable the clock to the GPIO port D
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;

//2. Enable the clock to SSI module 1 (PD0-3)
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R1;

//3. Wait for the GPIO port D to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R3) != SYSCTL_PRGPIO_R3)
  {}

//4. Program the GPIO to use the alternate functions on the SSI pins
  /*  "The digital alternate hardware functions are enabled by setting the
      appropriate bit in the GPIO Alternate Function Select (GPIOAFSEL) (Step 4)
      and GPIODEN registers (Step 7.) and configuring the PMCx bit field in the GPIO
      Port Control (GPIOPCTL) (Step 5.) register to the numeric encoding shown in
      the table below" pg 650-651 (also see pg 953-4) */
  //SSI0Clk   IO:PD0   Pin:23 on Tiva
  //SSI0Fss   IO:PD1   Pin:22 on Tiva
  //SSI0Rx    IO:PD2   Pin:21 on Tiva (MISO)
  //SSI0Tx    IO:PD3   Pin:20 on Tiva (MOSI)
  //Set these pins high on portD in GPIOPCTL to use these alternate functions
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);

//5. Set mux position in GPIOPCTL to select the SSI use of the pins
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 2 for SSI0Fss, SSI0Clk, SSI0Rx, SSI0Tx
  // Clear bits 2, 3, 4, 5 with 0xffff0000
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) & 0xffff0000) + //clear 2, 3, 4, 5
      (2 << (0 * BitsPerNibble)) +                          //SSI0Clk
      (2 << (1 * BitsPerNibble)) +                          //SSI0Fss
      (2 << (2 * BitsPerNibble)) +                          //SSI0Rx
      (2 << (3 * BitsPerNibble));                           //SSI0Tx

//6. Program the port lines for digital I/O (use GPIO_O_DEN)
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);

//7. Program the required data directions on the port lines
  // SSI0Fss - out, SSI0Clk - out, SSI0Rx - in, SSI0Tx - out
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT3HI);
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= BIT2LO; //MISO line

//8. If using SPI mode 3, program the pull-up on the clock line
  // Since we are using Mode 3, we need a pull-up on the clock line (SSI0Clk)
  HWREG(GPIO_PORTD_BASE + GPIO_O_PUR) |= BIT0HI;

//9. Wait for the SSI1 to be ready
  while ((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R1) != SYSCTL_PRSSI_R1)
  {}

//10. Make sure that the SSI 1 is disabled before programming mode bits
  //SSI Synchronous Serial Port Enable
  // Name: SSE (bit 3)
  // Value Description
  //  0 SSI operation is disabled.
  //  1 SSI operation is enabled.
  //    Note: This bit must be cleared before any control registers
  //    are reprogrammed. (pg 972)
  HWREG(SSI1_BASE + SSI_O_CR1) &= (~SSI_CR1_SSE);  //name is HI setting, ~ for LO set

//11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  //SSI Master/Slave Select
  // Name: MS (bit 2)
  //    This bit selects Master or Slave mode and can be modified only when
  //    the SSI is disabled (SSE=0).
  //  Value Description
  //    0 The SSI is configured as a master.
  //    1 The SSI is configured as a slave. (pg 972)
  HWREG(SSI1_BASE + SSI_O_CR1) &= (~SSI_CR1_MS);

  // End of Transmission
  //   Value    Description
  //   1         The TXRIS interrupt indicates that the transmit FIFO is half full
  //              or less.
  //   0         The End of Transmit interrupt mode for the TXRIS interrupt is
  //              enabled.
  HWREG(SSI1_BASE + SSI_O_CR1) |= (SSI_CR1_EOT);
  //HWREG(SSI1_BASE + SSI_O_CR1) = (HWREG(SSI1_BASE + SSI_O_CR1) & ~SSI_CR1_MS) | (SSI_CR1_EOT); //TEST

//12. Configure the SSI clock source to the system clock
  //SSI Baud Clock Source
  //The following table specifies the source that generates for the SSI baud
  //clock:
  //  Value     Description
  //  0x0       System clock (based on clock source and divisor factor)
  //  0x1-0x4   Reserved
  //  0x5       PIOSC
  //  0x6-0xF   Reserved  (pg 984)
  HWREG(SSI1_BASE + SSI_O_CC) |= (SSI_CC_CS_SYSPLL);

  // Configure clock rate (SCR), phase and polarity (SPH, SPO), mode (FRF), data size (DSS)
  // Configure clock rate (SCR) to not exceed 917 kHz; setting CPSDVSR = 4 and SCR = 24 gives us
  // BR = SysClk/(CPSDVSR * (1 + SCR)) = 400 kHz < 917 kHz: check, MAY NEED DEBUG
  HWREG(SSI1_BASE + SSI_O_CR0) = (HWREG(SSI1_BASE + SSI_O_CR0) & ~SSI_CR0_SCR_M) | (SCR_VALUE << (2 * BitsPerNibble));

  // We are operating in mode 3 (SPH = 1, SPO = 1), freescale SPI (FRF = 0), and 8-bit data size (DSS = 0x7): check
  HWREG(SSI1_BASE + SSI_O_CR0) = (HWREG(SSI1_BASE + SSI_O_CR0) & ~(SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_FRF_M
      | SSI_CR0_DSS_M)) | (SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_FRF_MOTO | SSI_CR0_DSS_8);

//13. Configure the clock pre-scaler (CPSR) and clock rate (SCR)
  // In class found max frequency 917 kHz
  // Set bitrate to 25kHz we want: 500 kHz, 200 Hz
  // bitrate = SYSCLCK / (CPSR * (1+SCR) = 40MHz / 80 = 40,000,000 kHz
  // Define CPSR = 8, SCR = 9
  /*
  HWREG(SSI1_BASE + SSI_O_CPSR) &= 0xffffff00;
  HWREG(SSI1_BASE + SSI_O_CPSR) |= SSI_PRESCALER;
  HWREG(SSI1_BASE + SSI_O_CR0) &= 0xffff00ff;
  HWREG(SSI1_BASE + SSI_O_CR0) |= SCR_VALUE;

    // We are operating in mode 3 (SPH = 1, SPO = 1), freescale SPI (FRF = 0), and 8-bit data size (DSS = 0x7): check
  HWREG(SSI1_BASE + SSI_O_CR0) = (HWREG(SSI1_BASE + SSI_O_CR0) & ~(SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_FRF_M
                                  | SSI_CR0_DSS_M)) | (SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_FRF_MOTO | SSI_CR0_DSS_8); // TEST


//14. Configure phase & polarity (SPH, SPO), mode (FRF), data size (DSS)
  // configure SPH = 1, because data is captured at the second edge
  HWREG(SSI1_BASE + SSI_O_CR0) |= SSI_CR0_SPH;
  // configure SPO = 1, because idle state is high
  HWREG(SSI1_BASE + SSI_O_CR0) |= SSI_CR0_SPO;
  // set freescale SPI Mode
  HWREG(SSI1_BASE+SSI_O_CR0) |= SSI_CR0_FRF_MOTO;
  //Set data size to 8 bits
  HWREG(SSI1_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8;

*/
//15. Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI1_BASE + SSI_O_IM) |= SSI_IM_TXIM;

//16. Make sure that the SSI is enabled for operation
  HWREG(SSI1_BASE + SSI_O_CR1) |= SSI_CR1_SSE;

//17. Enable the NVIC interrupt for the SSI when starting to transmit
  // vector number is 50 and interrupt number is 34, so EN1,bit2HI
  HWREG(NVIC_EN1) |= BIT2HI;     //when disable EN = DIS
// Lower the priority of this interrupt (below SPUD)
  HWREG(NVIC_PRI8) = (HWREG(NVIC_PRI8) & ~NVIC_PRI8_INT34_M) + (1 << NVIC_PRI8_INT34_S);

//Globally enable interrupts (done elsewhere, comment out)
  __enable_irq();

  printf( "\n Completed Initialization of Accelerometer communication\r\n");
  printf( "ACC max: %d", ACC_MAX);
}

static void configAccelerometer(void)
{
  HWREG(SSI1_BASE + SSI_O_DR) = WRITE | POWER_CTL;
  HWREG(SSI1_BASE + SSI_O_DR) = ENABLE_MEASUREMENT;

  HWREG(SSI1_BASE + SSI_O_IM) |= SSI_IM_TXIM;
  initACC = true;
}

/****************************************************************************
 * InitAccelerometerService
 * Receives uint8_t : the priorty of this service
 * Returns bool, false if error in initialization, true otherwise
 * Description
     Saves away the priority, and does any
     other required initialization for the Acceleration module
****************************************************************************/
bool InitAccelerometerService(uint8_t Priority)
{
  InitPins();
  // Initialize SPI communication between TRACTOR and Accelerometer
  // We will use PD0-3 with SS1

  configAccelerometer();

  for (int i = 0; i < MOVING_AVERAGE_WINDOW; i++)
  {
    movingAverageACC_PITCH[i] = BASELINE_PITCH;
    movingAverageACC_YAW[i] = BASELINE_YAW;
    movingAverageACC_ROLL[i] = BASELINE_ROLL;
  }

  //Initialize MyPriority variable with passed in priority number
  MyPriority = Priority;
  //Post Event ES_Init to SPUDService queue (this service)
  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
  printf("\n\rInitialized Accelerometer Service");

  // Initialize first Accelerometer query using the first timer
  ES_Timer_InitTimer(QUERY_ACC_TIMER, QUERY_TIME_LIMIT);

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

ES_Event_t RunAccelerometerService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT;

  switch (ThisEvent.EventType)
  {
    case ES_TIMEOUT:
    {
      if (ThisEvent.EventParam == QUERY_ACC_TIMER)
      {
        QueryAccelerometer(QUERY_ACC);
        // printf("q %x: ", QUERY_ACC);
      }
    }
    break;
    case EU_EOT:
    {
      //printf(" r: %x %x ",ReceivedTrash, ReceivedByte);
      updateAccelerationData();

      AcquisitionState_t state = QueryAcquireMinerSM();
      if (QueryCurrentMovement() != STOPPING)
      {
        if ((RollAcceleration < 0) && (PitchAcceleration > 0))
        {
          printf("\r\nNE");
        }
        else if ((RollAcceleration > 0) && (PitchAcceleration > 0))
        {
          printf("\r\nNW");
        }
        else if ((RollAcceleration < 0) && (PitchAcceleration < 0))
        {
          printf("\r\nSE");
        }
        else if ((RollAcceleration > 0) && (PitchAcceleration < 0))
        {
          printf("\r\nSW");
        }
//        if ((YawAcceleration > (BASELINE_YAW + COLLISION_THRESHOLD_YAW)) |
//          (YawAcceleration < (BASELINE_YAW - COLLISION_THRESHOLD_YAW)))
//        /*if((RollAcceleration > (BASELINE_ROLL + COLLISION_THRESHOLD_ROLL)) | (RollAcceleration < (BASELINE_ROLL -COLLISION_THRESHOLD_ROLL))
//          | (YawAcceleration > (BASELINE_YAW + COLLISION_THRESHOLD_YAW)) | (YawAcceleration < (BASELINE_YAW - COLLISION_THRESHOLD_YAW)))*/
//        {
//          //printf("COLLISION DETECTED\n\r");
//          //ES_Event_t CollisionEvent;
//          //CollisionEvent.EventType = EU_COLLISION;
//          //PostGameplayHSM(CollisionEvent);
//        }
//
//      }
//      if(RollAcceleration < ROLL_ORTHO_THRESHOLD_HIGH && RollAcceleration > ROLL_ORTHO_THRESHOLD_LOW)
//      {
//        if(PitchAcceleration < PITCH_ORTHO_THRESHOLD_HIGH && PitchAcceleration > PITCH_ORTHO_THRESHOLD_LOW)
//        {
//          countOrtho++;
//          if (countOrtho >= COUNTORTHOMAX) {
//            Became_Orthogonal_Recently = true;
//            printf("\n\rBECAME ORTHOGONAL");
//          }
//        }
      }
      // printf("ACC: \t r%d \t y%d \t p%d \n\r", RollAcceleration, YawAcceleration, PitchAcceleration);
      // printf("comp x0%x x1%x y0%x y1%x z0%x  z1%x\n\r", x0, x1, y0, y1, z0, z1);
      ES_Timer_InitTimer(QUERY_ACC_TIMER, QUERY_TIME_LIMIT);
    }
    break;
    default:
    {
      ;
      break;
    }
  }
  return ReturnEvent;
}

static int16_t clip(int16_t val)
{
  if (val < ACC_MIN)
  {
    return ACC_MIN;
  }
  else if (val > ACC_MAX)
  {
    return ACC_MAX;
  }
  else
  {
    return val;
  }
}

static void updateAccelerationData(void)
{
  PitchAcceleration = 0;
  RollAcceleration = 0;
  YawAcceleration = 0;
  // shift movingAverageACC_PITCH values back
  for (int i = 0; i < MOVING_AVERAGE_WINDOW - 1; i++)
  {
    movingAverageACC_PITCH[i + 1] = movingAverageACC_PITCH[i];
    movingAverageACC_ROLL[i + 1] = movingAverageACC_ROLL[i];
    movingAverageACC_YAW[i + 1] = movingAverageACC_YAW[i];
    PitchAcceleration += movingAverageACC_PITCH[i];
    RollAcceleration += movingAverageACC_ROLL[i];
    YawAcceleration += movingAverageACC_YAW[i];
  }

  // now set the pitch, yaw, and roll values
  movingAverageACC_YAW[0] = clip(z1 << 8 | z0);
  movingAverageACC_ROLL[0] = clip(x1 << 8 | x0);
  movingAverageACC_PITCH[0] = clip(y1 << 8 | y0);

  /*
  if ((pitch_uint & MASK_SIGNED_BIT) > 0) // this tells us the value is negative
  {
     movingAverageACC_PITCH[0] = pitch_uint | 0xFC00; // from Riyaz's suggestion
  }
  else
  {
     movingAverageACC_PITCH[0] = pitch_uint & ~0xFC00; // from Riyaz's suggestion
  }

   if ((roll_uint & MASK_SIGNED_BIT) > 0) // this tells us the value is negative
  {
    movingAverageACC_ROLL[0] = roll_uint | 0xFC00; // from Riyaz's suggestion
  }
  else
  {
     movingAverageACC_ROLL[0] = roll_uint & ~0xFC00; // from Riyaz's suggestion
  }

   if ((yaw_uint & MASK_SIGNED_BIT) > 0) // this tells us the value is negative
  {
    movingAverageACC_YAW[0] = yaw_uint | 0xFC00; // from Riyaz's suggestion
  }
  else
  {
   movingAverageACC_YAW[0] = yaw_uint & ~0xFC00; // from Riyaz's suggestion
  }
  */
  PitchAcceleration += movingAverageACC_PITCH[0];
  PitchAcceleration /= MOVING_AVERAGE_WINDOW;

  RollAcceleration += movingAverageACC_ROLL[0];
  RollAcceleration /= MOVING_AVERAGE_WINDOW;

  YawAcceleration += movingAverageACC_YAW[0];
  YawAcceleration /= MOVING_AVERAGE_WINDOW;
}

/****************************************************************************
 * QueryAccelerometerInit
 * Receives a command address to send to Accelerometer. Returns nothing.
****************************************************************************/
void QueryAccelerometerInit(uint8_t ByteToSend)
{
  // Store the most recently sent command
  RecentQuery = ByteToSend;

  // Write CommandAddress to SSI Data Register
  HWREG(SSI1_BASE + SSI_O_DR) = ByteToSend;  // send it to the MOSI line
  // Write CommandAddress to SSI Data Register
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE;   // send it to the MOSI line

  // Activate local interrupt to send data
  HWREG(SSI1_BASE + SSI_O_IM) |= SSI_IM_TXIM; // send it to the SS line for the accelerometer
}

/****************************************************************************
 * QueryAccelerometer
 * Receives a command address to send to Accelerometer. Returns nothing.
****************************************************************************/
void QueryAccelerometer(uint8_t ByteToSend)
{
  // Store the most recently sent command
  RecentQuery = ByteToSend;

  // Write CommandAddress to SSI Data Register
  HWREG(SSI1_BASE + SSI_O_DR) = ByteToSend;  // send it to the MOSI line

  // Write 6 trash bytes
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE; // send it to the MOSI line
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE; // send it to the MOSI line
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE; // send it to the MOSI line
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE; // send it to the MOSI line
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE; // send it to the MOSI line
  HWREG(SSI1_BASE + SSI_O_DR) = TRASH_BYTE; // send it to the MOSI line

  // Activate local interrupt to send data
  HWREG(SSI1_BASE + SSI_O_IM) |= SSI_IM_TXIM; // send it to the SS line for the accelerometer
}

//Series of Getter functions
int16_t GetRoll(void)
{
  return RollAcceleration;
}

int16_t GetPitch(void)
{
  return PitchAcceleration;
}

int16_t GetYaw(void)
{
  return YawAcceleration;
}

/****************************************************************************
 * ReadAccelerometer(aka EOTISR, interrupt output)
 * Takes no parameters. Updates the pitch, roll, and yaw acceleration.
 * Description

****************************************************************************/
void ReadAccelerometer(void)
{
  // Disable local interrupt
  HWREG(SSI1_BASE + SSI_O_IM) &= ~SSI_IM_TXIM;
  // Get rid of first trash byte
  ReceivedTrash = HWREG(SSI1_BASE + SSI_O_DR);

  if (initACC)
  {
    //Read requested content - first is x0
    x0 = HWREG(SSI1_BASE + SSI_O_DR);
    x1 = HWREG(SSI1_BASE + SSI_O_DR);
    y0 = HWREG(SSI1_BASE + SSI_O_DR);
    y1 = HWREG(SSI1_BASE + SSI_O_DR);
    z0 = HWREG(SSI1_BASE + SSI_O_DR);
    z1 = HWREG(SSI1_BASE + SSI_O_DR);
  }
  else
  {
    // do nothing
    //Read trash byte
    ReceivedByte = HWREG(SSI1_BASE + SSI_O_DR);
  }

  //  // Post interrupt event to Accelerometer service
  ES_Event_t ThisEvent;
  ThisEvent.EventType = EU_EOT;
  ThisEvent.EventParam = ReceivedByte;
  PostAccelerometerService(ThisEvent);
}

/****************************************************************************
 * InitWaitOrthogonal(aka EOTISR, interrupt output)
 * Takes no parameters. Resets the Became_Orthogonal_Recently bool

****************************************************************************/
void InitWaitOrthogonal(void)
{
  Became_Orthogonal_Recently = false;
  countOrtho = 0;
}
