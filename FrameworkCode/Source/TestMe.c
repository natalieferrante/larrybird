/****************************************************************************
 Module
   SPUDService.c

 Revision
   1.0.1

 Description
   This is a Command Generator service file for reading the commands from the
   Command Generator under the Gen2 Events and Services Framework.

 Notes

 History
 When             Who     What/Why
 --------------   ---     --------
 01/16/12 09:58   jec     began conversion from TemplateFSM.c
 02/04/2020 8:43  nat     built CommGenService
 02/07/2020       jul     updated. working for Lab 8.
 02/18/2020       jul     repurposed and updated for SPUD communication
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "SPUDService.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ssi.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"

#include "DriveService.h"
#include "GameplayHSM.h"

/*----------------------------- Module Defines ----------------------------*/

#define QUERY_TIME_LIMIT 200          // 200 ms
#define SSI_PRESCALER 0x28            // CPSR, 40
#define SCR_VALUE 0x2700              // SCR, 39
#define TEAMPINHI BIT1HI              // team setting, INPUT
#define TEAMPINLO BIT1LO

// SPUD Command Addresses
#define STATUS 0xC0
#define C1MLOC1 0xC1
#define C1MLOC2 0xC2
#define C2MLOC1 0xC3
#define C2MLOC2 0xC4
#define C1RESH 0xC5
#define C1RESL 0xC6
#define C2RESH 0xC7
#define C2RESL 0xC8
#define PUR1 0xC9
#define PUR2 0xCA
#define VOIDBYTE 0x00
#define CKH true
#define GHI false

// Duty Cycle Values
#define HALF_DUTY 55

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/

bool InitTestMe(uint8_t Priority)  //Initialize MyPriority variable with passed in priority number
{
  MyPriority = Priority;
  //Post Event ES_Init to SPUDService queue (this service)
  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
  printf("\n\rInitialized TEST ME Service");

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PostTestMe(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunTestMe(ES_Event_t ThisEvent)
{
  ES_Event_t      ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static uint8_t  counter = 0;

  if (ThisEvent.EventType == EU_BEAM_BROKEN)
  {
    printf("\n\rDoomsday.");
  }

  counter++;

  return ReturnEvent;
}
