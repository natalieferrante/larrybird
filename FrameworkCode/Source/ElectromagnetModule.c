/****************************************************************************
 Module
   ElectromagnetModule.c

 Revision
   1.0.1

 Description
   This module contains a helper function to activate the electromagnet.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/22/20 19:21 LG      Created file
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ElectromagnetModule.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define EMPINHI BIT5HI          // electromagnet, OUTPUT
#define EMPINLO BIT5LO          // electromagnet, OUTPUT

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 *  TurnMagnetOn
 *  Turns on electromagnet
****************************************************************************/
void TurnMagnetOn(void)
{
	// set pin to HIGH
  HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) |= EMPINHI;
}

/****************************************************************************
 *  TurnMagnetOff
 *  Turns off magnet
****************************************************************************/
void TurnMagnetOff(void)
{
	// set pin to LOW
  HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) &= EMPINLO;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
