/****************************************************************************
 Module
   EventCheckers.c

 Revision
   1.0.1

 Description
   This is the sample for writing event checkers along with the event
   checkers used in the basic framework test harness.

 Notes
   Note the use of static variables in sample event checker to detect
   ONLY transitions.

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 13:36 jec     initial version
****************************************************************************/

// this will pull in the symbolic definitions for events, which we will want
// to post in response to detecting events
#include "ES_Configure.h"
// This gets us the prototype for ES_PostAll
#include "ES_Framework.h"
// this will get us the structure definition for events, which we will need
// in order to post events in response to detecting events
#include "ES_Events.h"
// if you want to use distribution lists then you need those function
// definitions too.
#include "ES_PostList.h"
// This include will pull in all of the headers from the service modules
// providing the prototypes for all of the post functions
#include "ES_ServiceHeaders.h"
// this test harness for the framework references the serial routines that
// are defined in ES_Port.c
#include "ES_Port.h"
// include our own prototypes to insure consistency between header &
// actual functionsdefinition
#include "EventCheckers.h"

// This is the event checking function sample. It is not intended to be
// included in the module. It is only here as a sample to guide you in writing
// your own event checkers
#if 0
/****************************************************************************
 Function
   Check4Lock
 Parameters
   None
 Returns
   bool: true if a new event was detected
 Description
   Sample event checker grabbed from the simple lock state machine example
 Notes
   will not compile, sample only
 Author
   J. Edward Carryer, 08/06/13, 13:48
****************************************************************************/
bool Check4Lock(void)
{
  static uint8_t  LastPinState = 0;
  uint8_t         CurrentPinState;
  bool            ReturnVal = false;

  CurrentPinState = LOCK_PIN;
  // check for pin high AND different from last time
  // do the check for difference first so that you don't bother with a test
  // of a port/variable that is not going to matter, since it hasn't changed
  if ((CurrentPinState != LastPinState) &&
      (CurrentPinState == LOCK_PIN_HI)) // event detected, so post detected event
  {
    ES_Event ThisEvent;
    ThisEvent.EventType = ES_LOCK;
    ThisEvent.EventParam = 1;
    // this could be any of the service post functions, ES_PostListx or
    // ES_PostAll functions
    ES_PostAll(ThisEvent);
    ReturnVal = true;
  }
  LastPinState = CurrentPinState; // update the state for next time

  return ReturnVal;
}

#endif

/****************************************************************************
 Function
   Check4Keystroke
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 Notes
   The functions that actually check the serial hardware for characters
   and retrieve them are assumed to be in ES_Port.c
   Since we always retrieve the keystroke when we detect it, thus clearing the
   hardware flag that indicates that a new key is ready this event checker
   will only generate events on the arrival of new characters, even though we
   do not internally keep track of the last keystroke that we retrieved.
 Author
   Julea Chin
****************************************************************************/

#include "DriveService.h"
#include "GameplayHSM.h"
#include "TestMe.h"
#include "SPUDService.h"

#define LAST_4_BIT_MASK 0x0F

bool Check4Keystroke(void)
{
  if (IsNewKeyReady())   // new key waiting?
  {
    ES_Event_t ThisEvent;
    ThisEvent.EventType = ES_NEW_KEY;
    ThisEvent.EventParam = GetNewKey();
    ES_PostAll(ThisEvent);

    // Test SPUD Communication and accuracy
    if (ThisEvent.EventParam == '0')
    {
      printf("\n\r Status: %x", QueryStatus());
    }
    else if (ThisEvent.EventParam == '1')
    {
      printf("\n\r MY Beacon1: %u", (Query_MYBeacon1() & LAST_4_BIT_MASK));
    }
    else if (ThisEvent.EventParam == '2')
    {
      printf("\n\r MY Beacon2: %u", (Query_MYBeacon2() & LAST_4_BIT_MASK));
    }
    else if (ThisEvent.EventParam == '3')
    {
      printf("\n\r Enemy1: %u", (Query_ENEMYBeacon1() & LAST_4_BIT_MASK));
    }
    else if (ThisEvent.EventParam == '4')
    {
      printf("\n\r Enemy2: %u", (Query_ENEMYBeacon2() & LAST_4_BIT_MASK));
    }
    else if (ThisEvent.EventParam == '5')
    {
      printf("\n\r MY Score: %u", Query_MYScore());
    }
    else if (ThisEvent.EventParam == '6')
    {
      printf("\n\r Enemy Score: %u", Query_ENEMYScore());
    }
    else if (ThisEvent.EventParam == '7')
    {
      printf("\n\r My Permit: %u", QueryExclusivePermit());
    }
    else if (ThisEvent.EventParam == '8')
    {
      printf("\n\r Enemy Permit: %u", QueryEnemyPermit());
    }
    else if (ThisEvent.EventParam == '9')
    {
      printf( "\n\r Neutral Permit1: %u", QueryNeutralPermit1());
      printf( "\n\r Neutral Permit2: %u", QueryNeutralPermit2());
    }
    else if (ThisEvent.EventParam == 'b')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_BEAM_BROKEN;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'c')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_COLLISION;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'd')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_MINER_DETECTED;
      PostEvent.EventParam = 2;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'g')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_GAME_OVER;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'm')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_MINER_MINING;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'p')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_PERMITS_ISSUED;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == 'f')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_FORWARD;
      PostEvent.EventParam = SLOW_RPM;
      PostDriveService(PostEvent);
    }
    else if (ThisEvent.EventParam == 'r')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_REVERSE;
      PostEvent.EventParam = SLOW_RPM;
      PostDriveService(PostEvent);
    }
    else if (ThisEvent.EventParam == 'l')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_TURN_LEFT;
      PostEvent.EventParam = 90;
      PostDriveService(PostEvent);
    }
    else if (ThisEvent.EventParam == ';')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_TURN_RIGHT;
      PostEvent.EventParam = 45;
      PostDriveService(PostEvent);
    }
    else if (ThisEvent.EventParam == 't')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = ES_TIMEOUT;
      PostEvent.EventParam = COLLIDE_TIMER;
      PostDriveService(PostEvent);
    }
    else if (ThisEvent.EventParam == 's')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_STOP;
      PostDriveService(PostEvent);
    }

    /*
    else if (ThisEvent.EventParam == '1')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_INDETERMINATE;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '2')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_COL_CORRECT;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '3')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_OFF_COURSE;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '4')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_CORRECT_SQUARE;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '5')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_NEW_SQUARE;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '6')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_TURNED_LEFT_90;
      PostGameplayHSM(PostEvent);
    }
    else if (ThisEvent.EventParam == '7')
    {
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_TURNED_RIGHT_90;
      PostGameplayHSM(PostEvent);
    }*/
//    else if (ThisEvent.EventParam == '8')
//    {
//      ES_Event_t PostEvent;
//      PostEvent.EventType = EU_TURN_90;
//      PostEvent.EventParam = L;
//      PostDriveService(PostEvent);
//    }
//    else if (ThisEvent.EventParam == '9')
//    {
//      ES_Event_t PostEvent;
//      PostEvent.EventType = EU_TURN_90;
//      PostEvent.EventParam = R;
//      PostDriveService(PostEvent);
//    }
//    else if (ThisEvent.EventParam == '0')
//    {
//      ES_Event_t PostEvent;
//      PostEvent.EventType = EU_SCORING_POINTS;
//      PostGameplayHSM(PostEvent);
//    }

    return true;
  }
  return false;
}
