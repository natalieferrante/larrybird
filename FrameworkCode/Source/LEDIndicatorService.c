/****************************************************************************
 Module
   LEDIndicatorService.c

 Description
   This service responds to requests to update the indicator LEDs to indicate the robot's state

Amanda Spyropoulos
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/

#include "LEDIndicatorService.h"
#include "ES_Events.h"     /* gets ES_Events */

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "bitdefs.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
# include "GameplayHSM.h"

/* include header files for the other modules in Lab8 that are referenced */

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Variables ---------------------------*/

static uint8_t MyPriority;            // priority of service

/*---------------------------- Modular Functions ---------------------------*/
static void allOff(void);
/*---------------------------- Service Functions ---------------------------*/

/****************************************************************************
 * InitLEDIndicatorService
 * Takes a priority number, returns true
****************************************************************************/
bool InitLEDIndicatorService(uint8_t Priority)
{
  MyPriority = Priority;

  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
  printf("\n\rInitialized LED Indicator Service");

  allOff();// Initialize by setting all the LEDs off

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 * RunLEDIndicatorService
 * Receives an event from any of the other state machines
 * Changes the LED indication lights as needed
****************************************************************************/
ES_Event_t RunLEDService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  if (ThisEvent.EventType == EU_UPDATE_LEDS)
  {
    allOff();
    switch (ThisEvent.EventParam)
    {
      case 1:
      {
        HWREG(LED_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_LSB_HI);   // turn on LED - LSB
      }
      break;
      case 2:
      {
        HWREG(LED_2_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_2_LSB_HI);   // turn on LED - LSB - 2
      }
      break;
      case 3:
      {
        HWREG(LED_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_LSB_HI);     // turn on LED - LSB
        HWREG(LED_2_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_2_LSB_HI); // turn on LED - LSB - 2
      }
      break;
      case 4:
      {
        HWREG(LED_3_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_3_LSB_HI);   // turn on LED - LSB - 3
      }
      break;
      case 5:
      {
        HWREG(LED_3_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_3_LSB_HI); // turn on LED - LSB - 3
        HWREG(LED_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_LSB_HI);     // turn on LED - LSB
      }
      break;
      case 6:
      {
        HWREG(LED_3_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_3_LSB_HI);   // turn on LED - LSB - 3
        HWREG(LED_2_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_2_LSB_HI);   // turn on LED - LSB - 2
      }
      break;
      case 7:
      {
        HWREG(LED_3_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_3_LSB_HI); // turn on LED - LSB - 3
        HWREG(LED_2_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_2_LSB_HI); // turn on LED - LSB - 2
        HWREG(LED_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) |= (LED_LSB_HI);     // turn on LED - LSB
      }
      break;
      default:
      {
        allOff();
      }
      break;
    }
  }

  return ReturnEvent;
}

static void allOff(void)
{
  HWREG(LED_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) &= (LED_LSB_LO);   // turn off LED - LSB

  HWREG(LED_3_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) &= (LED_3_LSB_LO);   // turn off LED - LSB - 3
  HWREG(LED_4_LSB_PORT + (GPIO_O_DATA + ALL_BITS)) &= (LED_4_LSB_LO);   // turn off LED - LSB - 4
}

/****************************************************************************
 * PostLEDIndicatorService
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
bool PostLEDIndicatorService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/*---------------------------- Module Functions ---------------------------*/
