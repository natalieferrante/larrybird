/****************************************************************************

  Source file for ShiftPositionSM (State Machine)
  Based on the Gen 2 Events and Services Framework
  Point Person: Amanda Spyropoulos

   History
 When           Who       What/Why
 -------------- ---       --------
 02/28/20       AS			  Wrote the module
 03/04/20     JYC + EGG   Updated for integration to HSM
 ****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ShiftPositionSM.h"
#include "DriveService.h"
#include "GamePlayHSM.h"
/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines
#define ENTRY_STATE GentlyTurning
#define REVERSE_DUTY 30
#define FORWARD_DUTY 45
#define GENTLE_TURN_TIME_LIMIT 2000
#define L 1
#define R 2

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static ShiftPositionState_t CurrentState;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunTemplateSM

 Parameters
   ES_Event_t: the event to process

 Returns
   ES_Event_t: an event to return

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 2/11/05, 10:45AM
   Amanda Spyropoulos, 2/27/20
****************************************************************************/
ES_Event_t RunShiftPositionSM(ES_Event_t CurrentEvent)
{
  ES_Event_t            ReturnEvent = CurrentEvent; // assume we are not consuming event
  bool                  MakeTransition = false;     /* are we making a state transition? */
  ShiftPositionState_t  NextState = CurrentState;
  ES_Event_t            EntryEventKind = { ES_ENTRY, 0 };// default to normal entry to new state

  switch (CurrentState)
  {
    case GentlyTurning:    // If current state is MovingBackward
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)      //If an event is active
      {
        if (CurrentEvent.EventType == ES_ENTRY)
        {
          printf("Start a gentle turn\n\r");
          // Handle the entry actions.
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_TURN_LEFT;
          PostEvent.EventParam = 15;
          PostDriveService(PostEvent);                              // Set motors to do a gentle turn
          ES_Timer_InitTimer(SHIFT_TIMER, GENTLE_TURN_TIME_LIMIT);  //Initialize shift timer

          // Consume event
          ReturnEvent.EventType = ES_NO_EVENT;
        }
        else if ((CurrentEvent.EventType == ES_TIMEOUT) && (CurrentEvent.EventParam == SHIFT_TIMER))
        {
          printf("Stop gently turning\n\r");
          // Stop gently turning.
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_STOP;
          PostDriveService(PostEvent);      // Set motors to stop
          // don't reset the timer.
          // Consume event
          ReturnEvent.EventType = EU_SHIFT_HANDLED;
        }
      }
    }
    break;
  }
  //   If we are making a state transition
  if (MakeTransition == true)
  {
    //   Execute exit function for current state
    CurrentEvent.EventType = ES_EXIT;
    RunShiftPositionSM(CurrentEvent);

    CurrentState = NextState;    //Modify state variable

    //   Execute entry function for new state
    // this defaults to ES_ENTRY
    RunShiftPositionSM(EntryEventKind);
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     StartShiftPositionSM

 Parameters
     None

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 2/18/99, 10:38AM
****************************************************************************/
void StartShiftPositionSM(ES_Event_t CurrentEvent)
{
  // to implement entry to a history state or directly to a substate
  // you can modify the initialization of the CurrentState variable
  // otherwise just start in the entry state every time the state machine
  // is started
  if (ES_ENTRY_HISTORY != CurrentEvent.EventType)
  {
    CurrentState = ENTRY_STATE;
  }
  // call the entry function (if any) for the ENTRY_STATE
  RunShiftPositionSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     ShiftPositionState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 2/11/05, 10:38AM
****************************************************************************/
ShiftPositionState_t QueryShiftPositionSM(void)
{
  return CurrentState;
}
