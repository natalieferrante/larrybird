/****************************************************************************
 Module
   HandleCollision.c

 Revision
   2.0.1

 Description
   This is a lower-level state machine within AcquireMiner that handles
   collisions of the robot with other objects

 Natalie Ferrante

  History
 When           Who     What/Why
 -------------- ---     --------
 02/26/20       NF      Created file
 03/04/20   JYC + EGG   Slight updates to integrate with rest of HSM
 03/05/20       JYC     Debugging to handle limit switches
 03/04/20 17:20 EGG     Added collision detection
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "HandleCollisionSM.h"
#include "DriveService.h"
#include "AcquireMinerSM.h"
#include "GameplayHSM.h"

/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE NegatingMotion
#define ENTRY_TIME 100                //100 ms
#define STRAIGHT_TIME 1500            //1.5 s
#define WAIT_TIME 2000                //2 s
#define FORWARD_TIME 2000             //2 s
#define FRONT_SWITCHES BIT3HI         // Front switches PB2
#define BACK_SWITCHES BIT2HI          // Back switches PE3

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static HandleCollisionState_t CurrentState;
static uint8_t                EntryMotion = 0;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunHandleCollisionSM
 Description
   Run function for Collision State Machine
 Notes
   uses nested switch/case to implement the machine.
 Author
   Natalie Ferrante
****************************************************************************/
ES_Event_t RunHandleCollisionSM(ES_Event_t CurrentEvent)
{
  bool                    MakeTransition = false;/* are we making a state transition? */
  HandleCollisionState_t  NextState = CurrentState;
  ES_Event_t              EntryEventKind = { ES_ENTRY, 0 }; // default to normal entry to new state
  ES_Event_t              ReturnEvent = CurrentEvent;       // assume we are not consuming event

  switch (CurrentState)
  {
    case NegatingMotion:         // If current state is NegatingMotion
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)    //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            // Record the entry motion by querying current movement from Drive Service
            EntryMotion = QueryCurrentMovement();
            // Initialize the Overshoot timer
            ES_Timer_InitTimer(OVERSHOOT_TIMER, ENTRY_TIME);
          }
          break;

          case ES_TIMEOUT:   //If event is Timout of Collision Timer
          {
            if (CurrentEvent.EventParam == COLLIDE_TIMER)
            {
              // Set motors to stop
              ES_Event_t PostEvent;
              PostEvent.EventType = EU_STOP;
              PostDriveService(PostEvent);
              //switch states to SlightTurning
              NextState = SlightTurning;
              //change states when end this run
              MakeTransition = true;  //mark that we are taking a transition
              //consume the event
              ReturnEvent.EventType = ES_NO_EVENT;
            }
            //if the Event Param is the OVERSHOOT TIMER
            else if (CurrentEvent.EventParam == OVERSHOOT_TIMER)
            {
              //if we were moving forward, move backward
              if (EntryMotion == MOVING_FORWARD)
              {
                // Set motors to reverse
                ES_Event_t PostEvent;
                PostEvent.EventType = EU_REVERSE;
                PostEvent.EventParam = SLOW_RPM;
                PostDriveService(PostEvent);
                //Initialize the collision timer
                ES_Timer_InitTimer(COLLIDE_TIMER, STRAIGHT_TIME);
              }

              //if we were going backwards, go forwards
              if (EntryMotion == REVERSING)
              {
                // Set motors to forward
                ES_Event_t PostEvent;
                PostEvent.EventType = EU_FORWARD;
                PostEvent.EventParam = SLOW_RPM;
                PostDriveService(PostEvent);
                //Initialize the collision timer
                ES_Timer_InitTimer(COLLIDE_TIMER, STRAIGHT_TIME);
              }

              //if we were rotating left, rotate right
              if (EntryMotion == TURNING_LEFT)
              {
                // Set motors to turn right, 90 degrees
                ES_Event_t PostEvent;
                PostEvent.EventType = EU_TURN_RIGHT;
                PostEvent.EventParam = 90;
                PostDriveService(PostEvent);
                //Initialize the collision timer
                ES_Timer_InitTimer(COLLIDE_TIMER, WAIT_TIME);
              }

              //if we were rotating right, rotate left
              if (EntryMotion == TURNING_RIGHT)
              {
                // Set motors to turn left, 90 degrees
                ES_Event_t PostEvent;
                PostEvent.EventType = EU_TURN_LEFT;
                PostEvent.EventParam = 90;
                PostDriveService(PostEvent);
                //Initialize the collision timer
                ES_Timer_InitTimer(COLLIDE_TIMER, WAIT_TIME);
              }
            }
          }
          break;

          default:  // Error handler
          {}
          break;
        }
      }
    }
    break;
    case SlightTurning:   // If current state is SlightTurning
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)  //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            // If the entry motion did not involve turning
            if ((EntryMotion == MOVING_FORWARD) || (EntryMotion == REVERSING))
            {
              // Turn 45 degrees
              ES_Event_t PostEvent;
              PostEvent.EventType = EU_TURN_RIGHT;
              PostEvent.EventParam = 45;
              PostDriveService(PostEvent);
            }
            //switch states to MovingForward
            NextState = MovingForward;
            //change states when end this run
            MakeTransition = true;  //mark that we are taking a transition
            //consume the event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;

          default:  //put after all in each state
          {}
          break;
        }
      }
    }
    break;
    case MovingForward:         // If current state is MovingForward
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)    //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            // Upon entry, start moving forward 
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_FORWARD;
            PostEvent.EventParam = SLOW_RPM;
            PostDriveService(PostEvent);
            // Initialize the collision timer
            ES_Timer_InitTimer(COLLIDE_TIMER, FORWARD_TIME);
          }
          case ES_TIMEOUT:  
            // If collide timer timeout   
            if (CurrentEvent.EventParam == COLLIDE_TIMER)
            {
              //Return that the crash has been handled
              ReturnEvent.EventType = EU_CRASH_HANDLED;
            }
          }
          break;

          default:    //put after all in each state
          {}
          break;
        }
      }
    }
    break;
  }
  //   If we are making a state transition
  if (MakeTransition == true)
  {
    //   Execute exit function for current state
    CurrentEvent.EventType = ES_EXIT;
    RunHandleCollisionSM(CurrentEvent);

    CurrentState = NextState;    //Modify state variable

    //   Execute entry function for new state
    // this defaults to ES_ENTRY
    RunHandleCollisionSM(EntryEventKind);
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     StartHandleCollisionSM
 Description
     Does any required initialization for this state machine
 Author
     Natalie Ferrante
****************************************************************************/
void StartHandleCollisionSM(ES_Event_t CurrentEvent)
{
  // to implement entry to a history state or directly to a substate
  // you can modify the initialization of the CurrentState variable
  // otherwise just start in the entry state every time the state machine
  // is started
  if (ES_ENTRY_HISTORY != CurrentEvent.EventType)
  {
    CurrentState = ENTRY_STATE;
  }
  // call the entry function (if any) for the ENTRY_STATE
  RunHandleCollisionSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryHandleCollisionSM
 Returns
     TemplateState_t The current state of the Template state machine
 Description
     returns the current state of the Template state machine
 Author
     Natalie Ferrante
****************************************************************************/
HandleCollisionState_t QueryHandleCollisionSM(void)
{
  return CurrentState;
}


/****************************************************************************
 Function
     CheckSwitch

 Description
     Event checker that checks to see if beam has been broken.

 Author
     Lisa Gardner
****************************************************************************/

bool CheckSwitch(void)
{
  static uint8_t  LastFrontValue = true;
  static uint8_t  LastBackValue = true;
  bool            ReturnVal = false;

  // Check current movement. If we are currently moving, then check for collision
  if (QueryCurrentMovement() != STOPPING)  
  {
    // Record the front and back switch values
    uint8_t CurrentFrontValue = HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) & FRONT_SWITCHES;
    uint8_t CurrentBackValue = HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) & BACK_SWITCHES;

    if (((CurrentFrontValue == 0) && (LastFrontValue != 0)) || ((CurrentBackValue == 0) && (LastBackValue != 0)))
    {
      ReturnVal = true;     // Set ReturnVal = True
      // If the something has hit the front or back of the robot
      // Post a collision event to gameplay
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_COLLISION;
      PostGameplayHSM(PostEvent);
      printf("\r\n Detected Collision");
    }
    // Record the current values in memory
    LastBackValue = CurrentBackValue;
    LastFrontValue = CurrentFrontValue;
  }
  return ReturnVal; // Return ReturnVal
}
