/****************************************************************************
 Module
   TopHSMTemplate.c

 Revision
   2.0.1

 Description
   This is a template for the top level Hierarchical state machine

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/24/20 12:57 JYC     Updated with LED pins
 02/23/20 23:35 JYC     Started the Gameplay HSM
 ***********************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "GameplayHSM.h"
#include "AcquireMinerSM.h"
#include "HandleCollisionSM.h"
#include "SPUDService.h"
#include "DetectMinerModule.h"
#include "DriveService.h"

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define FLASH_TIME_LIMIT 200  // 200 ms ON, 200 ms OFF
#define PURPLE_TIME_LIMIT 400 // 400 ms ON, 400 ms OFF
#define CKH true              // Red team
#define GHI false             // Blue team
#define REDPINHI BIT0HI       // LED, output PB0
#define REDPINLO BIT0LO
#define BLUEPINHI BIT1HI            // LED, output PB1
#define BLUEPINLO BIT1LO
#define LAST_4_BIT_MASK 0x0F

/*---------------------------- Module Functions ---------------------------*/
static ES_Event_t DuringPlaying(ES_Event_t Event);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, though if the top level state machine
// is just a single state container for orthogonal regions, you could get
// away without it
static GameState_t  CurrentState;
// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t      MyPriority;
static bool         FlashFlag = false; // LEDs off
static bool         Team;
static uint8_t      teamLEDHI;
static uint8_t      teamLEDLO;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitGameplayHSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     boolean, False if error in initialization, True otherwise

 Description
     Saves away the priority,  and starts
     the top level state machine
 Notes

 Author
     Julea Chin
****************************************************************************/
bool InitGameplayHSM(uint8_t Priority)
{
  printf("\n\rInitialize Gameplay HSM");
  ES_Event_t ThisEvent;
  MyPriority = Priority;  // Save our priority
  ThisEvent.EventType = ES_ENTRY;
  StartGameplayHSM(ThisEvent); // Start the Gameplay State machine
  InitDetectionModule();
  return true;
}

/****************************************************************************
 Function
     PostGameplayHSM

 Parameters
     ES_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the post operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     Julea Chin
****************************************************************************/
bool PostGameplayHSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunGameplayHSM

 Parameters
   ES_Event: the event to process

 Returns
   ES_Event: an event to return

 Description
   the run function for the top level state machine
 Notes
   uses nested switch/case to implement the machine.
 Author
   Julea Chin
****************************************************************************/
ES_Event_t RunGameplayHSM(ES_Event_t CurrentEvent)
{
  bool        MakeTransition = false; /* are we making a state transition? */
  GameState_t NextState = CurrentState;
  ES_Event_t  EntryEventKind = { ES_ENTRY, 0 };   // default to normal entry to new state
  ES_Event_t  ReturnEvent = { ES_NO_EVENT, 0 };   // assume no error

  switch (CurrentState)
  {
    case Waiting: // If current state is state one, Waiting
    {
      // Process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)      //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:    // Handle entry
          {
            printf( "\n\rSTATE: Waiting for permits");
            printf( "\n\r   Event: Entry");
            // Get team using public helper function
            Team = QueryTeam();
            // Set team LED
            if (Team == CKH)  // CKH = RED TEAM
            {
              teamLEDHI = REDPINHI;
              teamLEDLO = REDPINLO;
            }
            else      // GHI = BLUE TEAM
            {
              teamLEDHI = BLUEPINHI;
              teamLEDLO = BLUEPINLO;
            }
            ES_Timer_InitTimer(FLASH_TIMER, FLASH_TIME_LIMIT);  // Timer to flash LEDs
          }
          break;

          case EU_PERMITS_ISSUED: // Permits issued, time to start mining for ore!
          {
            printf("\n\r   Event: Permits Issued");
            // Turn on team LEDs (solidly)
            HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (teamLEDHI);
            FlashFlag = true;         // Mark that LEDs are currently on
            NextState = Playing;      // Decide what the next state will be
            MakeTransition = true;    // Mark that we are taking a transition
          }
          break;

          case ES_TIMEOUT:  // Flash team LEDs based on timer
          {
            if (CurrentEvent.EventParam == FLASH_TIMER)
            {
              if (FlashFlag == true)
              {
                // Turn off team LED
                HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (teamLEDLO);   // turn off LED
                // Reinitialize the flash timer
                ES_Timer_InitTimer(FLASH_TIMER, FLASH_TIME_LIMIT);
                FlashFlag = false;    // Mark to turn on LED
              }
              else
              {
                // Turn on team LED
                HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (teamLEDHI);   // turn on LED
                // Reinitialize the flash timer
                ES_Timer_InitTimer(FLASH_TIMER, FLASH_TIME_LIMIT);
                FlashFlag = true;     // Mark to turn off LED
              }
            }
          }
          break;

          default:
          {}
          break;
        }
      }
    }
    break;

    case Playing: // If current state is state two, Playing
    {             // Execute During function for state one. ES_ENTRY & ES_EXIT are
                  // processed here allow the lowere level state machines to re-map
                  // or consume the event
      CurrentEvent = DuringPlaying(CurrentEvent);
      if (CurrentEvent.EventType == ES_ENTRY)
      {
        printf( "\n\rSTATE: Playing");
        printf( "\n\r Status: %x",      QueryStatus());
        printf( "\n\r MY Beacon1: %u",  (Query_MYBeacon1() & LAST_4_BIT_MASK));
        printf( "\n\r MY Beacon2: %u",  (Query_MYBeacon2() & LAST_4_BIT_MASK));
        printf( "\n\r My Permit: %u",   QueryExclusivePermit());
      }
      //Keep playing unless the game ends
      if (CurrentEvent.EventType == EU_GAME_OVER)
      {
        printf("\n\r   Event: Game Over");
        NextState = GameOver;       // Game is over
        MakeTransition = true;      // Mark that we are taking a transition
      }
    }
    break;

    case GameOver:
    {
      // As soon as game is over
      if (CurrentEvent.EventType == ES_ENTRY)
      {
        printf("\n\rSTATE: Game Over");
        // Stop motors
        ES_Event_t PostEvent;
        PostEvent.EventType = EU_STOP;
        PostDriveService(PostEvent);
        // Set up timers to flash purple LEDs
        ES_Timer_InitTimer(FLASH_TIMER, FLASH_TIME_LIMIT);
        FlashFlag = false;
      }
      // Flash purple LEDS. Turn on both red and blue.
      if ((CurrentEvent.EventType == ES_TIMEOUT) && (CurrentEvent.EventParam == FLASH_TIMER))
      {
        if (FlashFlag == true)
        {
          // Turn off red and blue LED (purple)
          HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (REDPINLO & BLUEPINLO);   // turn off LEDs
          // Reinitialize the flash timer
          ES_Timer_InitTimer(FLASH_TIMER, PURPLE_TIME_LIMIT);
          FlashFlag = false;    // Mark to turn on LEDs
        }
        else
        {
          // Turn on LEDs
          HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) |= (REDPINHI | BLUEPINHI);   // turn on LEDs
          // Reinitialize the flash timer
          ES_Timer_InitTimer(FLASH_TIMER, PURPLE_TIME_LIMIT);
          FlashFlag = true;    // Mark to turn off LEDs
        }
      }
    }
    break;
  }
  //   If we are making a state transition
  if (MakeTransition == true)
  {
    //   Execute exit function for current state
    CurrentEvent.EventType = ES_EXIT;
    RunGameplayHSM(CurrentEvent);

    CurrentState = NextState;    //Modify state variable

    // Execute entry function for new state
    // this defaults to ES_ENTRY
    RunGameplayHSM(EntryEventKind);
  }
  // in the absence of an error the top level state machine should
  // always return ES_NO_EVENT, which we initialized at the top of func
  return ReturnEvent;
}

/****************************************************************************
 Function
     StartGameplayHSM

 Parameters
     ES_Event CurrentEvent

 Returns
     nothing

 Description
     Does any required initialization for this state machine
 Notes

 Author
     Julea Chin
****************************************************************************/
void StartGameplayHSM(ES_Event_t CurrentEvent)
{
  // Set the first current state to Waiting for permits
  CurrentState = Waiting;
  // now we need to let the Run function init the lower level state machines
  // use LocalEvent to keep the compiler from complaining about unused var
  RunGameplayHSM(CurrentEvent);
  return;
}

/***************************************************************************
 private functions
 ***************************************************************************/

static ES_Event_t DuringPlaying(ES_Event_t Event)
{
  ES_Event_t ReturnEvent = Event;   // assume no re-mapping or comsumption

  // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
  if ((Event.EventType == ES_ENTRY) ||
      (Event.EventType == ES_ENTRY_HISTORY))
  {
    // Start the lower level machine that runs in this state
    StartAcquireMinerSM(Event);
  }
  else if (Event.EventType == ES_EXIT)
  {
    // On exit, give the lower levels a chance to clean up first
    RunAcquireMinerSM(Event);
    // Execute action 1: Stop motors
    ES_Event_t PostEvent;
    PostEvent.EventType = EU_STOP;
    PostDriveService(PostEvent);
  }
  else
  // do the 'during' function for this state
  {
    // run any lower level state machine
    ReturnEvent = RunAcquireMinerSM(Event);
    // do any activity that is repeated as long as we are in this state
  }
  // return either Event, if you don't want to allow the lower level machine
  // to remap the current event, or ReturnEvent if you do want to allow it.
  return ReturnEvent;
}
