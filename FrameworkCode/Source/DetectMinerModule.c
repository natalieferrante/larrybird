/****************************************************************************
 Module
   DetectMinerModule.c

 Revision
   1.0.1

 Description
   This module is responsible for reporting changes in alignment and posting events to let the drive state machine understand the current status of alignment.

 History
 When             Who   What/Why
 --------------   ---   --------
 02/23/2020 21:25 JYC   Modified eventchecker to post only during specific state
                        and passes our target miner (1 or 2) as an event param
 02/22/2020 20:46 LG    Adapted module from Lab 8 for project
 02/05/2020 13:10 LG    Updated module with additional functions
 02/05/2020 13:10 JC    Updated module to handle only 1 IR sensor
 02/22/2020 18:48 LG    Updating module for multiple frequencies
 02/26/2020 12:49 LG    Added a getter function for the target miner
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "ES_Configure.h"
#include "ES_Framework.h"

// the headers to access the GPIO subsystem
#include "inc/hw_sysctl.h"
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_pwm.h"
#include "inc/hw_memmap.h"
#include "inc/hw_Timer.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"

#include "DetectMinerModule.h"
#include "AcquireMinerSM.h"
#include "SPUDService.h"
#include "GameplayHSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define CKH true // variable the team that we are on
#define GHI false
#define LOWTHRESH_1 0   // these are thresholds for the periods of the MINERs
#define HIGHTHRESH_1 1
#define LOWTHRESH_2 2
#define HIGHTHRESH_2 3
#define MINER_ONE 1
#define MINER_TWO 2
#define TicksPerMS 40000      // 40,000 ticks per mS assumes a 40Mhz clock
#define usPerMS 1000          // Conversion between micro and milliseconds
#define ConsecutivePeriods 40 // number of consecutive periods before we will consider a MINER to be detected
#define BitsPerNibble 4

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void InitInputCapturePeriod(void);
static uint16_t GetPeriod(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint32_t PeriodInTicks;
static uint32_t LastCapture;
static uint32_t LowThresh1;
static uint32_t HighThresh1;
static uint32_t LowThresh2;
static uint32_t HighThresh2;
static uint16_t CKHThresholds[4] = { 290, 310, 490, 510 };    // Period in us
static uint16_t GHIThresholds[4] = { 690, 710, 1080, 1120 };
static uint8_t  TargetMiner = MINER_ONE; // which miner are we going for

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDetectionModule

 Description
      Initializes the pins for MINER detection
      Sets the target beacon periods based on which team we are in

 Author
     Lisa Gardner
****************************************************************************/
void InitDetectionModule(void)
{
  InitInputCapturePeriod();   // Initialize Input Capture for Beacon Detection

  if (QueryTeam() == CKH) // set the thresholds based on which team we were on
  {
    LowThresh1 = CKHThresholds[LOWTHRESH_1];
    HighThresh1 = CKHThresholds[HIGHTHRESH_1];
    LowThresh2 = CKHThresholds[LOWTHRESH_2];
    LowThresh2 = CKHThresholds[HIGHTHRESH_2];
  }
  else
  {
    LowThresh1 = GHIThresholds[LOWTHRESH_1];
    HighThresh1 = GHIThresholds[HIGHTHRESH_1];
    LowThresh2 = GHIThresholds[LOWTHRESH_2];
    LowThresh2 = GHIThresholds[HIGHTHRESH_2];
  }
}

/****************************************************************************
 Function
  CheckMinerDetection

 Description
  Event checker to see if the robot is aligned with a MINER.
  If so, which MINER?

 Author
  Lisa Gardner, Julea Chin
****************************************************************************/

bool CheckMinerDetection(void)
{
  static uint16_t LastPeriod = 9999;    // Recall last period
  uint16_t        Period = GetPeriod(); // get the current period
  static uint8_t  counter = 0;          // initialize a counter to record how many times we detect the MINER
  bool            ReturnVal = false;    // default to no event being generated

  if (QueryAcquireMinerSM() == DetectingMiner) // if we are in the detecting MINER state
  {
    /* Check if this Period and LastPeriod were both within
      the threshold to align with MINER 1 */
    if (((Period > LowThresh1) && (Period < HighThresh1) &&
        (LastPeriod > LowThresh1) && (LastPeriod < HighThresh1))) // if we detected the first MINER period
    {
      counter++;                        // increase the counter because we've seen a MINER
      if (counter > ConsecutivePeriods) // if we've gotten greater than our minimum number of consecutive periods
      {
        ReturnVal = true;   // Set ReturnVal = True, MINER was found
        // Post that we have detected and aligned with MINER 1.
        TargetMiner = MINER_ONE;  // set our target MINER accordingly
        counter = 0;              // reset period counter
        ES_Event_t PostEvent;
        PostEvent.EventType = EU_MINER_DETECTED;
        PostEvent.EventParam = TargetMiner;
        PostGameplayHSM(PostEvent); //post to gameplay the detection event
      }
    }
    /* Check if this Period and LastPeriod were both within
      the threshold to align with MINER 2 */
    else if (((Period > LowThresh2) && (Period < HighThresh2) &&
        (LastPeriod > LowThresh2) && (LastPeriod < HighThresh2))) // if we detected the second MINER period
    {
      counter++;                        // increase the counter because we've seen a MINER
      if (counter > ConsecutivePeriods) // if we've gotten greater than our minimum number of consecutive periods
      {
        ReturnVal = true;   // Set ReturnVal = True, MINER was found
        // Post that we have detected and aligned with MINER 2.
        TargetMiner = MINER_TWO; // set our target MINER accordingly
        counter = 0;    // reset period counter
        ES_Event_t PostEvent;
        PostEvent.EventType = EU_MINER_DETECTED;
        PostEvent.EventParam = TargetMiner;
        PostGameplayHSM(PostEvent); //post to gameplay the detection event
      }
    }
    else
    {
      counter = 0; // reset the counter
    }
  }
  LastPeriod = Period;  // reset the Period
  return ReturnVal;     // Return ReturnVal
}

/****************************************************************************
 Function
  GetMinerAlignment

 Description
  Check if robot is aligned with target MINER

 Author
  Julea Chin
****************************************************************************/
bool GetMinerAlignment(void)
{
  // Check if robot is still aligned with target MINER after a period of time, so that we are on track
  uint16_t Period = GetPeriod();  // get the latest period
  if (TargetMiner == MINER_ONE)   // if we are going after MINER 1
  {
    if ((Period > LowThresh1) && (Period < HighThresh1)) // if the period is within the expected period of threshold 1
    {
      return true; // we are aligned
    }
  }
  else
  {
    if ((Period > LowThresh2) && (Period < HighThresh2)) // if we are going after MINER 2 and we see a period within the expected threshold
    {
      return true; // we are aligned
    }
  }
  return false; // we are unaligned
}

/****************************************************************************
 Function
  GetTargetMiner

 Description
  Returns which miner we are going after

 Author
  Julea Chin
****************************************************************************/
uint8_t GetTargetMiner(void)
{
  return TargetMiner;
}

/****************************************************************************
 * InitInputCapturePeriod
 * Uses Timer A in Wide Timer 5 to capture the input (PD6)
 * Julea Chin
****************************************************************************/
static void InitInputCapturePeriod(void)
{
  // start by enabling the clock to the timer (Wide Timer 5)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R5;
  // enable the clock to Port D
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
  // since we added this Port D clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER5_BASE + TIMER_O_CFG) = TIMER_CFG_16_BIT;
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER5_BASE + TIMER_O_TAILR) = 0xffffffff;
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER5_BASE + TIMER_O_TAPR) = 0;
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER5_BASE + TIMER_O_TAMR) =
      (HWREG(WTIMER5_BASE + TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
      (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER5_BASE + TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port D bit 6 (WT5CCP0)
  HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) |= BIT6HI;
  // Then, map bit 6's alternate functions to WT5CCP0
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 7 for WT5CCP0
  // Clear bits 6 with 0xfffff0f
  HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTD_BASE + GPIO_O_PCTL) & 0xf0ffffff) + //clear 6
      (7 << (6 * BitsPerNibble));
  // make pin 6 on Port D into an input (to read from the IR LED)
  HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= (BIT6HI);
  HWREG(GPIO_PORTD_BASE + GPIO_O_DIR) &= BIT6LO;
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER5_BASE + TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  // enable the Timer A in Wide Timer 5 interrupt in the NVIC
  // it is interrupt number 104 so appears in EN3 at bit 8
  HWREG(NVIC_EN3) = BIT8HI;
  // make sure interrupts are enabled globally
  __enable_irq();
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  HWREG(WTIMER5_BASE + TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 * IRInputCaptureResponse
 * Calculates the period between high edges sensed by the IR Receiver
 * Returns the period in TICKS
 * Julea Chin
****************************************************************************/
void IRInputCaptureResponse(void)
{
  uint32_t ThisCapture;
  // Start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER5_BASE + TIMER_O_ICR) = TIMER_ICR_CAECINT;
  // Grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER5_BASE + TIMER_O_TAR);
  PeriodInTicks = ThisCapture - LastCapture;
  LastCapture = ThisCapture;
}

/****************************************************************************
 * GetPeriod
 * Converts the period from TICKS to MICROSECONDS
 * Julea Chin
****************************************************************************/
static uint16_t GetPeriod(void)
{
  return PeriodInTicks / (TicksPerMS / usPerMS); // calculation for period
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
