/****************************************************************************
 Module
   FindSquareSM.c

 Revision
   2.0.1

 Description
   This is a third level state machine that allows the TRACTOR to find the square to deposit the MINER in.

 Notes
 Things we could/should add later:
 - Check to see if our desired square is full before moving to it
 - Calculate trajectories to ALL available squares and decide which is closest and therefore best to pursue
 - Check as we are doing all these maneuveurs that we are still working on the same square (could implement an event checker that returns a true or false based on whether
 we need to switch our target and then go directly to refinding the column instead of waiting the time for us to check again)

 History
 When           Who     What/Why
 -------------- ---     --------
 02/26/20 11:01 EGG     Created file
 02/26/20 21:46 EGG     0 errors, time to test!
 02/26/20 12:03 EGG     All basic code written and tested, but going to do some more in-depth testing to minimize surprises later
 03/01/20 11:30 EGG     Am editing code to remove testing and add comments for clarity in preparation for board-testing
 03/03/20 04:35 JYC     Updated to match turning events
 03/03/10 23:59 EGG+JYC Debugged code. In theory, will perform well heheh

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "bitdefs.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "FindSquareSM.h"
#include "DriveService.h"
#include "SpudService.h"
#include "DetectMinerModule.h"
#include "GameplayHSM.h"
#include "AcquireMinerSM.h"

/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE MovingToColumn
#define MINER_ONE 1
#define MINER_TWO 2
#define CORR_COL 4

#define OVERSHOOT_TIME 1000     // keep moving for 1 second
#define BAD_SQUARE 16
#define LAST_4_BIT_MASK 0x0F

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/
//static ES_Event_t DuringStateOne( ES_Event Event);
static void GetTargetCoordinates(void);
static void QueryCurrentSquare(void);
static void QueryCurrentSquare(void);
static uint8_t DetermineDriveDirection(uint8_t, uint8_t);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static FindSquareState_t  CurrentState;
static uint8_t            BoardAddresses[4][4] = { { 12, 8, 4, 0 },
                                                   { 13, 9, 5, 1 },
                                                   { 14, 10, 6, 2 },
                                                   { 15, 11, 7, 3 } };  // the board mapped based on SPUD values
static uint8_t            ExclusivePermit = 0;                          // contains the square that has the exclusive permit
static uint8_t            TargetRow = 0;                                // the row that the exclusive permit is in
static uint8_t            TargetCol = 0;                                // the column that the exclusive permit is in
static uint8_t            CurrRow = 0;                                  // the current row that the miner is in
static uint8_t            CurrCol = 0;                                  // the current column that the miner is in
static uint8_t            WhichMiner = 0;                               // which miner are we holding
static uint8_t            CurrSquare = 0;                               // which square are we currently at (SPUD)
static uint8_t            DriveDir = 0;                                 // which way should we drive based on where we are and where we need to go

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunFindSquareSM

 Parameters
   ES_Event: the event to process

 Returns
   ES_Event: an event to return

 Description
   This is the run function for the FindSquare state machine that helps the robot navigate to a space where it can score points.
 Notes

 Author
 Lisa Gardner, 2/26/20, 10:50 AM
****************************************************************************/
ES_Event_t RunFindSquareSM(ES_Event_t CurrentEvent)
{
  bool              MakeTransition = false;/* are we making a state transition? */
  FindSquareState_t NextState = CurrentState;
  ES_Event_t        EntryEventKind = { ES_ENTRY, 0 }; // default to normal entry to new state
  ES_Event_t        ReturnEvent = CurrentEvent;       // assume we are not consuming event
  ES_Event_t        PostEvent;                        // Event that you need to post to Gameplay or another service

  switch (CurrentState)
  {
    case MovingToColumn:
    {
      if (CurrentEvent.EventType != ES_NO_EVENT)      //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            printf( "\n\rSTATE: Finding Square");
            printf( "\n\r  EVENT: Entry");
            ReturnEvent.EventType = ES_NO_EVENT;  // Consume Event
            QueryCurrentSquare();                 // If we've just entered this state machine, we want to know our position
            if (CurrSquare == BAD_SQUARE)         // If we are at an indeterminate location
            {
              printf("\n\rBAD SQUARe");
              // Change event to EU_INDETERMINATE. Let higher level SM handle this case to shift position
              ReturnEvent.EventType = EU_INDETERMINATE;
            }
            else
            {
              GetTargetCoordinates();                                 // query the SPUD to find out where exclusive permits exist
              QueryCurrentSquare();                                   // query the SPUD to find out where the MINER (we) are
              printf( "\n\r Curr Row: %d\n\r",      CurrRow);
              printf( "\n\r CurrCol: %d\n\r",       CurrCol);
              printf( "\n\r Tar Row %d Tar Col %d", TargetRow, TargetCol);
              DriveDir = DetermineDriveDirection(CurrCol, TargetCol); // will we need to move forward or backward to get to our square // determine if the robot needs to move forward or backward (in roll direction) to get to that location
              if (DriveDir == MOVING_FORWARD)
              {
                printf("\n\rFOR");
                PostEvent.EventType = EU_FORWARD;
                PostEvent.EventParam = FAST_RPM;
                PostDriveService(PostEvent);
              }
              else if (DriveDir == REVERSING)
              {
                printf("\n\rBACK");
                PostEvent.EventType = EU_REVERSE;
                PostEvent.EventParam = SLOW_RPM;
                PostDriveService(PostEvent);
              }
            }
          }
          break;

          case EU_COL_CORRECT:
          {
            printf("\n\rEVENT: COL_CORRECT");

            NextState = ColOvershoot;    //Decide what the next state will be
            // for internal transitions, skip changing MakeTransition
            MakeTransition = true;                //mark that we are taking a transition
            ReturnEvent.EventType = ES_NO_EVENT;  // Consume Event
          }
          break;
          default:
          {}
          break;
        }
      }
    }
    break;

    case ColOvershoot:
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)      //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            printf("\n\rSTATE: COL_OVERSHOOT");
            ES_Timer_InitTimer(OVERSHOOT_TIMER, OVERSHOOT_TIME);  // start the overshoot timer to ensure that we get inside the square enough before turning
            ReturnEvent.EventType = ES_NO_EVENT;                  // Consume Event
          }
          break;

          case ES_TIMEOUT:
          {
            printf("\n\rEVENT: ES_TIMEOUT");
            ReturnEvent.EventType = ES_NO_EVENT;  // consume event
            PostEvent.EventType = EU_STOP;        // After we have overshot, stop
            PostDriveService(PostEvent);
            QueryCurrentSquare();
            ReturnEvent.EventType = ES_NO_EVENT;    // Consume Event
            /*if (CurrSquare == BAD_SQUARE)   // if we at an indeterminate location
            {
              PostEvent.EventType = EU_INDETERMINATE; // post this event to gameplay
              PostGameplayHSM(PostEvent);
            }
            else
            {
              DriveDir = DetermineDriveDirection(CurrRow, TargetRow);
              if (DriveDir == TURNING_LEFT)
              {
                PostEvent.EventType = EU_TURN_90; // turn 90 degrees
                PostEvent.EventParam = L;         // turn left
                PostDriveService(PostEvent);
                printf("\n\rEVENT: EU_LEFT");
              }
              else
              {
                PostEvent.EventType = EU_TURN_90;   // turn 90 degrees
                PostEvent.EventParam = R;           // turn right
                PostDriveService(PostEvent);
                printf("\n\rEVENT: EU_RIGHT");
              }
            }*/
            DriveDir = DetermineDriveDirection(CurrRow, TargetRow);
            if (DriveDir == TURNING_LEFT)
            {
              PostEvent.EventType = EU_TURN_90; // turn 90 degrees
              PostEvent.EventParam = L;         // turn left
              PostDriveService(PostEvent);
              printf("\n\rEVENT: EU_LEFT");
            }
            else
            {
              PostEvent.EventType = EU_TURN_90;   // turn 90 degrees
              PostEvent.EventParam = R;           // turn right
              PostDriveService(PostEvent);
              printf("\n\rEVENT: EU_RIGHT");
            }
            NextState = Rotating90;     //Decide what the next state will be
            MakeTransition = true;      //mark that we are taking a transition
          }
          break;
          default:   // Error handler // ADD EVERYWHERE!!!!!
          {}
          break;
        }
      }
    }
    break;

    case Rotating90:
    {
      printf("\n\rSTATE: Rotating90");
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)       //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case EU_TURN_COMPLETED: // this event should be posted to Gameplay from DriveService
          {
            printf("\n\rEVENT: EU_TURN_COMPLETED");
            CurrentEvent.EventType = ES_NO_EVENT;   // consume event
            PostEvent.EventType = EU_FORWARD;
            PostEvent.EventParam = SLOW_RPM;
            PostDriveService(PostEvent);
            NextState = MovingToRow;              //Decide what the next state will be
            MakeTransition = true;                //mark that we are taking a transition
            ReturnEvent.EventType = ES_NO_EVENT;  // Consume Event
          }
          break;
          default:   // Error handler // ADD EVERYWHERE!!!!!
          {}
          break;
        }
      }
    }
    break;

    case MovingToRow:
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)       //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case EU_OFF_COURSE:
          {
            printf("\n\r EVENT:EU_OFF_COURSE");
            NextState = MovingToColumn;    //Decide what the next state will be
            // for internal transitions, skip changing MakeTransition
            PostEvent.EventType = EU_STOP;
            PostDriveService(PostEvent);
            MakeTransition = true;                //mark that we are taking a transition
            ReturnEvent.EventType = ES_NO_EVENT;  // Consume Event
          }
          break;
          default:   // Error handler // ADD EVERYWHERE!!!!!
          {}
          break;
        }
      }
    }
    break;
  } // end of state switching

  //   If we are making a state transition
  if (MakeTransition == true)
  {
    //   Execute exit function for current state
    CurrentEvent.EventType = ES_EXIT;
    RunFindSquareSM(CurrentEvent);

    CurrentState = NextState;  //Modify state variable

    //   Execute entry function for new state
    // this defaults to ES_ENTRY
    RunFindSquareSM(EntryEventKind);
  }
  return ReturnEvent;
} // end of run function

/****************************************************************************
 Function
     StartFindingSquareSM

 Parameters
     CurrentEvent

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     Lisa Gardner, 2/26/20, 11:38AM
****************************************************************************/
void StartFindSquareSM(ES_Event_t CurrentEvent)
{
  if (ES_ENTRY_HISTORY != CurrentEvent.EventType)
  {
    CurrentState = ENTRY_STATE;
  }
  RunFindSquareSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryFindSquareSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 2/11/05, 10:38AM
****************************************************************************/
FindSquareState_t QueryFindSquareSM(void)
{
  return CurrentState;
}

/* **************************************************************************
CheckSquare
Checks to see if we have entered a new square
********************************************************************** */

bool CheckSquare(void)
{
  bool ReturnVal = false;
  if ((QueryFindSquareSM() == MovingToColumn) || (QueryFindSquareSM() == MovingToRow))
  {
    static uint8_t  LastSquare = BAD_SQUARE; // Recall last square

    ES_Event_t      PostEvent;
    QueryCurrentSquare();
    GetTargetCoordinates();
    if (((CurrSquare != LastSquare) && (CurrSquare != BAD_SQUARE)))
    {
      if (CurrCol == TargetCol)
      {
        PostEvent.EventType = EU_COL_CORRECT;
        PostGameplayHSM(PostEvent);
      }
      if ((QueryFindSquareSM() == MovingToRow) && (CurrCol != TargetCol))
      {
        PostEvent.EventType = EU_OFF_COURSE;
        PostGameplayHSM(PostEvent);
      }
      ReturnVal = true;   // Set ReturnVal = True, MINER was found
      // Post that we have detected and aligned with MINER 2.
    }
    /*else if (CurrSquare == BAD_SQUARE)
    {
      PostEvent.EventType = EU_INDETERMINATE;
      PostGameplayHSM(PostEvent);
    }*/

    LastSquare = CurrSquare;
  }

  return ReturnVal; // Return ReturnVal
}

/***************************************************************************
 private functions
 ***************************************************************************/

/**************************************************************************
Function
    GetTargetCoordinates
This function gets our current coordinates (i.e. row and col on the board)
*///***********************************************************************
static void GetTargetCoordinates(void)
{
  ExclusivePermit = QueryExclusivePermit(); // get the location we are trying to go to
  for (uint8_t i = 0; i < 4; ++i)           // get the row and column we are trying to go to
  {
    for (uint8_t j = 0; j < 4; ++j)
    {
      if (BoardAddresses[i][j] == ExclusivePermit)
      {
        TargetRow = i;
        TargetCol = j;
      }
    }
  }
}

/**************************************************************************
Function
    GetCurrentSquare
This function queries the SPUD to figure out where the MINER that is
currently in our possession is (i.e. where we are) in terms of board square
*///***********************************************************************

static void QueryCurrentSquare()
{
  WhichMiner = GetTargetMiner(); // get the miner that we currently possess
  uint8_t EntireQuery = 0;
  if (WhichMiner == MINER_ONE)
  {
    EntireQuery = Query_MYBeacon1();
  }
  else
  {
    EntireQuery = Query_MYBeacon2();
  }
  uint8_t IsLegitLocation = EntireQuery & BIT7HI;
  if (IsLegitLocation)
  {
    CurrSquare = EntireQuery & LAST_4_BIT_MASK;
    for (uint8_t i = 0; i < 4; ++i)       // get our current row and column
    {
      for (uint8_t j = 0; j < 4; ++j)
      {
        if (BoardAddresses[i][j] == CurrSquare)
        {
          CurrRow = i;
          CurrCol = j;
        }
      }
    }
  }
  else
  {
    CurrSquare = BAD_SQUARE;
  }
}

/**************************************************************************
Function
    DetermineDriveDirection
This function determines which way we need to drive or turn our motors
to reach the desired location of the miner
*///***********************************************************************

static uint8_t DetermineDriveDirection(uint8_t Curr, uint8_t Desired)
{
  static bool MOVE_FORWARD = true;
  if ((QueryFindSquareSM() == MovingToColumn))
  {
    //printf("\n\rCurr: %d Des: %d", Curr, Desired);
    if (Curr < Desired)
    {
      // drive forward
      return MOVING_FORWARD;
    }
    else if (Curr > Desired)
    {
      // drive backwards
      MOVE_FORWARD = false;
      return REVERSING;
    }
    else
    {
      return STOPPING;
    }
  }
  else
  {
    // turn left
    if (Curr < Desired)
    {
      return TURNING_RIGHT;
    }
    // turn right
    else
    {
      return TURNING_LEFT;
    }
  }
}
