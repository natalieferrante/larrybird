/****************************************************************************
 Module
   ZigzagToSquare.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 03/04/20 2:23  JYC     ITS CRUNCH TIME
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ZigzagToSquare.h"
#include "DetectMinerModule.h"
#include "SpudService.h"
#include "DriveService.h"
#include "GameplayHSM.h"

/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE   FindingCurrent
#define MINER_ONE     1
#define MINER_TWO     2
#define LAST_4_BIT_MASK     0x0F
#define BAD_SQUARE          16
#define ADJUST_TIME_LIMIT   200   //200 ms
#define WAIT_TIME_LIMIT     2000  //Wait 2 seconds
#define Ndeg 0
#define NEdeg 45
#define Edeg 90
#define SEdeg 135
#define Sdeg 180
#define SWdeg -135
#define Wdeg -90
#define NWdeg -45
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/
static void GetPermitCoordinates(void);
static void QueryCurrentSquare(void);
static void FindDirectionFacing(void);
static void FindPermitDirection(void);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static ZigzagState_t CurrentState;
static uint8_t  TargetPermit = 16;       // contains the square that has the exclusive permit
static uint8_t  PermitRow = 16;          // the row that the target permit is in
static uint8_t  PermitCol = 16;          // the column that the target permit is in
static uint8_t  CurrRow = 16;            // the current row that the miner is in
static uint8_t  CurrCol = 16;            // the current column that the miner is in
static uint8_t  PrevRow = 16;            // the previous row that the miner is in
static uint8_t  PrevCol = 16;            // the previous column that the miner is in
static uint8_t  WhichMiner = 0;           // which miner are we holding
static uint8_t  CurrSquare = 16;         // which square are we currently at (SPUD)
static uint8_t  DriveDir = 16;           // which way should we drive based on where we are and where we need to go
static int16_t  MyOrientation = 0;
static int16_t  PermitLocationDeg = 0;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunZigzagSM

 Parameters
   ES_Event_t: the event to process

 Returns
   ES_Event_t: an event to return

 Description
   Dictates movement to bring the MINER to the permit location
 Notes
   uses nested switch/case to implement the machine.
 Author
   Julea Chin
****************************************************************************/
ES_Event_t RunZigzagSM(ES_Event_t CurrentEvent)
{
  bool            MakeTransition = false;/* are we making a state transition? */
  ZigzagState_t   NextState = CurrentState;
  ES_Event_t      EntryEventKind = { ES_ENTRY, 0 }; // default to normal entry to new state
  ES_Event_t      ReturnEvent = CurrentEvent;       // assume we are not consuming event

  switch (CurrentState)
  {
    // Entry case: Finding current
    case FindingCurrent:
    {
      //process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)      //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            // Upon entry, figure out which miner we have
            WhichMiner = GetTargetMiner(); 
            QueryCurrentSquare();   //calculate current location
            GetPermitCoordinates(); //get permit location
            printf("\n\r Aim for Miner %u (%u,%u). We are in (%u,%u)", 
              WhichMiner, PermitRow, PermitCol, CurrRow, CurrCol );
            if (CurrSquare == BAD_SQUARE) // If we are at an indeterminate location
            {
              //Initialize the Zigzag timer 
              //Set motors to reverse
              ES_Timer_InitTimer(ZIGZAG_TIMER, ADJUST_TIME_LIMIT);
              ES_Event_t PostEvent;
              PostEvent.EventType = EU_REVERSE;
              PostEvent.EventParam = CRAWL_RPM;
              PostDriveService(PostEvent);
            } 
            // Otherwise, if good location found
            else {
              // Move to Finding previous state
              NextState = FindingPrevious;
              MakeTransition = true; // mark that we are taking a transition
            }  
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;            
          }
          break;
          
          case ES_TIMEOUT:
          { // If Zigzag Timeout, check if we are at a legit location
            if (CurrentEvent.EventParam == ZIGZAG_TIMER) 
            {
              QueryCurrentSquare();
              // If we are at an indeterminate location
              if (CurrSquare == BAD_SQUARE) 
              {
                // Initialize ZIgzag timer
                ES_Timer_InitTimer(ZIGZAG_TIMER, ADJUST_TIME_LIMIT);
                // Continue reversing
                ES_Event_t PostEvent;
                PostEvent.EventType = EU_REVERSE;
                PostEvent.EventParam = CRAWL_RPM;
                PostDriveService(PostEvent);
              }
              // Good location found
              else {
                // Move to Finding Previous
                NextState = FindingPrevious;
                MakeTransition = true; // mark that we are taking a transition
              }   
            }
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;
          
          default:
          { }
          break;
        }
      }
    }
    break;
    
    case FindingPrevious: 
    {
      if (CurrentEvent.EventType != ES_NO_EVENT)      //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:      // Upon entry, 
          {
            // Begin reversing 
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_REVERSE;
            PostEvent.EventParam = SLOW_RPM;
            PostDriveService(PostEvent);
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;
          
          case EU_NEW_SQUARE: // Found a new square!
          {
            FindDirectionFacing();      // Check which direction facing
            FindPermitDirection();      // Decide which permit to go to
            NextState = Turning;        // Move to CalculatingTrajectory
            MakeTransition = true;      // Mark that we are taking a transition
            ES_Event_t PostEvent;       // Stop the motors
            PostEvent.EventType = EU_STOP;
            PostDriveService(PostEvent);
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;
          
          default:
          { }
          break;
        }
      }
    }
    break;    

    case Turning:
    {
      if (CurrentEvent.EventType == ES_ENTRY){
        // Upon entry, begin moving forward
        ES_Timer_InitTimer(ZIGZAG_TIMER, WAIT_TIME_LIMIT);
        //Consume event
        ReturnEvent.EventType = ES_NO_EVENT;
      }
      if ((CurrentEvent.EventType == ES_TIMEOUT) && (CurrentEvent.EventParam == ZIGZAG_TIMER))
      {
        // Post to drive service to begin turning at a specific angle 
        int32_t TurnAngle = MyOrientation - PermitLocationDeg ;
        // If turn angle is over or under 180 degrees, adjust the angle by 360 degrees
        if ( TurnAngle > 180 ){
          TurnAngle -= 360;
        }else if (TurnAngle < -180){
          TurnAngle += 360;
        }
        if (TurnAngle == 0){
          NextState = GoingForward;  //Move to CalculatingTrajectory
          MakeTransition = true;      //Mark that we are taking a transition
        }
        else if (TurnAngle > 0) {
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_TURN_RIGHT;
          PostEvent.EventParam = TurnAngle;
          PostDriveService(PostEvent);
        } else {
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_TURN_LEFT;
          PostEvent.EventParam = (TurnAngle * -1);
          PostDriveService(PostEvent);
        }
        // Consume event
        ReturnEvent.EventType = ES_NO_EVENT;
      }
      if (CurrentEvent.EventType == EU_TURN_COMPLETED)
      {
        ES_Event_t PostEvent;       // Stop the motors
        PostEvent.EventType = EU_STOP;
        PostDriveService(PostEvent);
        NextState = GoingForward;   //Move to CalculatingTrajectory
        MakeTransition = true;      //Mark that we are taking a transition
        // Consume event
        ReturnEvent.EventType = ES_NO_EVENT;
      } 
    }
    break;
  
    case GoingForward: 
    {
      //process any events
      if (CurrentEvent.EventType == ES_ENTRY){
        // Upon entry, begin moving forward
        // Initialize the Zigzag timer
        ES_Timer_InitTimer(ZIGZAG_TIMER, WAIT_TIME_LIMIT);
        //Consume event
        ReturnEvent.EventType = ES_NO_EVENT;
      } 
      else if ((CurrentEvent.EventType == ES_TIMEOUT) && (CurrentEvent.EventParam == ZIGZAG_TIMER))
      {
        // Upon entry, begin moving forward
        ES_Event_t PostEvent;
        PostEvent.EventType = EU_FORWARD;
        PostEvent.EventParam = SLOW_RPM;
        PostDriveService(PostEvent);
        // Consume event
        ReturnEvent.EventType = ES_NO_EVENT;
      } 
      else if (CurrentEvent.EventType == EU_NEW_SQUARE) // Found a new square!
      {           
        FindDirectionFacing();
        FindPermitDirection();
        // Square found, stop moving
        ES_Event_t PostEvent;
        PostEvent.EventType = EU_STOP;
        PostDriveService(PostEvent);
        NextState = Turning;      // Move to Turning
        MakeTransition = true;    // Mark that we are taking a transition
        // Consume event
        ReturnEvent.EventType = ES_NO_EVENT;
      }
      printf("\n\r\tCurrent (%u,%u) \t Previous (%u,%u,)", 
        CurrRow, CurrCol, PrevRow, PrevCol);
    }
    break;
      // repeat state pattern as required for other states
  }
  //   If we are making a state transition
  if (MakeTransition == true)
  {
    //   Execute exit function for current state
    CurrentEvent.EventType = ES_EXIT;
    RunZigzagSM(CurrentEvent);

    CurrentState = NextState;    //Modify state variable

    //   Execute entry function for new state
    // this defaults to ES_ENTRY
    RunZigzagSM(EntryEventKind);
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     StartTemplateSM

 Parameters
     None

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     Julea Chin
****************************************************************************/
void StartZigzagSM(ES_Event_t CurrentEvent)
{
  // to implement entry to a history state or directly to a substate
  // you can modify the initialization of the CurrentState variable
  // otherwise just start in the entry state every time the state machine
  // is started
  if (ES_ENTRY_HISTORY != CurrentEvent.EventType)
  {
    printf("\n\rEntering ZigZag");
    CurrentState = ENTRY_STATE;
  }
  // call the entry function (if any) for the ENTRY_STATE
  RunZigzagSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryZigzagSM

 Parameters
     None

 Returns
     ZigzagState_t The current state of the Zigzag state machine

 Description
     returns the current state of the Zigzag state machine
 Notes

 Author
     Julea Chin
****************************************************************************/
ZigzagState_t QueryZigzagSM(void)
{
  return CurrentState;
}

/* **************************************************************************
CheckNewSquare
  Checks to see if we have entered a new square
  Julea Chin
********************************************************************** */

bool CheckNewSquare(void)
{
  static uint8_t  PrevSquare = BAD_SQUARE; 
  bool ReturnVal = false;
  // Only use event checker if in Finding Previous or Going Forward
  if ((QueryZigzagSM() == FindingPrevious) || (QueryZigzagSM() == GoingForward))
  {
    // Set previous column and row as the current column and row
    ES_Event_t      PostEvent;
    PrevCol = CurrCol;
    PrevRow = CurrRow;
    // Update the current square location
    QueryCurrentSquare();
    // Get the permit coordinates
    GetPermitCoordinates();
    if (((CurrSquare != PrevSquare) && (CurrSquare != BAD_SQUARE) && (PrevSquare != BAD_SQUARE)))
    {
      // If new square is found, post the event to gameplay
      ES_Event_t PostEvent;
      PostEvent.EventType = EU_NEW_SQUARE;
      PostGameplayHSM(PostEvent);
    }
    PrevSquare = CurrSquare;  // Set previous location to current location
  }
  return ReturnVal; // Return ReturnVal
}

/***************************************************************************
 private functions
 ***************************************************************************/

/**************************************************************************
Function
    QueryCurrentSquare
    This function queries the SPUD to figure out where the MINER that is 
    currently in our possession is (i.e. where we are) in terms of board square
    Lisa Gardner
*///***********************************************************************
static void QueryCurrentSquare()
{
  uint8_t     EntireQuery = 0;
  // Query the location of the beacon in possession
  if (WhichMiner == MINER_ONE)
  {
    EntireQuery = Query_MYBeacon1();
  }
  else
  {
    EntireQuery = Query_MYBeacon2();
  }
  uint8_t IsLegitLocation = EntireQuery & BIT7HI;
  // Check if location is trustworthy
  if (IsLegitLocation)
  {
    CurrSquare = EntireQuery & LAST_4_BIT_MASK;
    CurrRow = CurrSquare / 4;
    CurrCol = CurrSquare % 4; 
  }
  else
  {
    CurrSquare = BAD_SQUARE;
  }
}

/**************************************************************************
Function
  GetPermitCoordinates
  This function gets our target permit coordinates (i.e. row and col on the board)
  Amanda Spyropoulos
*///***********************************************************************
static void GetPermitCoordinates(void)
{
  TargetPermit = QueryExclusivePermit(); // get the location we are trying to go to
  PermitRow= TargetPermit / 4;
  PermitCol = TargetPermit % 4; 
}

/**************************************************************************
Function
  FindDirectionFacing
  Find which direction you (the bot) are facing
  Natalie Ferrante
*///***********************************************************************
static void FindDirectionFacing(void){

  if(CurrRow > PrevRow){ //check facing NORTH
    if(CurrCol < PrevCol){ //check facing EAST
      MyOrientation = NEdeg;
    }else if (CurrCol > PrevCol) { //check facing WEST
      MyOrientation = NWdeg;
    }else { // if not WEST or EAST but NORTH
      MyOrientation  = Ndeg;
    }	
  } else if (CurrRow < PrevRow){ //check facing SOUTH
    if(CurrCol < PrevCol){ //check facing EAST
      MyOrientation = SEdeg;
    }else if (CurrCol > PrevCol){ //check facing WEST
      MyOrientation = SWdeg;
    }else { // if not WEST or EAST but NORTH
      MyOrientation = Sdeg;
    }
  } else if (CurrCol > PrevCol){ //check facing WEST
    MyOrientation = Wdeg;
  } else if(CurrCol < PrevCol){ //check facing EAST
    MyOrientation = Edeg;
  }
}


/**************************************************************************
Function
  FindPermitDirectoin
  Call every time we get into a new square to update our trajectory
  Natalie Ferrante
*///***********************************************************************
static void FindPermitDirection(void){
  GetPermitCoordinates(); // Update permit location
  if(CurrRow > PermitRow){ //check facing NORTH
    if(CurrCol < PermitCol){ //check facing EAST
      PermitLocationDeg = NEdeg;
    } else if (CurrCol > PermitCol) { //check facing WEST
      PermitLocationDeg = NWdeg;
    } else { // if not WEST or EAST but NORTH
      PermitLocationDeg = Ndeg;
    }	
  }else if (CurrRow < PermitRow){ //check facing SOUTH
      if(CurrCol < PermitCol){ //check facing EAST
        PermitLocationDeg = SEdeg;
      }else if (CurrCol > PermitCol) { //check facing WEST
        PermitLocationDeg = SWdeg;
      }else { // if not WEST or EAST but NORTH
        PermitLocationDeg = Sdeg;
      }
  } else if (CurrCol > PermitCol){ //check facing WEST
      PermitLocationDeg = Wdeg;
  } else if(CurrCol < PermitCol){ //check facing EAST
      PermitLocationDeg = Edeg;
  }
}

