/****************************************************************************
 Module
   SPUDService.c

 Revision
   1.0.1

 Description
   This is a Command Generator service file for reading the commands from the
   Command Generator under the Gen2 Events and Services Framework.

 Notes

 History
 When             Who     What/Why
 --------------   ---     --------
 01/16/12 09:58   jec     began conversion from TemplateFSM.c
 02/04/2020 8:43  nat     built CommGenService
 02/07/2020       jul     updated. working for Lab 8.
 02/18/2020       jul     repurposed and updated for SPUD communication
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "SPUDService.h"

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "inc/hw_ssi.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"

#include "DriveService.h"
#include "GameplayHSM.h"

/*----------------------------- Module Defines ----------------------------*/

#define QUERY_TIME_LIMIT 200          // 200 ms
#define SSI_PRESCALER 0x28            // CPSR, 40
#define SCR_VALUE 0x2700              // SCR, 39
#define TEAMPINHI BIT1HI              // team setting, INPUT
#define TEAMPINLO BIT1LO

// SPUD Command Addresses
#define STATUS 0xC0
#define C1MLOC1 0xC1
#define C1MLOC2 0xC2
#define C2MLOC1 0xC3
#define C2MLOC2 0xC4
#define C1RESH 0xC5
#define C1RESL 0xC6
#define C2RESH 0xC7
#define C2RESL 0xC8
#define PUR1 0xC9
#define PUR2 0xCA
#define VOIDBYTE 0x00
#define CKH true
#define GHI false

#define LAST_4_BIT_MASK 0x0F

// Duty Cycle Values
#define HALF_DUTY 55

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void InitSPUD(void);
static void InitTeam(void);

/*---------------------------- Module Variables ---------------------------*/
static uint8_t  MyPriority;
static uint32_t BitsPerNibble = 4;

// SPI communication data
static uint8_t  RecentQuery;
static uint8_t  NextQuery = STATUS;
static uint8_t  ReceivedTrash1;
static uint8_t  ReceivedTrash2;
static uint8_t  ReceivedByte;
static uint8_t  SPUDContent;

// SPUD information
static uint8_t  PermitStatus = 0;
static uint8_t  CKHBeacon1 = 0;
static uint8_t  CKHBeacon2 = 0;
static uint8_t  GHIBeacon1 = 0;
static uint8_t  GHIBeacon2 = 0;
static uint16_t CKHScore = 0;
static uint16_t GHIScore = 0;
static uint8_t  ExclusivePermit = 0;
static uint8_t  NeutralPermit = 0;
static bool     Team; //true - CKH, false - GHI

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 * InitSPUDService
 * Receives uint8_t : the priorty of this service
 * Returns bool, false if error in initialization, true otherwise
 * Description
     Saves away the priority, and does any
     other required initialization for this service
****************************************************************************/
bool InitSPUDService(uint8_t Priority)
{
  // Initialize SPI communication between TRACTOR and SPUD
  InitSPUD();
  // Initialize first SPUD query using the first timer
  ES_Timer_InitTimer(QUERY_TIMER, QUERY_TIME_LIMIT);
  // Assign team to CKH or GHI based on input pin
  InitTeam();

  //Initialize MyPriority variable with passed in priority number
  MyPriority = Priority;
  //Post Event ES_Init to SPUDService queue (this service)
  ES_Event_t ThisEvent;
  ThisEvent.EventType = ES_INIT;
  printf("\n\rInitialized SPUD Service");

  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

//End of InitSPUDService

/****************************************************************************
 * PostSPUDService
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
bool PostSPUDService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

//End of PostSPUDService

/****************************************************************************
 * RunSPUDService
 * Handles the querying and reading for all SPUD communication
 * Updates module level variables that can be accessed by public functions
* Current Loop: Status - Beacon1 - Beacon2 - ExcPermit
****************************************************************************/
ES_Event_t RunSPUDService(ES_Event_t ThisEvent)
{
  static uint16_t tempScore = 0;
  ES_Event_t      ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  // If query timer timeout
  if ((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == QUERY_TIMER))
  {
    QuerySPUD(NextQuery); // Query the SPUD
  }

  // If interrupt
  if (ThisEvent.EventType == EU_EOT)
  {
    // Initialize the timer and process the content from the SPUD
    ES_Timer_InitTimer(QUERY_TIMER, QUERY_TIME_LIMIT);
    SPUDContent = ThisEvent.EventParam;

    switch (RecentQuery)
    {
      case STATUS:  // Permit Status  =====================================
      {
        // CASE 0: Waiting for Permits
        // Keep checking for permits until permits are acquired
        // Don't query other things if Waiting for Permits or Game Over

        // CASE 1: Permits Issued
        if (SPUDContent == 0x01)
        {
          // Check if previous permit status was WAITING and
          // new content is PERMITS ISSUED
          if (PermitStatus == 0x00)
          {
            // Post Waiting for Permit became Issues Acquired
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_PERMITS_ISSUED;
            PostGameplayHSM(PostEvent);
          }
          // Enter query loop of various beacon commands
          NextQuery = C1MLOC1;
        }
        // Case 3: Game Over
        else if (SPUDContent == 0x03)
        {
          // Post GAME OVER
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_GAME_OVER;
          PostGameplayHSM(PostEvent);
        }
        // Regardless of case
        PermitStatus = SPUDContent; // Update Permit status
        NextQuery = C1MLOC1;        // Loop to next query
        break;
      }

      case C1MLOC1: // CKH Beacon 1 =====================================
      {
        CKHBeacon1 = SPUDContent;   // Update Beacon 1 location
        NextQuery = C1MLOC2;        // Go to CKH Beacon 2
        break;
      }

      case C1MLOC2: // CKH Beacon 2 =====================================
      {
        CKHBeacon2 = SPUDContent;   // Update Beacon 2 location
        NextQuery = C1RESH;         // Go to CKH resource level HIGH
        break;
      }

      case C1RESH: // CKH Resource level (HIGH) =========================
      {
        tempScore = SPUDContent;  // Store HIGH score temporarily
        NextQuery = C1RESL;       // Go to CKH resource level LOW
        break;
      }

      case C1RESL: // CKH Resource level (LOW)  =========================
      {
        CKHScore |= ((tempScore << 8) | SPUDContent); // Update CKH Score
        if ((Team == CKH) && (SPUDContent > 0))
        {
          // If nonzero score, then miner is milling, woohoo!
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_MINER_MINING;
          PostGameplayHSM(PostEvent);
        }
        NextQuery = C2MLOC1;     // Go to Exc Permit
        break;
      }

      case C2MLOC1: // GHI Beacon 1 =====================================
      {
        GHIBeacon1 = SPUDContent;   // Update Beacon 1 location
        NextQuery = C2MLOC2;        // Go to GHI Beacon 2
        break;
      }

      case C2MLOC2: // GHI Beacon 2 =====================================
      {
        GHIBeacon2 = SPUDContent;   // Update Beacon 2 location
        NextQuery = C2RESH;         // Go to GHI resource level HIGH
        break;
      }

      case C2RESH: // GHI Resource level (HIGH) =========================
      {
        tempScore = SPUDContent;  // Store HIGH score temporarily
        NextQuery = C2RESL;       // Go to GHI resource level LOW
        break;
      }

      case C2RESL: // GHI Resource level (LOW)  =========================
      {
        GHIScore |= ((tempScore << 8) | SPUDContent);  // Update GHI Score
        if ((Team == GHI) && (SPUDContent > 0))
        {
          // If nonzero score, then miner is milling, woohoo!
          ES_Event_t PostEvent;
          PostEvent.EventType = EU_MINER_MINING;
          PostGameplayHSM(PostEvent);
        }
        NextQuery = PUR1;     // Go to Exc Permit
        break;
      }

      case PUR1:  // Exclusive Permit Location  =========================
      {
        ExclusivePermit = SPUDContent;  // Update Exc permit location
        NextQuery = PUR2;               // Check permit status
        break;
      }
      case PUR2:  // Neutral Permit Location  =========================
      {
        NeutralPermit = SPUDContent;    // Update Neutral permit location
        NextQuery = STATUS;             // Check permit status
        break;
      }

      default:  // Error handler
      {}
      break;
    }
  }
  return ReturnEvent;
}

/****************************************************************************
 * InitSPUD
 * Receives nothing, returns nothing.
 * Description
     Saves away the priority, and does any
     other required initialization for this service
* Author: Natalie Ferrante & Julea Chin
****************************************************************************/
static void InitSPUD(void)
{
//1. Enable the clock to the GPIO port A
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;

//2. Enable the clock to SSI module 0 (PA2, PA3, PA4, PA5)
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;

//3. Wait for the GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R0) != SYSCTL_PRGPIO_R0)
  {}

//4. Program the GPIO to use the alternate functions on the SSI pins
  /*  "The digital alternate hardware functions are enabled by setting the
      appropriate bit in the GPIO Alternate Function Select (GPIOAFSEL) (Step 4)
      and GPIODEN registers (Step 7.) and configuring the PMCx bit field in the GPIO
      Port Control (GPIOPCTL) (Step 5.) register to the numeric encoding shown in
      the table below" pg 650-651 (also see pg 953-4) */
  //SSI0Clk   IO:PA2   Pin:19 on Tiva
  //SSI0Fss   IO:PA3   Pin:20 on Tiva
  //SSI0Rx    IO:PA4   Pin:21 on Tiva (MISO)
  //SSI0Tx    IO:PA5   Pin:22 on Tiva (MOSI)
  //Set these pins high on portA in GPIOPCTL to use these alternate functions
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

//5. Set mux position in GPIOPCTL to select the SSI use of the pins
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 2 for SSI0Fss, SSI0Clk, SSI0Rx, SSI0Tx
  // Clear bits 2, 3, 4, 5 with 0xff0000ff
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) + //clear 2, 3, 4, 5
      (2 << (2 * BitsPerNibble)) +                          //SSI0Clk
      (2 << (3 * BitsPerNibble)) +                          //SSI0Fss
      (2 << (4 * BitsPerNibble)) +                          //SSI0Rx
      (2 << (5 * BitsPerNibble));                           //SSI0Tx

//6. Program the port lines for digital I/O (use GPIO_O_DEN)
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

//7. Program the required data directions on the port lines
  // SSI0Fss - out, SSI0Clk - out, SSI0Rx - in, SSI0Tx - out
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= BIT4LO; //MISO line

//8. If using SPI mode 3, program the pull-up on the clock line
  // Since we are using Mode 3, we need a pull-up on the clock line (SSI0Clk)
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= BIT2HI;

//9. Wait for the SSI0 to be ready
  while ((HWREG(SYSCTL_PRSSI) & SYSCTL_PRSSI_R0) != SYSCTL_PRSSI_R0)
  {}

//10. Make sure that the SSI 0 is disabled before programming mode bits
  //SSI Synchronous Serial Port Enable
  // Name: SSE (bit 3)
  // Value Description
  //  0 SSI operation is disabled.
  //  1 SSI operation is enabled.
  //    Note: This bit must be cleared before any control registers
  //    are reprogrammed. (pg 972)
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_SSE);  //name is HI setting, ~ for LO set

//11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  //SSI Master/Slave Select
  // Name: MS (bit 2)
  //    This bit selects Master or Slave mode and can be modified only when
  //    the SSI is disabled (SSE=0).
  //  Value Description
  //    0 The SSI is configured as a master.
  //    1 The SSI is configured as a slave. (pg 972)
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_MS);

  // End of Transmission
  //   Value    Description
  //   1         The TXRIS interrupt indicates that the transmit FIFO is half full
  //              or less.
  //   0         The End of Transmit interrupt mode for the TXRIS interrupt is
  //              enabled.
  HWREG(SSI0_BASE + SSI_O_CR1) |= (SSI_CR1_EOT);

//12. Configure the SSI clock source to the system clock
  //SSI Baud Clock Source
  //The following table specifies the source that generates for the SSI baud
  //clock:
  //  Value     Description
  //  0x0       System clock (based on clock source and divisor factor)
  //  0x1-0x4   Reserved
  //  0x5       PIOSC
  //  0x6-0xF   Reserved  (pg 984)
  HWREG(SSI0_BASE + SSI_O_CC) |= (SSI_CC_CS_SYSPLL);

//13. Configure the clock pre-scaler (CPSR) and clock rate (SCR)
// In class found max frequency 917 kHz
// Set bitrate to 25kHz
// bitrate = SYSCLCK / (CPSR * (1+SCR) = 40MHz / 800
// Define CPSR = 40, SCR = 39

  HWREG(SSI0_BASE + SSI_O_CPSR) &= 0xffffff00;
  HWREG(SSI0_BASE + SSI_O_CPSR) |= SSI_PRESCALER;   //defined as 40
  HWREG(SSI0_BASE + SSI_O_CR0) &= 0xffff00ff;
  HWREG(SSI0_BASE + SSI_O_CR0) |= SCR_VALUE;        //defined as 39

//14. Configure phase & polarity (SPH, SPO), mode (FRF), data size (DSS)
  // configure SPH = 1, because data is captured at the second edge
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH;
  // configure SPO = 1, because idle state is high
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO;
  // set freescale SPI Mode
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_FRF_MOTO;
  //Set data size to 8 bits
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8;

//15. Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

//16. Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;

//17. Enable the NVIC interrupt for the SSI when starting to transmit
  //enable bit 7 in EN0
  HWREG(NVIC_EN0) |= BIT7HI;     //when disable EN = DIS

//Globally enable interrupts (done elsewhere, comment out)
  __enable_irq();

  printf("\n Completed Initialization of SPUD communication\r\n");
}

/****************************************************************************
 * QuerySPUD
 * Receives a command address to send to SPUD. Returns nothing.
 * Description
    The first byte from the TRACTOR to SPUD is a read register commmand.
    This command is followed by two bytes of 0x00.
****************************************************************************/
void QuerySPUD(uint8_t ByteToSend)
{
  // Store the most recently sent command
  RecentQuery = ByteToSend;

  // Write CommandAddress to SSI Data Register
  HWREG(SSI0_BASE + SSI_O_DR) = ByteToSend;

  //Send trash bits for communication purposes
  HWREG(SSI0_BASE + SSI_O_DR) = VOIDBYTE;
  HWREG(SSI0_BASE + SSI_O_DR) = VOIDBYTE;

  // Activate local interrupt to send data
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
}

/****************************************************************************
 * ReadSPUD (aka EOTISR, interrupt output)
 * Takes no parameters. Returns the relevant SPUD information.
 * Description
    The first and second bytes are irrelevant (0x00 and 0xFF respectively).
    The third byte containts the contents of the requested register.
****************************************************************************/
void ReadSPUD(void)
{
  // Disable local interrupt
  HWREG(SSI0_BASE + SSI_O_IM) &= ~SSI_IM_TXIM;
  //Read trash byte, 0x00
  ReceivedTrash1 = HWREG(SSI0_BASE + SSI_O_DR);
  //Read trash byte, 0xFF
  ReceivedTrash2 = HWREG(SSI0_BASE + SSI_O_DR);
  //Read requested content
  ReceivedByte = HWREG(SSI0_BASE + SSI_O_DR);

  // Post interrupt event to SPUD service
  ES_Event_t ThisEvent;
  ThisEvent.EventType = EU_EOT;
  ThisEvent.EventParam = ReceivedByte;
  PostSPUDService(ThisEvent);
}

/****************************************************************************
 * Init Team - Handle team assignment (CKH or GHI) by reading pin value,
 * Mechanically controlled by a switch
****************************************************************************/
static void InitTeam(void)
{
  // Read input pin and see if HI or LO.
  if ((HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) & TEAMPINHI) == 0)
  {
    // if LO, then set Team to GHI
    Team = GHI;
    printf("\n\r TEAM GHI (blue)");
  }
  else
  {
    // if HI, then set Team to CKH
    Team = CKH;
    printf("\n\r TEAM CKH (red)");
  }
}


/****************************************************************************
 * The following public functions are used to query modular level variables
****************************************************************************/
// Query whether we are on team CKH (red) or GHI (blue)
bool QueryTeam(void)
{
  return Team;
}

// Query permit status: Waiting, Game Start, Sudden Death, Game Over
uint8_t QueryStatus(void)
{
  return PermitStatus;
}

// Query location of beacons
uint8_t Query_MYBeacon1(void)
{
  if (Team == CKH)
  {
    return CKHBeacon1;
  }
  else
  {
    return GHIBeacon1;
  }
}

uint8_t Query_MYBeacon2(void)
{
  if (Team == CKH)
  {
    return CKHBeacon2;
  }
  else
  {
    return GHIBeacon2;
  }
}

uint8_t Query_ENEMYBeacon1(void)
{
  if (Team == GHI)
  {
    return CKHBeacon1;
  }
  else
  {
    return GHIBeacon1;
  }
}

uint8_t Query_ENEMYBeacon2(void)
{
  if (Team == GHI)
  {
    return CKHBeacon2;
  }
  else
  {
    return GHIBeacon2;
  }
}

// Query current score
uint16_t Query_MYScore(void)
{
  if (Team == GHI)
  {
    return GHIScore;
  }
  else
  {
    return CKHScore;
  }
}

uint16_t Query_ENEMYScore(void)
{
  if (Team == CKH)
  {
    return GHIScore;
  }
  else
  {
    return CKHScore;
  }
}

// Query location of exclusive zone permits
uint8_t QueryExclusivePermit(void)
{
  if (Team == CKH)
  {
    return ExclusivePermit & LAST_4_BIT_MASK;
  }
  else
  {
    return ExclusivePermit >> 4;
  }
}

// Query location of exclusive zone permits
uint8_t QueryEnemyPermit(void)
{
  if (Team == GHI)
  {
    return ExclusivePermit & LAST_4_BIT_MASK;
  }
  else
  {
    return ExclusivePermit >> 4;
  }
}

// Query location of neutral zone permits
uint8_t QueryNeutralPermit1(void)
{
  return NeutralPermit & LAST_4_BIT_MASK;
}

// Query location of neutral zone permits
uint8_t QueryNeutralPermit2(void)
{
  return NeutralPermit >> 4;
}
