/****************************************************************************
 Module
   d:\me218bTNG\Lectures\Lecture31\SMTemplateMain.c

 Revision
   1.0.1

 Description
   This is a template file for a main() implementing Hierarchical
   State Machines within the Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 11/26/17 14:09 jec     updated with changes from main.c for headers & port
                        initialization
 02/27/17 12:01 jec     set framework timer rate to 1mS
 02/06/15 13:21 jec     minor tweaks to include header files & clock init for
                        Tiva
 02/08/12 10:32 jec     major re-work for the Events and Services Framework
                        Gen2
 03/03/10 00:36 jec     now that StartTemplateSM takes an event as input
                        you should pass it something.
 03/17/09 10:20 jec     added cast to return from RunTemplateSM() to quiet
                        warnings because now that function returns Event_t
 02/11/05 16:56 jec     Began coding
****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"
#include "termio.h"
#include "EnablePA25_PB23_PD7_PF0.h"
#include "LEDIndicatorService.h"

#define clrScrn() printf("\x1b[2J")
#define goHome() printf("\x1b[1,1H")
#define clrLine() printf("\x1b[K")

// Listing of all pins to initialize
// Port B
#define REDPINHI BIT0HI           // Red LED, output
#define REDPINLO BIT0LO           // Red LED, output
#define BLUEPINHI BIT1HI          // Blue LED, output
#define BLUEPINLO BIT1LO          // Blue LED, output
#define FRONT_SWITCHES_HI BIT3HI  // Front switches, enabled
#define FRONT_SWITCHES_LO BIT3LO  // Front switches, input
#define BACK_SWITCHES_HI BIT2HI   // Back switches, enabled
#define BACK_SWITCHES_LO BIT2LO   // Back switches, input

// Port E
#define BBPINHI BIT0HI          //Beam Break, INPUT
#define BBPINLO BIT0LO          //Beam Break, INPUT
#define EMPINHI BIT5HI          // electromagnet, OUTPUT
#define EMPINLO BIT5LO          // electromagnet, OUTPUT
#define TEAMPINHI BIT1HI        // team setting, INPUT
#define TEAMPINLO BIT1LO        // team setting, INPUT

static void initPins(void);

int main(void)
{
  ES_Return_t ErrorType;

// Set the clock to run at 40MhZ using the PLL and 16MHz external crystal
  SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
      | SYSCTL_XTAL_16MHZ);

  // Initialize the terminal for puts/printf debugging
  TERMIO_Init();
  clrScrn();

// When doing testing, it is useful to announce just which program
// is running.
  puts("\rStarting Test Harness for \r");
  printf( "the 2nd Generation Events & Services Framework V2.4\r\n");
  printf( "Template for HSM implementation\r\n");
  printf( "%s %s\n", __TIME__, __DATE__);
  printf( "\n\r\n");

  // reprogram the ports that are set as alternate functions or
  // locked coming out of reset. (PA2-5, PB2-3, PD7, PF0)
  // After this call these ports are set
  // as GPIO inputs and can be freely re-programmed to change config.
  // or assign to alternate any functions available on those pins
  PortFunctionInit();

  // Your hardware initialization function calls go here
  initPins();

  // now initialize the Events and Services Framework and start it running
  ErrorType = ES_Initialize(ES_Timer_RATE_1mS);
  if (ErrorType == Success)
  {
    ErrorType = ES_Run();
  }
//if we got to here, there was an error
  switch (ErrorType)
  {
    case FailedPointer:
    {
      puts("Failed on NULL pointer");
    }
    break;
    case FailedInit:
    {
      puts("Failed Initialization");
    }
    break;
    default:
    {
      puts("Other Failure");
    }
    break;
  }
  for ( ; ;)   // hang after reporting error
  {
    ;
  }
}

/* Initialize all used TIVA ports and pins (namely port B and E)
 * Excludes pins related to SPI communication, interrupts, and PWM
 */
static void initPins(void)
{
  printf("\n\rPins in progress");

  //1. Enable the clock to selected GPIO port
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; // Port B
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4; // Port E
  //HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5; //Port F
  //2. Wait for the GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1) // Port B
  {}
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R4) // Port E
  {}
  /*
while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4) != SYSCTL_PRGPIO_R5) // Port F
{}*/
  printf("\n\rPorts B, E, and F init");

  //3. Program the port lines for digital I/O (use GPIO_O_DEN)
  //4. Program the required data directions on the port lines (Out = HI, In = LO)

  // Port B
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (REDPINHI | BLUEPINHI);                // Step 3
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (REDPINHI | BLUEPINHI);                // Step 4, LED outputs
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (REDPINLO & BLUEPINLO);  // Initially, turn off LEDs

  // Port E
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BBPINHI | EMPINHI | TEAMPINHI); // Step 3
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= EMPINHI;                         // Step 4, Electromagnet output
  HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) &= EMPINLO;           // Initially, set LO
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= (BBPINLO & TEAMPINLO);           // Beam break, Team input
  HWREG(GPIO_PORTE_BASE + GPIO_O_PUR) |= (BBPINHI | TEAMPINHI);           // Internal pull-up resistor
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (FRONT_SWITCHES_HI);             // Set the pin enabled to the front switches
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= (FRONT_SWITCHES_LO);             // Set the direction to be an input
  HWREG(GPIO_PORTE_BASE + GPIO_O_PUR) |= (FRONT_SWITCHES_HI);
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BACK_SWITCHES_HI);  // Set the pin enabled to the front switches
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= (BACK_SWITCHES_LO);  // Set the direction to be an input
  HWREG(GPIO_PORTE_BASE + GPIO_O_PUR) |= (BACK_SWITCHES_HI);

  /*
  // Indicator LEDs
  HWREG(LED_LSB_PORT + GPIO_O_DEN) |= (LED_LSB_HI);                // Set as digital I/O
  HWREG(LED_LSB_PORT + GPIO_O_DIR) |= (LED_LSB_HI);                // Set as output

  HWREG(LED_2_LSB_PORT + GPIO_O_DEN) |= (LED_2_LSB_HI);                // Set as digital I/O
  HWREG(LED_2_LSB_PORT + GPIO_O_DIR) |= (LED_2_LSB_HI);                // Set as output

  HWREG(LED_3_LSB_PORT + GPIO_O_DEN) |= (LED_3_LSB_HI);                // Set as digital I/O
  HWREG(LED_3_LSB_PORT + GPIO_O_DIR) |= (LED_3_LSB_HI);                // Set as output

  HWREG(LED_4_LSB_PORT + GPIO_O_DEN) |= (LED_4_LSB_HI);                // Set as digital I/O
  HWREG(LED_4_LSB_PORT + GPIO_O_DIR) |= (LED_4_LSB_HI);                // Set as output
  */

  printf("\n\rTIVA pins initialized");
}
