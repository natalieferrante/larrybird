/****************************************************************************
 Module
   BeamBreakModule.c

 Revision
   1.0.1

 Description
   This module contains an event checker to detect if the beam breaker has broken.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/23/20 20:19 JYC     Updated BeamBreak eventchecker to only check during
                        ApproachingMiner State, otherwise move on
 02/22/20 19:21 LG      Created file
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "BeamBreakModule.h"
#include "AcquireMinerSM.h"
#include "GameplayHSM.h"

// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/*----------------------------- Module Defines ----------------------------*/
#define BBPINHI BIT0HI        // Beam break, PE0
#define BBPINLO BIT0LO

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     CheckBeamBroken

 Parameters
    None

 Returns
     Nothing

 Description
     Event checker that checks to see if beam has been broken.
****************************************************************************/

bool CheckBeamBroken(void)
{
  static uint8_t  LastValue = 0;
  bool            ReturnVal = false;
  if (QueryAcquireMinerSM() == ApproachingMiner)  // Check current state of AcquireMiner SM
  {
    uint8_t CurrentValue = HWREG(GPIO_PORTE_BASE + (GPIO_O_DATA + ALL_BITS)) & BBPINHI;
    // Set Alignment to read from port pin
    // If LO, then beam is broken. If HI, then beam unbroken.
    // Only post a BEAM BROKEN event if the sensor was
    // previously unbroken and is now broken
    if ((CurrentValue == 0) && (LastValue != 0))
    {
      ReturnVal = true;     // Set ReturnVal = True
      // If the MINER has entered the plow
      ES_Event_t ThisEvent;
      ThisEvent.EventType = EU_BEAM_BROKEN;
      PostGameplayHSM(ThisEvent);
    }
    LastValue = CurrentValue;
  }
  return ReturnVal; // Return ReturnVal
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
