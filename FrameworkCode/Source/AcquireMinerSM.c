/****************************************************************************
 Module
   AcquireMinerSM.c

 Description
   This is a hierarchal state machine (level 2) that handles MINER acquisition.

 Notes

 History
 When           Who       What/Why
 -------------- ---       --------
 02/23/20 11:00 jyc			  Adapted the template for AcquireMinerSM
 Forgot to Log  jyc       Code is working! Functional up until during functions
 03/04/20 23:59 jyc + egg Adapted to handle FindSquare

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"

/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "AcquireMinerSM.h"
#include "DetectMinerModule.h"
#include "DriveService.h"
#include "ElectromagnetModule.h"
#include "HandleCollisionSM.h"
#include "ZigzagToSquare.h"
#include "ShiftPositionSM.h"
#include "AccelerometerService.h"

/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define ENTRY_STATE DetectingMiner
#define EMPTY_COLLISION_STATE DetectingMiner
#define MINER_COLLISION_STATE FindingSquare
#define EMPTY_SHIFT_STATE DetectingMiner
#define MINER_SHIFT_STATE FindingSquare

#define MINER_ONE 1
#define MINER_TWO 2

// Timing
#define DETECT_TIME_LIMIT     10000   // must detect MINER in 5 seconds
#define ALIGN_TIME_LIMIT      1000    // Bang Bang control - check if still aligned
#define OVERSHOOT_TIME_LIMIT  1000    // drive forward for an extra 1000 ms
#define RELEASE_TIME_LIMIT    3000    // drive backwards to release MINER
#define COLLIDE_TIME_LIMIT    2000    // drive backwards to respond to collision
#define SHIFT_TIME_LIMIT      5000    // drive backwards to shift position
#define ORTHO_TURN_TIME       1000     // keep rotating and stopping to check ortho
#define ORTHO_STOP_TIME       3000     // keep rotating and stopping to check ortho

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine, things like during
   functions, entry & exit functions.They should be functions relevant to the
   behavior of this state machine
*/
static ES_Event_t DuringZigzagSM(ES_Event_t Event);
static ES_Event_t DuringHandlingCollision(ES_Event_t Event);
static ES_Event_t DuringShiftingPosition(ES_Event_t Event);

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well
static AcquisitionState_t CurrentState;
static bool MINERinPossession = false;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
    RunAcquireMinerSM
 Description
   Handles all action after permits are acquired up until the robot scores a point
 Notes
   uses nested switch/case to implement the machine.
 Author
   Julea Chin
****************************************************************************/
ES_Event_t RunAcquireMinerSM(ES_Event_t CurrentEvent)
{
  bool                MakeTransition = false;/* are we making a state transition? */
  AcquisitionState_t  NextState = CurrentState;
  ES_Event_t          EntryEventKind = { ES_ENTRY, 0 }; // default to normal entry to new state
  ES_Event_t          ReturnEvent = CurrentEvent;       // assume we are not consuming event

  switch (CurrentState)
  {
    case DetectingMiner:        // If current state is DetectingMiner
    {
      // Process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)   //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY:
          {
            // First, check whether it is safe to rotate right using color location
            // Set motors to rotate
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_TURN_RIGHT;
            PostEvent.EventParam = UNKNOWN_TURN;
            PostDriveService(PostEvent);
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;

          case EU_MINER_DETECTED: // If miner is detected
          {
            // Execute action 1: Set motors forward
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_FORWARD;
            PostEvent.EventParam = SLOW_RPM;
            PostDriveService(PostEvent);
            // Execute action 2: Initialize the alignment timer
            ES_Timer_InitTimer(ALIGN_TIMER, ALIGN_TIME_LIMIT);

            NextState = ApproachingMiner;
            MakeTransition = true; //mark that we are taking a transition
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;

          case EU_COLLISION:  // If event is collision
          {
            // Move to HandlingCollision state to handle collision
            NextState = HandlingCollision;
            MakeTransition = true; //mark that we are taking a transition
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;
          default:  // Error handler
          {}
          break;
        }
      }
    }
    break;

    case ApproachingMiner:       // If current state is ApproachingMiner
    {
      // Process any events
      if (CurrentEvent.EventType != ES_NO_EVENT)   //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_TIMEOUT:  //If ALIGN or OVERSHOOT timer timed out
          {
            // Check to see if we are still moving toward the beacon
            if (CurrentEvent.EventParam == ALIGN_TIMER)
            {
              // Check if aligned, then can keep moving forward
              if (GetMinerAlignment() == true)
              {
                // Execute action 1: Re-initialize the alignment timer
                ES_Timer_InitTimer(ALIGN_TIMER, ALIGN_TIME_LIMIT);
              }
              // If unaligned, robot needs to re-detect the miner
              else
              {
                // Execute action 1: Stop the alignment timer
                ES_Timer_StopTimer(ALIGN_TIMER);
                NextState = DetectingMiner;
                MakeTransition = true; //mark that we are taking a transition
              }
            }
            // Overshot the beam break to guarantee that the miner was acquired
            else if (CurrentEvent.EventParam == OVERSHOOT_TIMER)
            {
              // Miner has been acquired!
              // Update that miner is now in possession
              MINERinPossession = true;
              NextState = FindingSquare;
              MakeTransition = true;  //mark that we are taking a transition
            }
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;

          case EU_BEAM_BROKEN:  // if beam is broken
          {
            // Execute action 1: Turn on Electromagnet
            TurnMagnetOn();
            MINERinPossession = true;
            // Execute action 2: Stop ALIGN timer
            ES_Timer_StopTimer(ALIGN_TIMER);
            // Execute action 3: Initialize the OVERSHOOT timer
            ES_Timer_InitTimer(OVERSHOOT_TIMER, OVERSHOOT_TIME_LIMIT);
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;

          case EU_COLLISION:  //If event is collision
          {
            // Move to HandlingCollision state to handle collision
            NextState = HandlingCollision;
            MakeTransition = true; //mark that we are taking a transition
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;
          default:  // Error handler
          {}
          break;
        }
      }
    }
    break;

    case FindingSquare:        // If current state is FindingSquare
    {
      // Execute During function for ZigzagToSquareSM. ES_ENTRY & ES_EXIT are
      // processed here allow the lower level state machines to re-map
      // or consume the event
      ReturnEvent = CurrentEvent = DuringZigzagSM(CurrentEvent);

      //process any events

      // Wait here until Permits are acquired
      if (CurrentEvent.EventType != ES_NO_EVENT)   //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case EU_MINER_MINING:  //If MINER is successfully acquiring ore
          {
            // Move to releasing miner state
            NextState = ReleasingMiner;
            MakeTransition = true; //mark that we are taking a transition
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;
          
          case EU_COLLISION:  //If event is collision
          {
            // Move to HandlingCollision state to handle collision
            NextState = HandlingCollision;
            MakeTransition = true; //mark that we are taking a transition
            // Consume event
            ReturnEvent.EventType = ES_NO_EVENT;
          }
          break;

          case ES_EXIT:
          {
            // If exit, stop motors
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_STOP;
            PostDriveService(PostEvent);
            ReturnEvent.EventType = ES_NO_EVENT; // Consume Event
          }
          break;
          
          default:  // Error handler
          {}
          break;
        }
      }
    }
    break;

    case ReleasingMiner:        // If current state is ReleasingMiner
    {

      // Keep reversing until satisfied to begin
      if (CurrentEvent.EventType != ES_NO_EVENT)   //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case ES_ENTRY: 
          {
            // Execute action 1: Turn off electromagnet
            TurnMagnetOff();
            // Miner is no longer in possession once magnet is off
            MINERinPossession = false;
            // Execute action 2: Set motors to reverse
            ES_Event_t PostEvent;
            PostEvent.EventType = EU_REVERSE;
            PostEvent.EventParam = SLOW_RPM;
            PostDriveService(PostEvent);
            // Execute action 3: Initiate shift timer to release MINER
            ES_Timer_InitTimer(SHIFT_TIMER, RELEASE_TIME_LIMIT);
            ReturnEvent.EventType = ES_NO_EVENT; // Consume Event
          }
          break;  
            
          case ES_TIMEOUT:  //If Shift Timeout
          {
            if (CurrentEvent.EventParam == SHIFT_TIMER)
            {
              // If we get 1 miner, we are happy. Game is over (for us).
              // If feeling ambitious, can consume the event and continue gameplay
              ReturnEvent.EventType = EU_GAME_OVER; // Consume Event
            }
          }
          break;

          
          default:  // Error handler
          {}
          break;
        }
      }
    }
    break;

    case HandlingCollision:       // If current state is HandlingCollision
    {
      printf("\n\rSTATE: HandlingCollision");
      // Execute During function for CollisionSM. ES_ENTRY & ES_EXIT are
      // processed here allow the lower level state machines to re-map
      // or consume the event
      ReturnEvent = CurrentEvent = DuringHandlingCollision(CurrentEvent);
      if (CurrentEvent.EventType != ES_NO_EVENT)   //If an event is active
      {
        switch (CurrentEvent.EventType)
        {
          case EU_CRASH_HANDLED:  //If collision timer times out
          {
            if (CurrentEvent.EventParam == COLLIDE_TIMER)
            {
              if (MINERinPossession == true)
              {
                // If holding MINER, need to find the correct permit square
                NextState = MINER_COLLISION_STATE;
              }
              else
              {
                // If not holding MINER, need to find a MINER
                NextState = EMPTY_COLLISION_STATE;
              }
              MakeTransition = true;        // Mark that we are taking a transition
              // Consume event
              ReturnEvent.EventType = ES_NO_EVENT;
            }
          }
          break;
          default:  // Error handler
          {}
          break;
        }
      }
    }
    break;

  }
  //   If we are making a state transition
  if (MakeTransition == true)
  {
    //   Execute exit function for current state
    CurrentEvent.EventType = ES_EXIT;
    RunAcquireMinerSM(CurrentEvent);

    CurrentState = NextState;  //Modify state variable

    //  Execute entry function for new state
    //  this defaults to ES_ENTRY
    RunAcquireMinerSM(EntryEventKind);
  }
  return ReturnEvent;
}

/****************************************************************************
 Function
     StartAcquireMinerSM

 Parameters
     None

 Returns
     None

 Description
     Does any required initialization for this state machine
 Notes

 Author
     Julea Chin
****************************************************************************/
void StartAcquireMinerSM(ES_Event_t CurrentEvent)
{
  // Check for history
  if (ES_ENTRY_HISTORY != CurrentEvent.EventType)
  {
    CurrentState = ENTRY_STATE;
  }
  // call the entry function (if any) for the ENTRY_STATE
  RunAcquireMinerSM(CurrentEvent);
}

/****************************************************************************
 Function
     QueryAcquireMinerSM

 Parameters
     None

 Returns
     AcquisitionState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     Julea Chin
****************************************************************************/
AcquisitionState_t QueryAcquireMinerSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
// During function for Zigzag HSM
static ES_Event_t DuringZigzagSM(ES_Event_t Event)
{
  ES_Event_t ReturnEvent = Event;   // assume no re-mapping or consumption
  //ES_Event_t CurrentEvent;

  // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
  if ((Event.EventType == ES_ENTRY) ||
      (Event.EventType == ES_ENTRY_HISTORY))
  {
    // implement any entry actions required for this state machine

    // Start the lower level state machine
    StartZigzagSM(Event);
  }
  else if (Event.EventType == ES_EXIT)
  {
    // on exit, give the lower level a chance to clean up first
    RunZigzagSM(Event);
    // now do any local exit functionality
  }
  else
  // do the 'during' function for this state
  {
    // run the lower level state machine
    ReturnEvent = RunZigzagSM(Event);
  }
  // return either Event, if you don't want to allow the lower level machine
  // to remap the current event, or ReturnEvent if you do want to allow it.
  return ReturnEvent;
}

// During function for Handling Collision HSM
static ES_Event_t DuringHandlingCollision(ES_Event_t Event)
{
  ES_Event_t ReturnEvent = Event;   // assume no re-mapping or consumption

  // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
  if ((Event.EventType == ES_ENTRY) ||
      (Event.EventType == ES_ENTRY_HISTORY))
  {
    // implement any entry actions required for this state machine

    // Start the lower level state machine
    StartHandleCollisionSM(Event);
  }
  else if (Event.EventType == ES_EXIT)
  {
    // on exit, give the lower level a chance to clean up first
    RunHandleCollisionSM(Event);
    // now do any local exit functionality
  }
  else
  // do the 'during' function for this state
  {
    // run the lower level state machine
    ReturnEvent = RunHandleCollisionSM(Event);
  }
  // return either Event, if you don't want to allow the lower level machine
  // to remap the current event, or ReturnEvent if you do want to allow it.
  return ReturnEvent;
}
