/****************************************************************************
  HandleCollisionSM.h
  Header file for Hierarchical Sate Machines AKA StateCharts
  Point Person: Natalie Ferrante
 ****************************************************************************/

#ifndef HSMCollision_H
#define HSMCollision_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { NegatingMotion, SlightTurning, MovingForward } HandleCollisionState_t ;

// Public Function Prototypes
ES_Event_t RunHandleCollisionSM( ES_Event_t CurrentEvent );
void StartHandleCollisionSM ( ES_Event_t CurrentEvent );
HandleCollisionState_t QueryHandleCollisionSM ( void );

#endif /*HSMCollision_H */

