/****************************************************************************

  Header file for Accelerometer Communication service
  based on the Gen 2 Events and Services Framework
  Point Person: Amanda Spyropoulos
 ****************************************************************************/

#ifndef AccelerometerService_H
#define AccelerometerService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"
#include "ES_Events.h"

bool InitAccelerometerService(uint8_t Priority);
bool PostAccelerometerService(ES_Event_t ThisEvent);
ES_Event_t RunAccelerometerService(ES_Event_t ThisEvent);
void QueryAccelerometerInit(uint8_t ByteToSend);
void QueryAccelerometer(uint8_t ByteToSend);
bool QueryOrthogonal(void);
void ReadAccelerometer(void);
int16_t GetRoll(void);
int16_t GetPitch(void);
int16_t GetYaw(void);
void InitWaitOrthogonal(void);

#endif /* AccelerometerService_H */
