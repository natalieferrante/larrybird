/****************************************************************************

  Header file for TestMe service
  based on the Gen 2 Events and Services Framework
  Point Person: Julea Chin

 ****************************************************************************/

#ifndef TestMe_H
#define TestMe_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"
#include "ES_Events.h"


bool InitTestMe(uint8_t Priority);
bool PostTestMe(ES_Event_t ThisEvent);
ES_Event_t RunTestMe (ES_Event_t ThisEvent);

#endif /* TestMe_H */
