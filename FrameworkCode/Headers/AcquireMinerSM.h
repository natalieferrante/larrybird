/****************************************************************************

  Header file for AcquireMinerSM (State Machine)
  Based on the Gen 2 Events and Services Framework
  Point Person: Julea Chin

 ****************************************************************************/

#ifndef AcquireMinerSM_H
#define AcquireMinerSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { DetectingMiner, ApproachingMiner, HandlingCollision, 
  ShiftingPosition, FindingSquare, ReleasingMiner } AcquisitionState_t;

// Public Function Prototypes
void StartAcquireMinerSM ( ES_Event_t CurrentEvent );
ES_Event_t RunAcquireMinerSM( ES_Event_t CurrentEvent );
AcquisitionState_t QueryAcquireMinerSM ( void );
  
#endif /* AcquireingMinerSM_H */
