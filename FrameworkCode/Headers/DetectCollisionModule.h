/****************************************************************************

  Header file for Detect Collision Module
  Eventchecker to see if Collision occurs through 4 limit switches
  Point Person: Lisa Gardner

 ****************************************************************************/

#ifndef DetectCollisionModule_H
#define DetectCollisionModule_H

#include "ES_Types.h"
#include "ES_Events.h"

bool CheckSwitch(void);

#endif /* DetectCollisionModule_H */
