/****************************************************************************

  Header file for SPUD Communication service
  based on the Gen 2 Events and Services Framework
  Point Person: Julea Chin

 ****************************************************************************/

#ifndef SPUDService_H
#define SPUDService_H

#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"
#include "ES_Events.h"

/*
typedef enum { Waiting, UpdatePermit, UpdateBeacon1, UpdateBeacon2, 
  UpdateExclusivePermit, GameOver} SPUDState_t;
*/

// For SPUD Service handling
bool InitSPUDService(uint8_t Priority);
bool PostSPUDService(ES_Event_t ThisEvent);
ES_Event_t RunSPUDService(ES_Event_t ThisEvent);
void QuerySPUD(uint8_t ByteToSend);
void ReadSPUD(void);

// Queries for other modules to access SPUD information
bool QueryTeam (void);
uint8_t QueryStatus(void);
uint8_t Query_MYBeacon1(void);
uint8_t Query_MYBeacon2(void);
uint8_t Query_ENEMYBeacon1(void);
uint8_t Query_ENEMYBeacon2(void);
uint16_t Query_MYScore(void);
uint16_t Query_ENEMYScore(void);
uint8_t QueryExclusivePermit(void);
uint8_t QueryEnemyPermit(void);
uint8_t QueryNeutralPermit1(void);
uint8_t QueryNeutralPermit2(void);

#endif /* SPUDService_H */
