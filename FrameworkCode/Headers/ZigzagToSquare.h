/****************************************************************************

  Header file for ZigzagToSquare (State Machine)
  Based on the Gen 2 Events and Services Framework
  Point Person: Julea Chin and Natalie Ferrante

 ****************************************************************************/

#ifndef ZigzagToSquare_H
#define ZigzagToSquare_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { FindingCurrent, FindingPrevious, Turning, GoingForward } ZigzagState_t;

// Public Function Prototypes
void StartZigzagSM( ES_Event_t CurrentEvent );
ES_Event_t RunZigzagSM( ES_Event_t CurrentEvent );
ZigzagState_t QueryZigzagSM ( void );
  
#endif /* ZigzagToSquare_H */
