/****************************************************************************

  Header file for LED Indicator Service Module
  Used as an external indication of the robot's state
  Amanda Spyropoulos

 ****************************************************************************/
 
#ifndef LEDIndicatorService_H
#define LEDIndicatorService_H


#include "ES_Types.h"
#include "ES_Events.h"

// defines for the LED state mappings

#define MOVE_TO_COLUMN 1
#define COL_OVERSHOOT 2
#define ROTATING_90 3
#define MOVING_FORWARD 4
#define COLLISION 7

// Pins associated with each LED + Ports
#define LED_4_LSB_HI BIT2HI // PE2
#define LED_4_LSB_LO BIT2LO // PE2
#define LED_4_LSB_PORT GPIO_PORTE_BASE

#define LED_LSB_HI BIT2HI // PF2
#define LED_LSB_LO BIT2LO // PF2
#define LED_LSB_PORT GPIO_PORTF_BASE

#define LED_3_LSB_HI BIT3HI // PF3
#define LED_3_LSB_LO BIT3LO // PF3
#define LED_3_LSB_PORT GPIO_PORTF_BASE

#define LED_2_LSB_HI BIT4HI // PF4
#define LED_2_LSB_LO BIT4LO // PF4
#define LED_2_LSB_PORT GPIO_PORTF_BASE


bool InitLEDIndicatorService(uint8_t Priority);
ES_Event_t RunLEDIndicatorService(ES_Event_t ThisEvent);
bool PostLEDIndicatorService(ES_Event_t ThisEvent);

#endif /* LEDIndicatorService_H */
