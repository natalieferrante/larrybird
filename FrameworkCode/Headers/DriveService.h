/****************************************************************************

  Header file for Drive Service Module
  Handles robot movement
  Point Person: Natalie Ferrante

 ****************************************************************************/
 
#ifndef DriveService_H
#define DriveService_H

#include "ES_Types.h"
#include "ES_Events.h"

#define SLOW_RPM        40  // Slow forward/reverse speed
#define FAST_RPM        50  // Fast forward/reverse speed
#define CRAWL_RPM       20  // Super slow forward/reverse speed
#define UNKNOWN_TURN    0
#define STOPPING        0
#define MOVING_FORWARD  1
#define TURNING_RIGHT   2
#define TURNING_LEFT    3
#define REVERSING       4

bool InitDriveService(uint8_t Priority);
ES_Event_t RunDriveService(ES_Event_t ThisEvent);
bool PostDriveService(ES_Event_t ThisEvent);

void ControlLaw(void);
void EncoderLEFTInputCaptureResponse(void);
void EncoderRIGHTInputCaptureResponse(void);
void PeriodicIntResponse(void);
uint8_t QueryCurrentMovement (void);

#endif /* DriveService_H */
