/****************************************************************************
  Header file for FindSquareSM
  Point Person: Lisa Gardner
 ****************************************************************************/

#ifndef FindSquareSM_H
#define FindSquareSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"


// typedefs for the states
// State definitions for use with the query function
typedef enum { MovingToColumn, ColOvershoot, Rotating90, MovingToRow } FindSquareState_t ;


// Public Function Prototypes

ES_Event_t RunFindSquareSM( ES_Event_t CurrentEvent );
void StartFindSquareSM ( ES_Event_t CurrentEvent );
FindSquareState_t QueryFindSquareSM ( void );

#endif /*SHMTemplate_H */

