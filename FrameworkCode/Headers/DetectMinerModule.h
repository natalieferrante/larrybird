/****************************************************************************

  Header file for Beacon Detect Module
  Interrupt that posts if a beacon is detected
  Point Person: Lisa Gardner

 ****************************************************************************/

#ifndef DetectMinerModule_H
#define BeaconMinerModule_H

#include "ES_Types.h"

// Public Function Prototypes

void InitDetectionModule(void);
bool CheckMinerDetection(void);
bool GetMinerAlignment(void);
void IRInputCaptureResponse(void);
uint8_t GetTargetMiner(void);

#endif /* BeaconDetectModule_H */
