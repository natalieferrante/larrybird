/****************************************************************************

  Header file for ShiftPositionSM (State Machine)
  Based on the Gen 2 Events and Services Framework
  Point Person: Amanda Spyropoulos

 ****************************************************************************/

#ifndef ShiftPositionSM_H
#define ShiftPositionSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum {GentlyTurning} ShiftPositionState_t;

// Public Function Prototypes
void StartShiftPositionSM ( ES_Event_t CurrentEvent );
ES_Event_t RunShiftPositionSM( ES_Event_t CurrentEvent );
ShiftPositionState_t QueryShiftPositionSM ( void );

#endif /* ShiftPositionSM_H */
