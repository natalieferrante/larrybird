/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework
  Point Person: Lisa Gardner

 ****************************************************************************/

#ifndef ElectromagnetModule_H
#define ElectromagnetModule_H

#include "ES_Types.h"

// Public Function Prototypes
void InitElectromagnetModule(void);
void TurnMagnetOn(void);
void TurnMagnetOff(void);

#endif /* ElectromagnetModule_H */
