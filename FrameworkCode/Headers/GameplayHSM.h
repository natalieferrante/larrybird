/****************************************************************************
  
  Header file for the Gameplay Hierarchical State Machines AKA StateCharts
  Handles basic gameplay 
  Point Person: Julea Chin

 ****************************************************************************/

#ifndef GameplayHSM_H
#define GameplayHSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"

// State definitions for use with the query function
typedef enum { Waiting, Playing, GameOver } GameState_t;

// Public Function Prototypes
ES_Event_t RunGameplayHSM(ES_Event_t CurrentEvent);
void StartGameplayHSM(ES_Event_t CurrentEvent);
bool PostGameplayHSM(ES_Event_t ThisEvent);
bool InitGameplayHSM(uint8_t Priority);

#endif /*GameplayHSM_H */
