/****************************************************************************

  Header file for Beam Break Module
  Eventchecker to see if MINER is engaged through a IR beam break
  Point Person: Lisa Gardner

 ****************************************************************************/

#ifndef BeamBreakModule_H
#define BeamBreakModule_H

#include "ES_Types.h"
#include "ES_Events.h"

bool CheckBeamBroken(void);

#endif /* BeamBreakModule_H */
