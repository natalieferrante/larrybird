// Beam Break Service

/****************************************************************************
 Function
     CheckBeamBroken

 Parameters
    None

 Returns
     Nothing

 Description
     Event checker that checks to see if beam has been broken.
****************************************************************************/
// Set Alignment to read from port pin
    // If LO, then beam is broken. If HI, then beam unbroken.
    // Only post a BEAM BROKEN event if the sensor was
    // previously unbroken and is now broken
    // If the MINER has entered the plow
	// Post to gameplay about the Beam Break

