// Gameplay SM Pseudocode

/****************************************************************************
 Function
     InitGameplayHSM

 Description
     Saves away the priority,  and starts
     the top level state machine
****************************************************************************/
  // Save our priority
  // Start the Gameplay State machine
  // Initialize the detect miner module

/****************************************************************************
 Function
     PostGameplayHSM

 Description
     Posts an event to this state machine's queue
****************************************************************************/
// Post the priority and event

/****************************************************************************
 Function
    RunGameplayHSM
 Description
   the run function for the top level state machine
 Notes
   uses nested switch/case to implement the machine.
****************************************************************************/
// Switch based on current state
  // If current state is state one, Waiting
  // Process any events
    //If an event is active
      //Entry event
        // Handle entry
        // Get team using public helper function
        // Set team LED
          // CKH = RED TEAM
          // GHI = BLUE TEAM
        // Initialize timer to flash LEDs

    // If permits issued, time to start mining for ore!
      // Turn on team LEDs (solidly)
      // Mark that LEDs are currently on 
      // Decide what the next state will be
      // Mark that we are taking a transition
    
    // If timeout
      // Flash team LEDs based on timer
      // If flash flag true
        // Turn off team LED
        // Reinitialize the flash timer
        // Mark to turn on LED
      // Otherwise,
        // Turn on team LED
        // Reinitialize the flash timer
        // Mark to turn off LED

  // If current state is state two, Playing
    // Execute During function for state one. ES_ENTRY & ES_EXIT are
    // processed here allow the lowere level state machines to re-map
    // or consume the event

    //Keep playing unless the game ends
    // If game over event
      // Game is over
      // Mark that we are taking a transition

  // As soon as game is over
    // Stop motors
    // Set up timers to flash purple LEDs
    // Flash purple LEDS. Turn on both red and blue.
    
    // If timeout, flash flag is true
      // Turn off red and blue LED (purple)
      // Reinitialize the flash timer
      // Mark to turn on LEDs
    // If timeout, flash flag is false 
      // Turn on LEDs
      // Reinitialize the flash timer
      // Mark to turn off LEDs

  //  If we are making a state transition
  //  Execute exit function for current state
  //  Modify state variable

    // Execute entry function for new state
    // this defaults to ES_ENTRY
    
  // in the absence of an error the top level state machine should
  // always return ES_NO_EVENT, which we initialized at the top of func


/****************************************************************************
 Function
     StartGameplayHSM
 Description
     Does any required initialization for this state machine
****************************************************************************/
  // Set the first current state to Waiting for permits
  // now we need to let the Run function init the lower level state machines
  // use LocalEvent to keep the compiler from complaining about unused var

/***************************************************************************
 private functions
 ***************************************************************************/

// During function for During Playing
  // assume no re-mapping or comsumption
   // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
  // Start the start function for AcquireMiner SM

  // On exit, give the lower levels a chance to clean up first
  // Execute action 1: Stop motors

  // Otherwise, do the 'during' function for this state

  // run any lower level state machines
  // do any activity that is repeated as long as we are in this state

  // return either Event, if you don't want to allow the lower level machine
  // to remap the current event, or ReturnEvent if you do want to allow it.

