// Acquire Miner SM

/****************************************************************************
 Function
    RunAcquireMinerSM
 Description
   Handles all action after permits are acquired up until the robot scores a point
 Notes
   uses nested switch/case to implement the machine.
****************************************************************************/
// Switch based on state
  // If current state is DetectingMiner
      // Process any events
      //If an event is active
      // Switch on event type
		// If entry
            // First, check whether it is safe to rotate right using color location
            // Set motors to rotate

            // If miner is detected
            // Execute action 1: Set motors forward
            // Execute action 2: Initialize the alignment timer
            //mark that we are taking a transition
            // Consume event

       	// If event is collision
            // Move to HandlingCollision state to handle collision
            //mark that we are taking a transition
            // Consume event

  // If current state is ApproachingMiner
      // Process any events
    	//If an event is active
      	//If ALIGN or OVERSHOOT timer timed out
            // Check to see if we are still moving toward the beacon
              // Check if aligned, then can keep moving forward
                // Execute action 1: Re-initialize the alignment timer
              // If unaligned, robot needs to re-detect the miner
                // Execute action 1: Stop the alignment timer
            // Overshot the beam break to guarantee that the miner was acquired
              // Miner has been acquired!
              // Update that miner is now in possession
              //mark that we are taking a transition
            // Consume event

      	// if beam is broken
      		// Execute action 1: Turn on Electromagnet
            // Execute action 2: Stop ALIGN timer
            // Execute action 3: Initialize the OVERSHOOT timer
            // Consume event

      	//If event is collision
            // Move to HandlingCollision state to handle collision
            //mark that we are taking a transition
            // Consume event

  	// If current state is FindingSquare
      // Execute During function for ZigzagToSquareSM. ES_ENTRY & ES_EXIT are
      // processed here allow the lower level state machines to re-map
      // or consume the event

      // Wait here until Permits are acquired
      //If an event is active
     	//If MINER is successfully acquiring ore
            // Move to releasing miner state
            //mark that we are taking a transition
            // Consume event

       	//If event is collision
            // Move to HandlingCollision state to handle collision
	      	//mark that we are taking a transition
            // Consume event
    
    	// If exit, stop motors
    		// Consume Event

	// If current state is ReleasingMiner
      // Keep reversing until satisfied to begin
      //If an event is active
            // Execute action 1: Turn off electromagnet
            // Miner is no longer in possession once magnet is off
            // Execute action 2: Set motors to reverse
            // Execute action 3: Initiate shift timer to release MINER
            // Consume Event

   		//If Shift Timeout
              // If we get 1 miner, we are happy. Game is over (for us).
              // If feeling ambitious, can consume the event and continue gameplay

   	// If current state is HandlingCollision
      // Execute During function for CollisionSM. ES_ENTRY & ES_EXIT are
      // processed here allow the lower level state machines to re-map
      // or consume the event

		//If collision timer times out
			// If holding MINER, need to find the correct permit square
      		// If not holding MINER, need to find a MINER
      			// Mark that we are taking a transition
              	// Consume event

  //   If we are making a state transition
    //   Execute exit function for current state
   	//Modify state variable
    //  Execute entry function for new state
    //  this defaults to ES_ENTRY

/****************************************************************************
 Function
     StartAcquireMinerSM
****************************************************************************/
  // Check for history
  // call the entry function (if any) for the ENTRY_STATE

/****************************************************************************
 Function
     QueryAcquireMinerSM
****************************************************************************/
// returns current state of AcquireMinerSM

/***************************************************************************
 private functions
 ***************************************************************************/
// During function for Zigzag HSM
	// assume no re-mapping or consumption
  	//ES_Event_t CurrentEvent;
  	// process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
	// implement any entry actions required for this state machine
    // Start the lower level state machine
    // on exit, give the lower level a chance to clean up first
    // now do any local exit functionality

  // do the 'during' function for this state
    // run the lower level state machine
  // return either Event, if you don't want to allow the lower level machine
  // to remap the current event, or ReturnEvent if you do want to allow it.

// During function for Handling Collision HSM
  // process ES_ENTRY, ES_ENTRY_HISTORY & ES_EXIT events
    // implement any entry actions required for this state machine
    // Start the lower level state machine
    // on exit, give the lower level a chance to clean up first
    // now do any local exit functionality
  // do the 'during' function for this state
    // run the lower level state machine
  // return either Event, if you don't want to allow the lower level machine
  // to remap the current event, or ReturnEvent if you do want to allow it.

