// Handle Collision Pseudocode

/****************************************************************************
 Function
    RunHandleCollisionSM
 Description
   Run function for Collision State Machine
 Notes
   uses nested switch/case to implement the machine.
****************************************************************************/
// Switch based on state 
	// If current state is NegatingMotion
    //process any events
    //If an event is active
      // Record the entry motion by querying current movement from Drive Service
      // Initialize the Overshoot timer
      // If event is Timout of Collision Timer
   		// Set motors to stop
       	//switch states to SlightTurning
	    //change states when end this run
    	//mark that we are taking a transition
  		//consume the event
  	  // If the Event Param is the OVERSHOOT TIMER
       	//if we were moving forward, move backward
	   		// Set motors to reverse
	       	// Initialize the collision timer
	   	//if we were going backwards, go forwards
	       	// Set motors to forward
	      	//Initialize the collision timer
	    //if we were rotating left, rotate right
	       	// Set motors to turn right, 90 degrees
	      	//Initialize the collision timer
    	//if we were rotating right, rotate left
           	// Set motors to turn left, 90 degrees
     		//Initialize the collision timer
   	
   	// If current state is SlightTurning
	//process any events
 	//If an event is active
	  // If the entry motion did not involve turning
     	// Turn 45 degrees
    	// Switch states to MovingForward
       	// Change states when end this run
       	// Mark that we are taking a transition
      	// Consume the event
   
   	// If current state is MovingForward
  	//process any events
	//If an event is active
      	// Upon entry, start moving forward 
      		// Initialize the collision timer
   		// If collide timer times out
	       	//Return that the crash has been handled

  // If we are making a state transition
    //	Execute exit function for current state
	//	Modify state variable
    //	Execute entry function for new state
    //	this defaults to ES_ENTRY
// Return the return event

/****************************************************************************
 Function
     StartHandleCollisionSM
 Description
     Does any required initialization for this state machine
****************************************************************************/
  // to implement entry to a history state or directly to a substate
  // you can modify the initialization of the CurrentState variable
  // otherwise just start in the entry state every time the state machine
  // is started
  // call the entry function (if any) for the ENTRY_STATE

/****************************************************************************
 Function
     QueryHandleCollisionSM
 Returns
     HandleCollisionState_t The current state of this state machine
 Description
     returns the current state of the Handle Collision state machine
****************************************************************************/
// Returns current state

/****************************************************************************
 Function
     CheckSwitch

 Description
     Event checker that checks to see if beam has been broken.

 Author
     Lisa Gardner
****************************************************************************/
  // Check current movement. If we are currently moving, then check for collision
	// Record the front and back switch values
	// If the something has hit the front or back of the robot
      // Post a collision event to gameplay
	// Set return value to true
  // Record the current values in memory
  // Return the return value
