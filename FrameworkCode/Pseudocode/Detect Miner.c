// DetectMiner Pseudocode

/****************************************************************************
 Function
     InitDetectionModule

 Description
      Initializes the pins for MINER detection
      Sets the target beacon periods based on which team we are in
****************************************************************************/
  // Initialize Input Capture for Beacon Detection
  // set the thresholds based on which team we were on
  

/****************************************************************************
 Function
  CheckMinerDetection

 Description
  Event checker to see if the robot is aligned with a MINER.
  If so, which MINER?
****************************************************************************/
  // Recall last period
  // get the current period
  // initialize a counter to record how many times we detect the MINER
  // default to no event being generated

  // if we are in the detecting MINER state
  /* Check if this Period and LastPeriod were both within
      the threshold to align with MINER 1 */
  // if we detected the first MINER period
    // increase the counter because we've seen a MINER
    // if we've gotten greater than our minimum number of consecutive periods
      // Set ReturnVal = True, MINER was found
        // Post that we have detected and aligned with MINER 1.
        // set our target MINER accordingly
        // reset period counter
        //post to gameplay the detection event

  /* Check if this Period and LastPeriod were both within
      the threshold to align with MINER 2 */  
  // if we detected the second MINER period
    // increase the counter because we've seen a MINER
    // if we've gotten greater than our minimum number of consecutive periods
      // Set ReturnVal = True, MINER was found
      // Post that we have detected and aligned with MINER 2.
      // set our target MINER accordingly
      // reset period counter
       //post to gameplay the detection event
  // Otherwise, reset the counter
  // reset the Period
  // Return ReturnVal

/****************************************************************************
 Function
  GetMinerAlignment

 Description
  Check if robot is aligned with target MINER
****************************************************************************/
  // Check if robot is still aligned with target MINER after a period of time, so that we are on track
  // get the latest period
  // if we are going after MINER 1
  // if the period is within the expected period of threshold 1
    // we are aligned
  // if we are going after MINER 2 and we see a period within the expected threshold
    // we are aligned, return true
  // otherwise, we are unaligned, return false

/****************************************************************************
 Function
  GetTargetMiner

 Description
  Returns which miner we are going after

 Author
  Julea Chin
****************************************************************************/
// return the target miner (1 or 2)

/****************************************************************************
 * InitInputCapturePeriod
 * Uses Timer A in Wide Timer 5 to capture the input (PD6)
****************************************************************************/
  // start by enabling the clock to the timer (Wide Timer 5)
  // enable the clock to Port D
  // since we added this Port D clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A) is disabled before configuring
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  // set up timer A in capture mode (TAMR=3, TAAMS = 0),
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  // To set the event to rising edge, we need to modify the TAEVENT bits
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port D bit 6 (WT5CCP0)
  // Then, map bit 6's alternate functions to WT5CCP0
  // GPIOPCTL PMCx Bit Field Encoding/mux value = 7 for WT5CCP0
  // Clear bits 6 with 0xfffff0f
  // make pin 6 on Port D into an input (to read from the IR LED)
  // back to the timer to enable a local capture interrupt
  // enable the Timer A in Wide Timer 5 interrupt in the NVIC
  // it is interrupt number 104 so appears in EN3 at bit 8
  // make sure interrupts are enabled globally
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger

/****************************************************************************
 * IRInputCaptureResponse
 * Calculates the period between high edges sensed by the IR Receiver
 * Returns the period in TICKS
****************************************************************************/
  // Start by clearing the source of the interrupt, the input capture event
  // Grab the captured value and calculate the period


/****************************************************************************
 * GetPeriod
 * Converts the period from TICKS to MICROSECONDS
 * Julea Chin
****************************************************************************/
  // Calculate period

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/
