// Zigzag Pseudocode

/****************************************************************************
 Function
    RunZigzagSM

 Description
   Dictates movement to bring the MINER to the permit location
****************************************************************************/
    // Entry case: Finding current
      //process any events
      //If an event is active
    	// Upon entry, figure out which miner we have
	   	// calculate current location
	   	// get permit location
	  	// If we are at an indeterminate location
           	//Initialize the Zigzag timer 
         	//Set motors to reverse
	       	// Otherwise, if good location found
          	// Move to Finding previous state
        	// mark that we are taking a transition
            // Consume event
	   	// If Zigzag Timeout, check if we are at a legit location
	       	// If we are at an indeterminate location
       		// Initialize ZIgzag timer
          	// Continue reversing
     		// Good location found
	      	// Move to Finding Previous
			// mark that we are taking a transition
	      	// Consume event

	// Second state: Finding previous
   		//If an event is active
	 		// Upon entry, begin reversing 
	    	// Consume event
          
 	  	// If Found a new square!
	        // Check which direction facing
	      	// Decide which permit to go to
	     	// Move to CalculatingTrajectory
	     	// Mark that we are taking a transition
	       	// Stop the motors
	       	// Consume event

    // Next state: Turning
        // Upon entry, begin moving forward
        //Consume event
        // Post to drive service to begin turning at a specific angle 
      	//Move to CalculatingTrajectory
		//Mark that we are taking a transition
        // Consume event
		// Stop the motors
      	//Move to CalculatingTrajectory
		//Mark that we are taking a transition
        // Consume event
  
	// Next state: Going Forward
      //process any events
        // Upon entry, begin moving forward
		// Initialize the Zigzag timer 
        //Consume event
        // Upon entry, begin moving forward
        // Consume event
        // Found a new square!
        // Square found, stop moving
       	// Move to CalculatingTrajectory
		// Mark that we are taking a transition
        // Consume event
      // repeat state pattern as required for other states
  	//   If we are making a state transition
    //   Execute exit function for current state
 	//Modify state variable
    //   Execute entry function for new state
    // this defaults to ES_ENTRY

/****************************************************************************
 Function
     StartTemplateSM

 Description
     Does any required initialization for this state machine
****************************************************************************/
  // to implement entry to a history state or directly to a substate
  // you can modify the initialization of the CurrentState variable
  // otherwise just start in the entry state every time the state machine
  // is started
  // call the entry function (if any) for the ENTRY_STATE

/****************************************************************************
 Function
     QueryZigzagSM

 Returns
     ZigzagState_t The current state of the Zigzag state machine
****************************************************************************/
// Return current state

/***************************************************************************
CheckNewSquare
  Checks to see if we have entered a new square
********************************************************************** */
// Only use event checker if in Finding Previous or Going Forward
// Set previous column and row as the current column and row
// Update the current square location
// Get the permit coordinates
// If new square is found, post the event to gameplay
// Set previous location to current location
// Return ReturnVal

/***************************************************************************
 private functions
 ***************************************************************************/

/**************************************************************************
Function
    QueryCurrentSquare
    This function queries the SPUD to figure out where the MINER that is 
    currently in our possession is (i.e. where we are) in terms of board square
*///***********************************************************************
  // Query the location of the beacon in possession
  // Check if location is trustworthy

/**************************************************************************
Function
  GetPermitCoordinates
  This function gets our target permit coordinates (row and col on the board)
*///***********************************************************************
  // get the location we are trying to go to

/**************************************************************************
Function
  FindDirectionFacing
  Find which direction you (the bot) are facing
*///***********************************************************************
// Check facing NORTH
	// Check facing EAST
	// Check facing WEST
// Check facing SOUTH
	// Check facing East
	// Check facing West
// Check facing West
// Check facing East
// Set orientation as required


/**************************************************************************
Function
  FindPermitDirectoin
  Call every time we get into a new square to update our trajectory
*///***********************************************************************
// Update permit location
// Check facing NORTH
	// Check facing EAST
	// Check facing WEST
// Check facing SOUTH
	// Check facing East
	// Check facing West
// Check facing West
// Check facing East
// Update permit location angle as required




