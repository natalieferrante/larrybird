// Drive Service Pseudocode

/****************************************************************************
 * InitDriveService
 * Takes a priority number, returns true
****************************************************************************/
// Initialize the PWM pins
// Set motors to stop
// Record that we are stopping
// Initialize periodic interrupt
// Initialize encoders' input capture
// Initialize motor timer
// Return true for successful initialization

/****************************************************************************
 * RunDriveService
 * Receives an event from DriveFSM
 * Moves the robot forward/backward/turn at full or half speed, as needed
****************************************************************************/
// Switch between events
// Case 1: TIMEOUT
	// Re-initialize motor timer
	// Set motors to motion required by the stored current movement
	// which could be STOPPING, MOVING FORWARD, REVERSING, TURNING 
	// if Turning Left
		// Check if you were supposed to turn 90 degrees
		//  and if the input capture counter has met the required threshold
			// Stop the motors once 90 degrees is reached
			// Mark that you've finished turning 90 degrees
			// Post to Gameplay that the 90 degree turn was completed
		// Otherwise, keep turning left normally
	// if Turning Right
		// Check if you were supposed to turn 90 degrees
		//  and if the input capture counter has met the required threshold
			// Stop the motors once 90 degrees is reached
			// Mark that you've finished turning 90 degrees
			// Post to Gameplay that the 90 degree turn was completed
		// Otherwise, keep turning right normally
// For the following cases, sort which type of motion and set target RPM:
// Case 2: STOP
	// Set RPM goal to 0
	// Set specific turn flag to false
	// Set current motion to stop
// Case 3: FORWARD
	// Set RPM goal to event parameter
	// Set current motion to forward
// Case 4: REVERSE
	// Set RPM goal to event parameter
	// Set current motion to reverse
// Case 5: TURN LEFT
	// Set RPM goal to the turn RPM
	// Set current motion to turn left
	// If the turn is set to a specific angle 
		// Set the specific turn flag to true
		// Reset the input capture counter
		// Calculate and update the capture threshol
	// Otherwise, set turn flag to false
// Case 6: TURN RIGHT
	// Set RPM goal to the turn RPM
	// Set current motion to turn right
	// If the turn is set to a specific angle 
		// Set the specific turn flag to true
		// Reset the input capture counter
		// Calculate and update the capture threshol
	// Otherwise, set turn flag to false
// Return NO EVENT

/****************************************************************************
 * PostDriveService
 * Takes no parameters, returns True if an event was posted
 * local ReturnVal = False, CurrentInputState
****************************************************************************/
// Return the priority and this event

/****************************************************************************
 * RestoreDC
 * Call this function to revert to DC between 1-99
****************************************************************************/
// Set the action back to the normal actions

/****************************************************************************
 turn_left
 The robot will turn left at the given duty cycle
****************************************************************************/
// Restore any pins
// Set the left motor duty cycle 	(reverse)
// Set the right motor duty cycle 	(forward)
// Set GEN A and B

/****************************************************************************
 turn_right
 The robot will turn left at the given duty cycle
****************************************************************************/
// Restore any pins
// Set the left motor duty cycle	(forward)
// Set the right motor duty cycle	(reverse)
// Set GEN A and B

/****************************************************************************
 forward
 The robot will go forward at the given duty cycle
****************************************************************************/
// Restore any pins
// Set the left motor duty cycle	(forward)
// Set the right motor duty cycle	(forward)
// Set GEN A to LOW

/****************************************************************************
  reverse
  The robot will drive forward at the given duty cycle
****************************************************************************/
// Restore any pins
// Set the left motor duty cycle	(reverse)
// Set the right motor duty cycle	(reverse)
// Set GEN B to LOW

/****************************************************************************
 stop
 Sets the duty cycle on all four pins to 0. Stops the robot.
****************************************************************************/
// Set all pins to LOW

/****************************************************************************
 * initPins
 * Initialize all four PWM pins for the robot.
 * PB 4 = Left Motor
 * PB 5 = Left Motor
 * PB 6 = Right Motor
 * PB 7 = Right Motor
*****************************************************************************/
  // start by enabling the clock to the PWM Module (PWM0) for PB 4-7
  // enable the clock to Port B
  // Select the PWM clock as System Clock/32
  // make sure that the PWM module clock has gotten going
  // disable the PWM while initializing for all four pins
  // program generators to go to 1 at rising compare A/B, 0 on falling compare A/B
  // for all four pins
  // Set the PWM period. Since we are counting both up & down, we initialize
  // the load register to 1/2 the desired total period. We will also program
  // the match compare registers to 1/2 the desired high time
  // Set the initial Duty cycle on all four pins to 50% by programming the compare value
  // to 1/2 the period to count up (or down). Technically, the value to program
  // should be Period/2 - DesiredHighTime/2, but since the desired high time is 1/2
  // the period, we can skip the subtract
  // enable the PWM outputs for all four pins
  // now configure the Port B pins to be PWM outputs
  // start by selecting the alternate function for PB4-7
  // now choose to map PWM to those pins, this is a mux value of 4 that we
  // want to use for specifying the function on bits 4, 5, 6, 7
  // Enable pins 4, 5, 6, 7 on Port B for digital I/O
  // make pins 4-7 on Port B into outputs
  // set the up/down count mode, enable the PWM generator and make
  // both generator updates locally synchronized to zero count

/****************************************************************************
 EncoderLEFTInputCaptureResponse
 Response to input at Wide Timer 1, subtimer A
 Captures Encoder edges at PC6 (Left motor), calculates the period
****************************************************************************/
  // start by clearing the source of the interrupt, the input capture event
  // now grab the captured value on left motor and calculate the period
  // update LastCaptureLEFT to prepare for the next edge
  // Increment Counter

/****************************************************************************
 EncoderRIGHTInputCaptureResponse
 Response to input at Wide Timer 1, subtimer B
 Captures Encoder edges at PC7 (Right motor), calculates the period
****************************************************************************/
  // start by clearing the source of the interrupt, the input capture event
  // now grab the captured value on left motor and calculate the period
  // update LastCaptureRIGHT to prepare for the next edge
  // Increment Counter

/****************************************************************************
 InitPeriodicInt
 Implements PI Control function on motors based on encoder values & desired RPM
 Periodic Interrupt set on Wide Timer 0 subtimer B
****************************************************************************/
  //start by enabling the clock to the timer (Wide Timer 0)
  // kill a few cycles to let the clock get going
  // make sure that timer (Timer B) is disabled before configuring
  // set it up in 32bit wide (individual, not concatenated) mode
  // set up timer B in periodic mode so that it repeats the time-outs
  // set timeout DEFINED ABOVE
  // enable a local timeout interrupt
  // enable the Timer B in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 95 so appears in EN2 at bit 31
  // make sure interrupts are enabled globally
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger

/****************************************************************************
 PeriodicIntResponse
 Implements PI Control function on motors based on encoder values & desired RPM
 Periodic Interrupt set on Wide Timer 0 subtimer B
****************************************************************************/
  // start by clearing the source of the interrupt
  //run the closed loop control
  // increment our counter so that we can tell this is running

/****************************************************************************
 ControlLaw
 Implements PI Control function on motors based on encoder values & desired RPM
 Calculated for both motors
****************************************************************************/
// Update this period 
//12 counts per revolution of the motor when counting both edges of both channels
  // for encoder resolution (so 6 counts per rev for only one), with motor GR = 50
  //Calculate: SYSCLCK[Hz] * 1000[ms/s] * 60[s/min] / Period [ms] / (resolution*GR)
// Calculate the RPM based on this period
// Calculate both motors' error
// get integral terms for both motors
// Calculate updated duty cycle to send to motors
// Clamp the duty cycles if needed
// Update modular variables for requested duty cycles

/****************************************************************************
 Clamp
 Ensures that Duty cycle will not go above or below the max & min values
constrain val to be in range clampL to clampH
 Natalie Ferrante
****************************************************************************/
// If too high, return upper bound
// If too low, return lower bound
// Otherwise, return the unchanged value

/****************************************************************************
 QueryCurrentMovement
 Allows other modules (i.e. CollisionSM) to query the robot's live motion
****************************************************************************/
// Returns the modular variable, current movement

